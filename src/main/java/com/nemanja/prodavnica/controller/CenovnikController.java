package com.nemanja.prodavnica.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.CenovnikDtoToCenovnik;
import com.nemanja.prodavnica.converter.CenovnikToCenovnikDto;
import com.nemanja.prodavnica.converter.PoslovniPartnerToPoslovniPartnerDto;
import com.nemanja.prodavnica.converter.StavkaCenovnikaToStavkaCenovnikaDto;
import com.nemanja.prodavnica.dto.CenovnikDto;
import com.nemanja.prodavnica.dto.PoslovniPartnerDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.repository.PoslovniPartnerRepository;
import com.nemanja.prodavnica.repository.PreduzeceRepository;
import com.nemanja.prodavnica.service.interfaces.ICenovnikService;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;

@RestController
@RequestMapping("/api/cenovnik")
public class CenovnikController {

	@Autowired
	PreduzeceRepository preduzeceRepository;
	
	@Autowired
	PoslovniPartnerRepository poslovniPartnerRepository;
	
	@Autowired
	IPoslovniPartnerService poslovniPartenerServiceInterface;
	
	@Autowired
	PoslovniPartnerToPoslovniPartnerDto poslovniPartnerToDto;
	
	@Autowired
	ICenovnikService cenovnikServiceInterface;
	
	@Autowired
	CenovnikToCenovnikDto cenovnikToDto;
	
	@Autowired
	CenovnikDtoToCenovnik cenovnikFromDto;
	
	@Autowired
	StavkaCenovnikaToStavkaCenovnikaDto stavkaToDto;
	
	@GetMapping
	public ResponseEntity getAll() {
		List<Cenovnik> cenovnici = cenovnikServiceInterface.findAll();
		return ResponseEntity.ok().body(cenovnikToDto.convert(cenovnici));
	}
	
	@PostMapping
	public ResponseEntity postOne(@Validated @RequestBody CenovnikDto dto, Errors errors) {
		if(errors.hasErrors()) {
			return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
		}
		
		Cenovnik cenovnikConv = cenovnikFromDto.convert(dto);
		
		List<Cenovnik> cenovnici = null;
		if(dto.getPreduzeceId() > 0) {
		 cenovnici = cenovnikServiceInterface.findAllByPreduzeceId(dto.getPreduzeceId());
		}
		if(dto.getPoslovniPartnerId() > 0) {
		 cenovnici = cenovnikServiceInterface.findAllByPoslParnterId(dto.getPoslovniPartnerId());
		}
		
		
		for (Cenovnik c : cenovnici) {
			boolean isafter = c.getDatumVazenjaOd().after(cenovnikConv.getDatumVazenjaOd());
			boolean isafter2 = c.getDatumVazenjaOd().after(cenovnikConv.getDatumVazenjaDo());
			boolean isbefore = c.getDatumVazenjaDo().before(cenovnikConv.getDatumVazenjaOd());
			boolean isbefore2 = c.getDatumVazenjaDo().before(cenovnikConv.getDatumVazenjaDo());
			if(c.isObrisano() == false) {
				System.out.println("cenovnik od " + c.getDatumVazenjaOd()  + " sa id-em " + c.getId()  + " obrisan: " + c.isObrisano());
			if(!((isafter && isafter2) || (isbefore && isbefore2))) {
				System.out.println("Preklapanje datuma");
				return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
			}
			}
			
		}
		
		Cenovnik cenovnik = cenovnikServiceInterface.save(cenovnikFromDto.convert(dto));
		
		if(cenovnik != null) {
			if(dto.getPreduzeceId() > 0) {
				Optional<Preduzece> pred = preduzeceRepository.findById(dto.getPreduzeceId());
				if(pred.isPresent()) {
				Preduzece ppreduzece = pred.get();
				ppreduzece.getCenovnici().add(cenovnik);
				preduzeceRepository.save(ppreduzece);
				}
			}
			else {
				Optional<PoslovniPartner> partner = poslovniPartnerRepository.findById(dto.getPoslovniPartnerId());
				if(partner.isPresent()) {
				PoslovniPartner ppartner = partner.get();
				ppartner.getCenovnici().add(cenovnik);
				poslovniPartnerRepository.save(ppartner);
				}
				}
			return new ResponseEntity(cenovnikToDto.convert(cenovnik),HttpStatus.CREATED);
		}else {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity deleteOne(@PathVariable("id") long id) {
		Cenovnik cenovnik = cenovnikServiceInterface.findOne(id);
		if (cenovnik != null) {
			cenovnikServiceInterface.delete(cenovnik.getId());
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity getOne(@PathVariable("id") long id) {
		Cenovnik cenovnik = cenovnikServiceInterface.findOne(id);
		if (cenovnik!=null) {
			return new ResponseEntity(cenovnikToDto.convert(cenovnik),HttpStatus.OK);
		}else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity addCenovnik(@PathVariable("id") long id, @Validated @RequestBody CenovnikDto dto, Errors errors) {
		
		if(errors.hasErrors()) {
			return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
		}
		if(dto.getId()!=id) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		Cenovnik cenovnik = cenovnikServiceInterface.save(cenovnikFromDto.convert(dto));
		if(cenovnik!=null) {
			return new ResponseEntity(cenovnikToDto.convert(cenovnik),HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}/stavke_cenovnika")
	public ResponseEntity getStavkecenovnika(@PathVariable("id") long id,
			@RequestParam(value = "naziv", defaultValue = "") String naziv) {
		
		Cenovnik cenovnik = cenovnikServiceInterface.findOne(id);
		if (cenovnik!=null) {
			List<StavkaCenovnika> stavkeCenovnika = cenovnikServiceInterface.findAllByCenovnikId(cenovnik.getId(), naziv);
			return ResponseEntity.ok().body(stavkaToDto.convert(stavkeCenovnika));
			
		}
		else {
			
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/{id}/poslovni_partner")
	public ResponseEntity getPoslovniPartner(@PathVariable("id") long id) {
		Cenovnik cenovnik = cenovnikServiceInterface.findOne(id);
		if(cenovnik!= null) {
			try {
				PoslovniPartnerDto dto = poslovniPartnerToDto.convert(poslovniPartenerServiceInterface.findPartner(cenovnik));
				return ResponseEntity.ok(dto);
				
			}catch (Exception e) {
				// TODO: handle exception
				return ResponseEntity.notFound().build();
			}
			
		}
		else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/{id}/bezIzabranog")
	public ResponseEntity getAllWithoutGivenOne(@PathVariable("id") long id) {
		List<Cenovnik> cenovnik = cenovnikServiceInterface.findAllByNotId(id);
		return ResponseEntity.ok(cenovnikToDto.convert(cenovnik));

	}
	
	@PutMapping("/{id}/kopirajIzCenovnika/{izvorniCenovnikId}")
	public ResponseEntity kopirajCenovnik(@PathVariable("id") long id, @PathVariable("izvorniCenovnikId") long izvorniCenovnikId) {

		Cenovnik ciljaniCenovnik = cenovnikServiceInterface.findOne(id);
		Cenovnik izvorniCenovnik = cenovnikServiceInterface.findOne(izvorniCenovnikId);

		cenovnikServiceInterface.kopirajCenovnik(ciljaniCenovnik, izvorniCenovnik);

		return new ResponseEntity(HttpStatus.OK);
	}

}
