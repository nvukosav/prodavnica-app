package com.nemanja.prodavnica.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.FakturaDtoToFaktura;
import com.nemanja.prodavnica.converter.FakturaToFakturaDto;
import com.nemanja.prodavnica.converter.RobaToRobaDto;
import com.nemanja.prodavnica.converter.StavkaFaktureToStavkaFaktureDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.model.StavkaFakture;
import com.nemanja.prodavnica.service.interfaces.IFakturaService;
import com.nemanja.prodavnica.service.interfaces.IPoslovnaGodinaService;
import com.nemanja.prodavnica.service.interfaces.IStavkaFaktureService;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@RestController
@RequestMapping("api/fakture")
public class FakturaController {

	@Autowired
    private IStavkaFaktureService stavkaFaktureServiceInterface;
	
    @Autowired
    private IFakturaService fakturaServiceInterface;

    @Autowired
    private IPoslovnaGodinaService poslovnaGodinaServiceInterface;

    @Autowired
    private FakturaDtoToFaktura toFaktura;

    @Autowired
    private FakturaToFakturaDto toDto;
    
    @Autowired
    private StavkaFaktureToStavkaFaktureDto stavkaFaktureToDto;
    
    @Autowired
    private RobaToRobaDto robaToDto;
    
    @GetMapping
    public ResponseEntity getAll(){
        return ResponseEntity.ok(toDto.convert(fakturaServiceInterface.findAll()));
    }

    
    @GetMapping(value = "/ulazne")
    public ResponseEntity getUlazne(@RequestParam(value = "godina", defaultValue = "0") int godina,
                                    @RequestParam(value = "naziv",defaultValue = "") String naziv){
        if(godina == 0){
            List<Faktura> fakture = fakturaServiceInterface.findAllByVrstaFaktureAndNazivPartnera(1, naziv);
            return ResponseEntity.ok().body(toDto.convert(fakture));
        }
        List<Faktura> fakture = fakturaServiceInterface.findAllByVrstaFaktureAndPoslovnaGodinaAndNazivPartnera(1, naziv, godina);
        return ResponseEntity.ok().body(toDto.convert(fakture));
    }

    @GetMapping(value = "/izlazne")
    public ResponseEntity getIzlazne(@RequestParam(value = "godina", defaultValue = "0") int godina,
                                     @RequestParam(value = "naziv",defaultValue = "") String naziv){
        if(godina==0){
            List<Faktura> fakture = fakturaServiceInterface.findAllByVrstaFaktureAndNazivPartnera(0, naziv);
            return ResponseEntity.ok().body(toDto.convert(fakture));
        }
        List<Faktura> fakture = fakturaServiceInterface.findAllByVrstaFaktureAndPoslovnaGodinaAndNazivPartnera(0, naziv, godina);
        return ResponseEntity.ok().body(toDto.convert(fakture));
    }

    @GetMapping("/{id}/report")
    public ResponseEntity getReport(@PathVariable("id") long id){
        Faktura faktura = fakturaServiceInterface.findOne(id);
        if(faktura==null){
        	return new ResponseEntity(HttpStatus.NOT_FOUND);
        	}
    	
    	List<StavkaFakture> stavkeFaktureOrig = stavkaFaktureServiceInterface.findByFaktura_id(faktura.getId());
    	
    	JRBeanCollectionDataSource stavkeFakture = new JRBeanCollectionDataSource(stavkeFaktureOrig);
    	
        Map<String, Object> params  = new HashMap<>();

    	params.put("faktura", faktura);
    	params.put("stavkeFakture", stavkeFakture);
    	
    	for (StavkaFakture stavkaFakture : stavkeFaktureOrig) {
			System.out.println("id fakture: " + stavkaFakture.getId());
		}
    	
    	try {
    		
    		 InputStream is = null;
    	     String tip = null;
    	        
    		if(faktura.getVrstaFakture() == 1) {
            	is = new FileInputStream(new File("/home/nemanja/bachelor/3_Posl.Inf/Projekat/prodavnica-app/src/main/resources/ulazna-faktura.jrxml"));
            	tip = "ulazna-faktura";
            } else {
                is = new FileInputStream(new File("/home/nemanja/bachelor/3_Posl.Inf/Projekat/prodavnica-app/src/main/resources/izlazna-faktura.jrxml"));
                tip = "izlazna-faktura";
            }
    	
            JasperDesign jasperDesign = JRXmlLoader.load(is);
            
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
    		
    		JasperPrint jp = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
    		ByteArrayInputStream bis = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));
    		HttpHeaders headers = new HttpHeaders();
    		headers.add("Content-Disposition", "inline; filename="+faktura.getBrojFakture()+"-"+faktura.getPoslovnaGodina().getGodina()+".pdf");
    		return ResponseEntity
    	       		.ok()
    	       		.headers(headers)
    	       		.contentType(MediaType.APPLICATION_PDF)
    	       		.body(new InputStreamResource(bis));
    	}catch (Exception ex) {
    			ex.printStackTrace();
    		}
    	return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        Faktura faktura = fakturaServiceInterface.findOne(id);
        if(faktura==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(faktura));
    }
    
    @GetMapping("/{id}/roba_cenovnika")
    public ResponseEntity getCenovnikRoba(@PathVariable("id") long id){
    	Faktura faktura = fakturaServiceInterface.findOne(id);
        if(faktura==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<Cenovnik> cenovnici = new ArrayList<Cenovnik>();
        if (faktura.getPoslovniPartner().getVrstaPartnera()==1)
        	cenovnici.addAll(faktura.getPoslovniPartner().getCenovnici());
        else
        	cenovnici.addAll(faktura.getPreduzece().getCenovnici());
        Cenovnik cj = cenovnici.get(cenovnici.size()-1);
        ArrayList<Roba> roba = new ArrayList();
        for (StavkaCenovnika st : cj.getStavkeCenovnika()) {
        	roba.add(st.getRoba());
        }
        return ResponseEntity.ok(robaToDto.convert(roba));
    }
    
    @GetMapping("/{id}/stavke")
    public ResponseEntity getStavke(@PathVariable("id") long id){

        List<StavkaFakture> stavka_fakture = stavkaFaktureServiceInterface.findByFaktura_id(id);
        return ResponseEntity.ok(stavkaFaktureToDto.convert(stavka_fakture));
    }
   
}
