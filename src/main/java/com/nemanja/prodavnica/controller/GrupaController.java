package com.nemanja.prodavnica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.GrupaDtoToGrupa;
import com.nemanja.prodavnica.converter.GrupaToGrupaDto;
import com.nemanja.prodavnica.converter.RobaToRobaDto;
import com.nemanja.prodavnica.dto.GrupaDto;
import com.nemanja.prodavnica.model.Grupa;
import com.nemanja.prodavnica.service.interfaces.IGrupaService;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@RestController
@RequestMapping("api/grupa")
public class GrupaController {

	@Autowired
    private IGrupaService grupaServiceInterface;

    @Autowired
    private GrupaDtoToGrupa toGrupa;

    @Autowired
    private GrupaToGrupaDto toDto;

    @Autowired
    private IRobaService robaService;
    
    @Autowired
    private RobaToRobaDto toRobaDto;
    
    @GetMapping
    public ResponseEntity getAll(@RequestParam(value = "naziv",defaultValue = "") String naziv){
        List<Grupa> grupa = grupaServiceInterface.findAll(naziv);
        return ResponseEntity.ok().body(toDto.convert(grupa));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        Grupa grupa = grupaServiceInterface.findOne(id);
        if(grupa == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(grupa));
    }
    
    @PutMapping("/{id}")
    public ResponseEntity editGrupa(@PathVariable("id") long id,@Validated @RequestBody GrupaDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if(dto.getId() !=id){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Grupa grupa = grupaServiceInterface.save(toGrupa.convert(dto));
        if(grupa == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(grupa));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity deleteGrupa(@PathVariable("id") long id){
        Grupa grupa = grupaServiceInterface.findOne(id);
        if(grupa==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        grupaServiceInterface.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    
    @PostMapping
    public ResponseEntity addGrupa(@Validated @RequestBody GrupaDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Grupa grupa = grupaServiceInterface.save(toGrupa.convert(dto));
        if(grupa==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(toDto.convert(grupa),HttpStatus.CREATED);
    }
	
}
