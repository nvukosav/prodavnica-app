package com.nemanja.prodavnica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.MestoDtoToMesto;
import com.nemanja.prodavnica.converter.MestoToMestoDto;
import com.nemanja.prodavnica.dto.MestoDto;
import com.nemanja.prodavnica.model.Mesto;
import com.nemanja.prodavnica.service.interfaces.IMestoService;

@RestController
@RequestMapping("api/mesto")
public class MestoController {

	@Autowired
	IMestoService mestoServiceInterface;
	
	@Autowired
	MestoDtoToMesto toMesto;
	
	@Autowired
	MestoToMestoDto toDto;
	
    @GetMapping
    public ResponseEntity getAll(@RequestParam(value = "naziv",defaultValue = "") String naziv) {

        List<Mesto> mesta = mestoServiceInterface.findAll(naziv);
        return ResponseEntity.ok()
        		.body(toDto.convert(mesta));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id){
        Mesto mesto = mestoServiceInterface.findOne(id);
        if(mesto==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(mesto));
    }
    
    @PutMapping("/{id}")
    public ResponseEntity updateMesto(@PathVariable("id") long id,@Validated @RequestBody MestoDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if(dto.getId()!=id){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Mesto mesto = mestoServiceInterface.save(toMesto.convert(dto));
        if(mesto==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(mesto));
    }
    
    @PostMapping
    public ResponseEntity addMesto(@Validated @RequestBody MestoDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Mesto mesto = mestoServiceInterface.save(toMesto.convert(dto));
        if(mesto==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(mesto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity obrisiMesto(@PathVariable("id") long id) {
    	Mesto mesto = mestoServiceInterface.findOne(id);
    	if(mesto != null) {
    		mestoServiceInterface.delete(mesto.getId());
    		return new ResponseEntity(HttpStatus.NO_CONTENT);
    	}
    	else {
    		return new ResponseEntity(HttpStatus.NOT_FOUND);
    	}
    }
    
    

}
