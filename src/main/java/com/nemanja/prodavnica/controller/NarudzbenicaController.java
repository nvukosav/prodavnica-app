package com.nemanja.prodavnica.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.NarudzbenicaDtoToNarudzbenica;
import com.nemanja.prodavnica.converter.NarudzbenicaToNarudzbenicaDto;
import com.nemanja.prodavnica.converter.RobaToRobaDto;
import com.nemanja.prodavnica.converter.StavkaNarudzbeniceToStavkaNarudzbeniceDto;
import com.nemanja.prodavnica.dto.NarudzbenicaDto;
import com.nemanja.prodavnica.dto.StavkaNarudzbeniceDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.PoslovnaGodina;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.model.StavkaNarudzbenice;
import com.nemanja.prodavnica.service.interfaces.INarudzbenicaService;
import com.nemanja.prodavnica.service.interfaces.IOtpremnicaService;
import com.nemanja.prodavnica.service.interfaces.IPoslovnaGodinaService;
import com.nemanja.prodavnica.service.interfaces.IStavkaNarudzbeniceService;

@RestController
@RequestMapping("api/narudzbenice")
public class NarudzbenicaController {

	@Autowired
	private INarudzbenicaService narudzbenicaService;

	@Autowired
	private IStavkaNarudzbeniceService stavkaNarudzbeniceService;

	@Autowired
	private IPoslovnaGodinaService poslovnaGodinaService;

	@Autowired
	private IOtpremnicaService otpremnicaService;

	@Autowired
	private RobaToRobaDto robaToDto;

	@Autowired
	private NarudzbenicaDtoToNarudzbenica narudzbenicaDtoToNarudzbenica;

	@Autowired
	private NarudzbenicaToNarudzbenicaDto narudzbenicaToNarudzbenicaDto;

	@Autowired
	private StavkaNarudzbeniceToStavkaNarudzbeniceDto stavkaNarudzbeniceToStavkaNarudzbeniceDto;

	@GetMapping
	public ResponseEntity getAll(@RequestParam(value = "godina", defaultValue = "0") int godina,
			@RequestParam(value = "naziv", defaultValue = "") String naziv) {

		List<Narudzbenica> narudzbenice;

		if (godina == 0) {
			narudzbenice = narudzbenicaService.findAllByNazivPartnera(naziv);
		} else {
			narudzbenice = narudzbenicaService.findAllByPoslovnaGodinaAndNazivPartnera(naziv, godina);
		}

		List<NarudzbenicaDto> narudzbeniceDto = narudzbenicaToNarudzbenicaDto.convert(narudzbenice);
		return ResponseEntity.ok().body(narudzbeniceDto);
	}

	@GetMapping(value = "/ulazne")
	public ResponseEntity getAllUlazne(@RequestParam(value = "godina", defaultValue = "0") int godina,
			@RequestParam(value = "naziv", defaultValue = "") String naziv) {

		List<Narudzbenica> narudzbenice;

		if (godina == 0) {
			narudzbenice = narudzbenicaService.findAllByTipAndNazivPartnera(1, naziv);
		} else {
			narudzbenice = narudzbenicaService.findAllByTipAndNazivPartneraAndGodina(1, naziv, godina);
		}

		List<NarudzbenicaDto> narudzbeniceDto = narudzbenicaToNarudzbenicaDto.convert(narudzbenice);

		return ResponseEntity.ok().body(narudzbeniceDto);
	}

	@GetMapping(value = "/izlazne")
	public ResponseEntity getAllIzlazne(@RequestParam(value = "godina", defaultValue = "0") int godina,
			@RequestParam(value = "naziv", defaultValue = "") String naziv) {

		List<Narudzbenica> narudzbenice;

		if (godina == 0) {
			narudzbenice = narudzbenicaService.findAllByTipAndNazivPartnera(0, naziv);
		} else {
			narudzbenice = narudzbenicaService.findAllByTipAndNazivPartneraAndGodina(0, naziv, godina);
		}

		List<NarudzbenicaDto> narudzbeniceDto = narudzbenicaToNarudzbenicaDto.convert(narudzbenice);

		return ResponseEntity.ok().body(narudzbeniceDto);
	}

	@GetMapping("/{id}")
	public ResponseEntity getOne(@PathVariable("id") long id) {

		Narudzbenica narudzbenica = narudzbenicaService.findOne(id);
		if (narudzbenica == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

		NarudzbenicaDto narudzbenicaDto = narudzbenicaToNarudzbenicaDto.convert(narudzbenica);
		return new ResponseEntity<>(narudzbenicaDto, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity addNarudzbenica(@Validated @RequestBody NarudzbenicaDto dto, Errors errors) {

		if (errors.hasErrors()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		PoslovnaGodina poslednjaPoslovnaGodina = poslovnaGodinaService.findPoslovnaGodinaIsNotObrisanoIsNotZakljucena();
		Narudzbenica narudzbenica = narudzbenicaDtoToNarudzbenica.convert(dto);

		narudzbenica.setBrojNarudzbenice(poslednjaPoslovnaGodina.getNarudzbenice().size() + 1);
		narudzbenica.setDatumNarudzbenice(new Date());
		narudzbenica.setTipNarudzbenice(narudzbenica.getPoslovniPartner().getVrstaPartnera());
		narudzbenica.setPoslovnaGodina(poslednjaPoslovnaGodina);
		narudzbenica.setObrisano(false);

		Narudzbenica dbNarudzbenica = narudzbenicaService.save(narudzbenica);
		NarudzbenicaDto narudzbenicaDto = narudzbenicaToNarudzbenicaDto.convert(dbNarudzbenica);

		return new ResponseEntity<>(narudzbenicaDto, HttpStatus.CREATED);
	}
	
	@PostMapping("/{id}/napraviOtpremnicu")
	public ResponseEntity napraviOtpremnicuOdNarudzbenice(@PathVariable("id") long id) {

		Narudzbenica narudzbenica = narudzbenicaService.findOne(id);
		otpremnicaService.napraviOtpremnicuOdNarudzbenice(narudzbenica);

		return new ResponseEntity(HttpStatus.CREATED);
	}

	@PostMapping("/{id}/napraviFakturu")
	public ResponseEntity napraviFakturuOdNarudzbenice(@PathVariable("id") long id) {
		PoslovnaGodina poslovnaGodina = poslovnaGodinaService.findPoslovnaGodinaIsNotObrisanoIsNotZakljucena();
		int poslednjaPoslovnjaGodina = poslovnaGodina.getFakture().size();

		Narudzbenica narudzbenica = narudzbenicaService.findOne(id);
		narudzbenicaService.napraviFakturuOdNarudzbenice(narudzbenica, poslednjaPoslovnjaGodina);

		return new ResponseEntity(HttpStatus.CREATED);
	}

	@GetMapping("/{id}/stavkeNarudzbenice")
	public ResponseEntity getStavke(@PathVariable("id") long id) {

		List<StavkaNarudzbenice> stavkeNarudzbenice = stavkaNarudzbeniceService.pronadjiStavkeNarudzbenice(id);
		
		for (StavkaNarudzbenice stavkaNarudzbenice : stavkeNarudzbenice) {
			System.out.println(stavkaNarudzbenice.getOpis());
		}
		
		List<StavkaNarudzbeniceDto> stavkeNarudzbeniceDto = stavkaNarudzbeniceToStavkaNarudzbeniceDto
				.convert(stavkeNarudzbenice);
		return new ResponseEntity<>(stavkeNarudzbeniceDto, HttpStatus.OK);
	}

	@GetMapping("/{id}/robaCenovnika")
	public ResponseEntity getCenovnikRoba(@PathVariable("id") long id) {
		Narudzbenica narudzbenica = narudzbenicaService.findOne(id);
		if (narudzbenica == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		List<Cenovnik> cenovnici = new ArrayList<Cenovnik>();
		if (narudzbenica.getPoslovniPartner().getVrstaPartnera() == 1)
			cenovnici.addAll(narudzbenica.getPoslovniPartner().getCenovnici());
		else
			cenovnici.addAll(narudzbenica.getPreduzece().getCenovnici());

		ArrayList<Roba> roba = new ArrayList();

		Date date = new Date();

		for (Cenovnik cenovnik : cenovnici) {
			boolean isafter = cenovnik.getDatumVazenjaOd().before(date);
			boolean isbefore = cenovnik.getDatumVazenjaDo().after(date);
			
			System.out.println(isafter);
			System.out.println(isbefore);
			
			if (isafter && isbefore && !(cenovnik.isObrisano())) {
				for (StavkaCenovnika s : cenovnik.getStavkeCenovnika()) {
					roba.add(s.getRoba());
				}
			}
		}

		return ResponseEntity.ok(robaToDto.convert(roba));
	}

}
