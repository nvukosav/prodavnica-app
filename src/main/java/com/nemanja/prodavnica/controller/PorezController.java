package com.nemanja.prodavnica.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.PorezDtoToPorez;
import com.nemanja.prodavnica.converter.PorezToPorezDto;
import com.nemanja.prodavnica.converter.StopaPorezaToStopaPorezaDto;
import com.nemanja.prodavnica.model.Porez;
import com.nemanja.prodavnica.model.StopaPoreza;
import com.nemanja.prodavnica.service.interfaces.IPorezService;

@RestController
@RequestMapping(value = "api/porez")
public class PorezController {

    @Autowired
    private IPorezService porezServiceInterface;

    @Autowired
    private PorezDtoToPorez toPorez;

    @Autowired
    private PorezToPorezDto todto;
    
    @Autowired
    private StopaPorezaToStopaPorezaDto stopaPorezatoDto;
    
    @GetMapping
    public ResponseEntity getAll(){
    	List<Porez> porezi = porezServiceInterface.findAll();
    	if(porezi != null) {
        return ResponseEntity.ok(todto.convert(porezi));
    	}
    	else {
    		throw new RuntimeException("Porez je null");
    	}
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity getOne(@PathVariable long id){
        Porez porez = porezServiceInterface.findOne(id);
        if(porez==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(todto.convert(porez));
    }
    
    @GetMapping(value = "/{id}/stopa")
    public ResponseEntity getStopa(@PathVariable long id){
        Porez porez = porezServiceInterface.findOne(id);
        if(porez==null || porez.getStopePoreza().isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<StopaPoreza> stope = new ArrayList(porez.getStopePoreza());
        Collections.sort(stope, (o1, o2) -> (o1.getDatumVazenja().compareTo(o2.getDatumVazenja())));
        return ResponseEntity.ok(stopaPorezatoDto.convert(stope.get(stope.size()-1)));
    }
	
}
