package com.nemanja.prodavnica.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.CenovnikToCenovnikDto;
import com.nemanja.prodavnica.converter.PoslovniPartnerDtoToPoslovniPartner;
import com.nemanja.prodavnica.converter.PoslovniPartnerToPoslovniPartnerDto;
import com.nemanja.prodavnica.dto.PoslovniPartnerDto;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;

@RestController
@RequestMapping(value = "api/poslovni_partneri")
public class PoslovniPartnerController {

    @Autowired
    private IPoslovniPartnerService poslovniPartnerServiceInterface;

    @Autowired
    private PoslovniPartnerDtoToPoslovniPartner toPoslovniPartner;

    @Autowired
    private PoslovniPartnerToPoslovniPartnerDto toDto;
    
    @Autowired
    private CenovnikToCenovnikDto toCenovnikDto;

    
    @GetMapping
    public ResponseEntity getAll(@RequestParam(value = "filter",defaultValue = "") String filter,
                                 @RequestParam(value = "tip", defaultValue = "1") int tip){
        List<PoslovniPartner> poslovniPartneri = poslovniPartnerServiceInterface.findAll(filter);
        tip = Integer.valueOf(tip);

        int finalTip = tip;
        List<PoslovniPartner> poslovniPartnerFilter = poslovniPartneri.stream()
                .filter(p->p.getVrstaPartnera() == finalTip && !p.isObrisano()).collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(toDto.convert(poslovniPartnerFilter));
    }
    
    @PostMapping
    public ResponseEntity dodajPartnera(@Validated @RequestBody PoslovniPartnerDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        PoslovniPartner partner = poslovniPartnerServiceInterface.save(toPoslovniPartner.convert(dto));
        if(partner==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(toDto.convert(partner),HttpStatus.CREATED);
    }
	
    @GetMapping(value = "/{id}")
    public ResponseEntity getOne(@PathVariable long id){
        PoslovniPartner partner = poslovniPartnerServiceInterface.findOne(id);
        if(partner==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(partner));
    }
    
    @PutMapping(value = "/{id}")
    public ResponseEntity editPartner(@PathVariable long id, @Validated @RequestBody PoslovniPartnerDto dto,Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        if(dto.getId()!=id){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        PoslovniPartner partner = poslovniPartnerServiceInterface.save(toPoslovniPartner.convert(dto));
        if(partner==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(partner));
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deletePartner(@PathVariable long id){
        PoslovniPartner partner = poslovniPartnerServiceInterface.findOne(id);
        if(partner==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        poslovniPartnerServiceInterface.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
