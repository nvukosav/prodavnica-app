package com.nemanja.prodavnica.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.CenovnikToCenovnikDto;
import com.nemanja.prodavnica.converter.PoslovniPartnerToPoslovniPartnerDto;
import com.nemanja.prodavnica.converter.PreduzeceDtoToPreduzece;
import com.nemanja.prodavnica.converter.PreduzeceToPreduzeceDto;
import com.nemanja.prodavnica.dto.MestoDto;
import com.nemanja.prodavnica.dto.PreduzeceDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.Mesto;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@RestController
@RequestMapping("/api/preduzece")
public class PreduzeceController {
	
    @Autowired
    private IPoslovniPartnerService poslovniPartnerServiceInterface;

    @Autowired
    private IPreduzeceService preduzeceService;

    @Autowired
    private PreduzeceDtoToPreduzece toPreduzece;

    @Autowired
    private PreduzeceToPreduzeceDto toDto;

    @Autowired
    private PoslovniPartnerToPoslovniPartnerDto poslovniPartnerToDto;

    @Autowired
    private CenovnikToCenovnikDto toCenovnikDto;
    
    @GetMapping
    public ResponseEntity getAll() {
    	return ResponseEntity.ok(toDto.convert(preduzeceService.findAll()));
    }
    
    @GetMapping(value= "/{id}")
    public ResponseEntity getOne(@PathVariable long id) {
    	Preduzece preduzece = preduzeceService.findOne(id);
    	if(preduzece == null) {
    		return new ResponseEntity(HttpStatus.NOT_FOUND);
    	}
    	else {
    		return ResponseEntity.ok(toDto.convert(preduzece));
    	}	
    }
    
    @GetMapping("/{id}/partneri")
    public ResponseEntity getPartneri(@PathVariable("id") long id){
        return ResponseEntity.ok(poslovniPartnerToDto.convert(poslovniPartnerServiceInterface.findByPreduzece_id(id)));
    }
    
    @GetMapping("/{id}/cenovnici")
    public ResponseEntity getCenovnici(@PathVariable("id") long id) {
    	Preduzece preduzece = preduzeceService.findOne(id);
    	Set<Cenovnik> c = preduzece.getCenovnici();
		return ResponseEntity.ok(toCenovnikDto.convert(c.stream().filter(x -> !x.isObrisano()).collect(Collectors.toList())));
    }
    
    @PostMapping
    public ResponseEntity addPreduzece(@Validated @RequestBody PreduzeceDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Preduzece preduzece = preduzeceService.save(toPreduzece.convert(dto));
        if(preduzece==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(preduzece));
    }
    
    @PutMapping(value= "/{id}")
    public ResponseEntity editPreduzece(@PathVariable long id, @Validated @RequestBody PreduzeceDto dto, Errors errors) {
    	
    	if(errors.hasErrors()) {
    		return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
    	}
    	if(dto.getId() != id) {
    		return new ResponseEntity(HttpStatus.NOT_FOUND);
    	}
    	Preduzece preduzece = preduzeceService.save(toPreduzece.convert(dto));
    	if(preduzece == null) {
    		return new ResponseEntity(HttpStatus.BAD_REQUEST);
    	}
    	return ResponseEntity.ok(toDto.convert(preduzece));
    	
    }
    
    @GetMapping("{id}/reports/{vrsta}")
    public ResponseEntity getReports(@RequestParam("godina") int godina, @PathVariable("id") long id, @PathVariable("vrsta") String vrsta) throws JRException, FileNotFoundException{
    	boolean tipFakture = false;
    	if(vrsta.equalsIgnoreCase("ulazne")) {
    	tipFakture = true;
    	}
    	
    	Preduzece preduzece = preduzeceService.findOne(id);
        List<Faktura> faktureFinal = new ArrayList<Faktura>();
        List<Faktura> fakture = preduzeceService.findAllByPreduzeceAndVrstaFaktureAndPlaceno(tipFakture, id, true);

        if(fakture==null){
        	return new ResponseEntity(HttpStatus.NOT_FOUND);
        	}
    	
        if(fakture.isEmpty()){
        	return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
          
        Map<String, Object> params  = new HashMap<String, Object>();
        for(Faktura i : fakture){
        	if(i.getPoslovnaGodina().getGodina() == godina){
            	faktureFinal.add(i);      		
        	}        	
        }
        params.put("godina", godina);
        params.put("fakture", faktureFinal);
        params.put("preduzece", preduzece);     
        
       
        InputStream is = null;
        String tip = null;
        if(tipFakture) {
        	is = new FileInputStream(new File("/home/nemanja/bachelor/3_Posl.Inf/Projekat/prodavnica-app/src/main/resources/ulazne-fakture.jrxml"));
        	tip = "ulazne-fakture";
        } else {
            is = new FileInputStream(new File("/home/nemanja/bachelor/3_Posl.Inf/Projekat/prodavnica-app/src/main/resources/izlazne-fakture.jrxml"));
            tip = "izlazne-fakture";
        }
	
        JasperDesign jasperDesign = JRXmlLoader.load(is);
        
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
         
        try{ 	
    		JasperPrint jp = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
    		ByteArrayInputStream bis = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));
    		HttpHeaders headers = new HttpHeaders();
    		headers.add("Content-Disposition", "inline; filename="+preduzece.getNaziv()+"-"+tip+".pdf");
    		return ResponseEntity
    	       		.ok()
    	       		.headers(headers)
    	       		.contentType(MediaType.APPLICATION_PDF)
    	       		.body(new InputStreamResource(bis));
        }catch(Exception ex){
        	ex.printStackTrace();

        }
    	return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    

}
