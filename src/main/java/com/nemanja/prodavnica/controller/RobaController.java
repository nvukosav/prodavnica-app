package com.nemanja.prodavnica.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.PorezToPorezDto;
import com.nemanja.prodavnica.converter.RobaDtoToRoba;
import com.nemanja.prodavnica.converter.RobaToRobaDto;
import com.nemanja.prodavnica.converter.StavkaCenovnikaToStavkaCenovnikaDto;
import com.nemanja.prodavnica.dto.RobaDto;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.service.interfaces.IRobaService;
import com.nemanja.prodavnica.service.interfaces.IStavkaCenovnikaService;

@RestController
@RequestMapping("/api/roba")	
public class RobaController {

	
	@Autowired
	IRobaService robaService;
	
	@Autowired
	IStavkaCenovnikaService stavkaCenovnikaService;
	
	@Autowired
	StavkaCenovnikaToStavkaCenovnikaDto stavkaCenovnikaToDtoConv;
	
	@Autowired
	RobaToRobaDto toDtoConv;
	
	@Autowired
	RobaDtoToRoba fromDtoConv;
	
	@Autowired
	PorezToPorezDto porezToDto;
	
	@GetMapping
	public ResponseEntity getAll(@RequestParam(value="grupa", defaultValue="0") Long grupa,
								@RequestParam(value = "naziv", defaultValue="") String naziv) {
		
		if(grupa == 0) {
			List<Roba> robe = robaService.findAll(naziv);
			return ResponseEntity.ok().body(toDtoConv.convert(robe));
			
		}
		else {
			List<Roba> robe = robaService.findAllByGrupa_id(grupa,naziv);
			return ResponseEntity.ok().body(toDtoConv.convert(robe));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity getOne(@PathVariable("id") long id) {
		Roba roba = robaService.findOne(id);
		if (roba!=null) {
			return new ResponseEntity(toDtoConv.convert(roba),HttpStatus.OK);
		}else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
    }
	
	
	@PutMapping("/{id}")
	public ResponseEntity editRoba(@PathVariable("id") long id, @Validated @RequestBody RobaDto dto, Errors errors){
		
		if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        if(dto.getId()!=id){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Roba roba = fromDtoConv.convert(dto);
        Roba robasaved = robaService.save(roba);
        if(roba!=null){
            return new ResponseEntity(toDtoConv.convert(robasaved),HttpStatus.OK);
        }else {
        	return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity deleteRoba(@PathVariable("id") long id){
		Roba roba = robaService.findOne(id);
		if (roba!=null) {
			robaService.delete(roba.getId());
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
	}

	
	@PostMapping
	public ResponseEntity addRoba(@Validated @RequestBody RobaDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        Roba roba = robaService.save(fromDtoConv.convert(dto));
        if(roba != null){
        	return new ResponseEntity(toDtoConv.convert(roba),HttpStatus.CREATED);
        }else {
        	return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
	
	@GetMapping("/{id}/cena")
	public ResponseEntity getCena(@PathVariable("id") long id) {
		Roba roba = robaService.findOne(id);
		if (roba!=null) {
			List<StavkaCenovnika> stavkeCenovnika = stavkaCenovnikaService.findAllByRoba_id(id);
			Collections.sort(stavkeCenovnika, (o1, o2) -> (o1.getCenovnik().getDatumVazenjaOd().compareTo(o2.getCenovnik().getDatumVazenjaOd())));
			return new ResponseEntity(stavkaCenovnikaToDtoConv.convert(stavkeCenovnika.get(stavkeCenovnika.size()-1)),HttpStatus.OK);
		}else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
    }
	
	@GetMapping("/{id}/porez")
	public ResponseEntity getPorez(@PathVariable("id") long id) {
		Roba roba = robaService.findOne(id);
		return ResponseEntity.ok(porezToDto.convert(roba.getGrupa().getPorez()));
	}
	
	
}
