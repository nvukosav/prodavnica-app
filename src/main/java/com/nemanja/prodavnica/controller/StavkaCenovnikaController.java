package com.nemanja.prodavnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.StavkaCenovnikaDtoToStavkaCenovnika;
import com.nemanja.prodavnica.converter.StavkaCenovnikaToStavkaCenovnikaDto;
import com.nemanja.prodavnica.dto.StavkaCenovnikaDto;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.service.interfaces.IStavkaCenovnikaService;

@RestController
@RequestMapping("/api/stavka_cenovnika")
public class StavkaCenovnikaController {

  @Autowired
  private IStavkaCenovnikaService stavkaCenovnikaService;

  @Autowired
  private StavkaCenovnikaDtoToStavkaCenovnika toStavkaCenovnika;

  @Autowired
  private StavkaCenovnikaToStavkaCenovnikaDto toDto;
	  
	  
    @GetMapping(value = "/{id}")
    public ResponseEntity getOne(@PathVariable long id){
    	StavkaCenovnika stavka = stavkaCenovnikaService.findOne(id);
        if(stavka==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(stavka));
    }
    
    @GetMapping
    public ResponseEntity getAll(){
        return ResponseEntity.ok(toDto.convert(stavkaCenovnikaService.findAll()));
    }
	    
	  
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteOne(@PathVariable long id){
        StavkaCenovnika stavka = stavkaCenovnikaService.findOne(id);
        if(stavka==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        stavkaCenovnikaService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    
    @PostMapping
    public ResponseEntity addStavkaCenovnika(@Validated @RequestBody StavkaCenovnikaDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        System.out.println("CENA DTO: "+ dto.getCena());
        StavkaCenovnika stavka = stavkaCenovnikaService.save(toStavkaCenovnika.convert(dto));
        System.out.println(stavka.getId());
        System.out.println("KONVERTOVANA CENA: " + stavka.getCena());
        
        if(stavka==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(toDto.convert(stavka),HttpStatus.CREATED);
    }
    
    @PutMapping(value = "/{id}")
    public ResponseEntity editStavkaCenovnika(@PathVariable long id, @Validated @RequestBody StavkaCenovnikaDto dto,Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        if(dto.getId()!=id){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        
        StavkaCenovnika stavka = toStavkaCenovnika.convert(dto);
        stavka.setId(id);
        
        StavkaCenovnika saved = stavkaCenovnikaService.save(stavka);
        if(saved==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(stavka));
    }

	
}
