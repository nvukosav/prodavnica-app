package com.nemanja.prodavnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nemanja.prodavnica.converter.StopaPorezaDtoToStopaPoreza;
import com.nemanja.prodavnica.converter.StopaPorezaToStopaPorezaDto;
import com.nemanja.prodavnica.dto.StopaPorezaDto;
import com.nemanja.prodavnica.model.StopaPoreza;
import com.nemanja.prodavnica.service.interfaces.IStopaPorezaService;

@RestController
@RequestMapping(value = "api/stopa_poreza")
public class StopaPorezaController {

    @Autowired
    private IStopaPorezaService stopaPorezaService;

    @Autowired
    private StopaPorezaDtoToStopaPoreza toStopaPoreza;

    @Autowired
    private StopaPorezaToStopaPorezaDto toDto;
    
    @GetMapping
    public ResponseEntity getAll(){
        return ResponseEntity.ok(toDto.convert(stopaPorezaService.findAll()));
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity getOne(@PathVariable long id){
    	StopaPoreza stopa = stopaPorezaService.findOne(id);
        if(stopa==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(toDto.convert(stopa));
    }
    
    @PutMapping(value = "/{id}")
    public ResponseEntity editStopaPoreza(@PathVariable long id, @Validated @RequestBody StopaPorezaDto dto,Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        if(dto.getId()!=id){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        StopaPoreza stopaPoreza = toStopaPoreza.convert(dto);
        stopaPoreza.setId(id);
        StopaPoreza stopa = stopaPorezaService.save(stopaPoreza);
        if(stopa==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(toDto.convert(stopa));
    }
    
    @PostMapping
    public ResponseEntity addStopaPoreza(@Validated @RequestBody StopaPorezaDto dto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(),HttpStatus.BAD_REQUEST);
        }
        StopaPoreza stopa = stopaPorezaService.save(toStopaPoreza.convert(dto));
        if( stopa==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(toDto.convert( stopa),HttpStatus.CREATED);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteStopaPoreza(@PathVariable long id){
        StopaPoreza stopa = stopaPorezaService.findOne(id);
        if(stopa==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        stopaPorezaService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
