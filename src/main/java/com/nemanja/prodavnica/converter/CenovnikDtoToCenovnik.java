package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.CenovnikDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

@Component
public class CenovnikDtoToCenovnik  implements Converter<CenovnikDto, Cenovnik>{

    @Autowired
    private IPreduzeceService preduzeceServiceInterface;

    @Autowired
    private IPoslovniPartnerService poslovniPartnerServiceInterface;
    
	@Override
	public Cenovnik convert(CenovnikDto source) {
		Cenovnik cenovnik = new Cenovnik();
		cenovnik.setId(source.getId());
		cenovnik.setDatumVazenjaOd(source.getDatumVazenjaOd());
		cenovnik.setDatumVazenjaDo(source.getDatumVazenjaDo());
		
		Preduzece preduzece = preduzeceServiceInterface.findOne(source.getPreduzeceId());
	      if(preduzece!=null) {
	    	  cenovnik.setPreduzece(preduzece);
	      }
	      
	     PoslovniPartner poslovniPartner = poslovniPartnerServiceInterface.findOne(source.getPoslovniPartnerId());
	     if(poslovniPartner!=null) {
	    	  cenovnik.setPoslovniPartner(poslovniPartner);
	      }
		
		return cenovnik;
	}

	
	
}
