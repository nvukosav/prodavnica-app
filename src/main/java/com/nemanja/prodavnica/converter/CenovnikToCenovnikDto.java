package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.CenovnikDto;
import com.nemanja.prodavnica.model.Cenovnik;

@Component
public class CenovnikToCenovnikDto implements Converter<Cenovnik, CenovnikDto>{

	@Override
	public CenovnikDto convert(Cenovnik source) {
		CenovnikDto cenovnikDto = new CenovnikDto();
		
		cenovnikDto.setId(source.getId());
		cenovnikDto.setDatumVazenjaOd(source.getDatumVazenjaOd());
		cenovnikDto.setDatumVazenjaDo(source.getDatumVazenjaDo());
		if(source.getPoslovniPartner() != null) {
		cenovnikDto.setPoslovniPartnerId(source.getPoslovniPartner().getId());
		}
		if(source.getPreduzece() != null) {
		cenovnikDto.setPreduzeceId(source.getPreduzece().getId());
		}
		return cenovnikDto;
	}

	public List<CenovnikDto> convert(List<Cenovnik> cenovnici){
		List<CenovnikDto> cenovnikDto = new ArrayList<>();
		
		for(Cenovnik c : cenovnici) {
			cenovnikDto.add(convert(c));
		}
		return cenovnikDto;
	}
	
	
}
