package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.FakturaDto;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.PoslovnaGodina;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IPoslovnaGodinaService;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

@Component
public class FakturaDtoToFaktura implements Converter<FakturaDto, Faktura>{

    @Autowired
    private IPreduzeceService preduzeceServiceInterface;

    @Autowired
    private IPoslovniPartnerService poslovniPartnerServiceInterface;

    @Autowired
    private IPoslovnaGodinaService poslovnaGodinaServiceInterface;

	@Override
	public Faktura convert(FakturaDto source) {
		Faktura faktura = new Faktura();
		
	      faktura.setId(source.getId());
	      faktura.setBrojFakture(source.getBrFakture());
	      faktura.setDatumFakture(source.getDatumFakture());
	      faktura.setDatumValute(source.getDatumValute());
	      faktura.setOsnovica(source.getOsnovica());
	      faktura.setIznosBezRabata(source.getIznosBezRabata());
	      faktura.setRabat(source.getRabat());
	      faktura.setUkupanPorez(source.getUkupanPorez());
	      faktura.setIznosZaPlacanje(source.getIznosZaPlacanje());
	      faktura.setPlaceno(source.isPlaceno());
	      faktura.setVrstaFakture(source.getVrstaFakture());
	      
	      Preduzece preduzece = preduzeceServiceInterface.findOne(source.getPreduzece());
	      if(preduzece!=null) {
	    	  faktura.setPreduzece(preduzece);
	      }
	      PoslovniPartner poslovniPartner = poslovniPartnerServiceInterface.findOne(source.getPoslovniPartner());
	      if(poslovniPartner !=null) {
	    	  faktura.setPoslovniPartner(poslovniPartner);
	      }
	      PoslovnaGodina poslovnaGodina = poslovnaGodinaServiceInterface.findOne(source.getPoslovnaGodina());
	      if(poslovnaGodina!=null){
	           faktura.setPoslovnaGodina(poslovnaGodina);
	        }
	        return faktura;
	}
	

	
}
