package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.GrupaDto;
import com.nemanja.prodavnica.model.Grupa;
import com.nemanja.prodavnica.model.Porez;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IPorezService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

@Component
public class GrupaDtoToGrupa implements Converter<GrupaDto, Grupa>{

	@Autowired
	private IPreduzeceService preduzeceServiceIf;
	
	@Autowired
	private IPorezService porezServiceIf;

	@Override
	public Grupa convert(GrupaDto source) {
		
        Grupa grupa = new Grupa();
        
        grupa.setId(source.getId());
        grupa.setNaziv(source.getNazivGrupe());
        Preduzece preduzece = preduzeceServiceIf.findOne(source.getPreduzece());
        if(preduzece!=null){
            grupa.setPreduzece(preduzece);
        }
        Porez porez = porezServiceIf.findOne(source.getPorez());
        if(porez!=null){
            grupa.setPorez(porez);
        }
        return grupa;
	}
	
	
}
