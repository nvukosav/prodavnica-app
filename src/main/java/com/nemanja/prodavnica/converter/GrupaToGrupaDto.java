package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.GrupaDto;
import com.nemanja.prodavnica.model.Grupa;

@Component
public class GrupaToGrupaDto implements Converter<Grupa, GrupaDto> {

	@Override
	public GrupaDto convert(Grupa source) {
		GrupaDto grupaDto = new GrupaDto();

		grupaDto.setId(source.getId());
		grupaDto.setNazivGrupe(source.getNaziv());
		grupaDto.setPreduzece(source.getPreduzece().getId());
		grupaDto.setPorez(source.getPorez().getId());

		return grupaDto;
	}

	public List<GrupaDto> convert(List<Grupa> grupaList) {
		List<GrupaDto> grupaDtos = new ArrayList<>();
		for (Grupa gr : grupaList) {
			grupaDtos.add(convert(gr));
		}
		return grupaDtos;
	}
}
