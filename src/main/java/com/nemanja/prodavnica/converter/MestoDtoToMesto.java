package com.nemanja.prodavnica.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.MestoDto;
import com.nemanja.prodavnica.model.Mesto;

@Component
public class MestoDtoToMesto implements Converter<MestoDto, Mesto>{

	@Override
	public Mesto convert(MestoDto source) {
        Mesto mesto = new Mesto();
        
        mesto.setId(source.getId());
        mesto.setNaziv(source.getNaziv());
        mesto.setPostanskiBroj(source.getPostanskiBroj());
        mesto.setDrzava(source.getDrzava());
        
        return mesto;
	}

	
	
}
