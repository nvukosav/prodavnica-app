package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.MestoDto;
import com.nemanja.prodavnica.model.Mesto;

@Component
public class MestoToMestoDto implements Converter<Mesto, MestoDto>{

	@Override
	public MestoDto convert(Mesto source) {
		MestoDto mestoDto = new MestoDto();
		
		mestoDto.setId(source.getId());
		mestoDto.setNaziv(source.getNaziv());
		mestoDto.setPostanskiBroj(source.getPostanskiBroj());
		mestoDto.setDrzava(source.getDrzava());
		
		return mestoDto;
	}

	public List<MestoDto> convert(List<Mesto> mesta){
        List<MestoDto> mestoDtos = new ArrayList<>();
        for (Mesto m:mesta) {
            mestoDtos.add(convert(m));
        }
        return mestoDtos;
    }
	
}
