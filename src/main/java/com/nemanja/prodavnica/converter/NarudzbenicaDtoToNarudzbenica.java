package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.NarudzbenicaDto;
import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.PoslovnaGodina;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IPoslovnaGodinaService;
import com.nemanja.prodavnica.service.interfaces.IPoslovniPartnerService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

@Component
public class NarudzbenicaDtoToNarudzbenica implements Converter<NarudzbenicaDto, Narudzbenica> {

	
	@Autowired
	private IPreduzeceService preduzeceServiceIf;

	@Autowired
	private IPoslovniPartnerService poslovniPartnerIf;
	
	@Autowired
	private IPoslovnaGodinaService poslovnaGodIf;
	
	@Override
	public Narudzbenica convert(NarudzbenicaDto source) {
		Narudzbenica narudzbenica = new Narudzbenica();

        narudzbenica.setId(source.getId());
        narudzbenica.setBrojNarudzbenice(source.getBrojNarudzbenice());
        narudzbenica.setDatumNarudzbenice(source.getDatumNarudzbenice());
        narudzbenica.setTipNarudzbenice(source.getTipNarudzbenice());
        narudzbenica.setObrisano(false);

        Preduzece preduzece = preduzeceServiceIf.findOne(source.getPreduzece());
        if(preduzece != null){
            narudzbenica.setPreduzece(preduzece);
        }

        PoslovniPartner poslovniPartner = poslovniPartnerIf.findOne(source.getPoslovniPartner());
        if(poslovniPartner != null){
            narudzbenica.setPoslovniPartner(poslovniPartner);
        }

        System.out.println("POSLOVNA GODINA ID: " + source.getPoslovnaGodina());
        PoslovnaGodina poslovnaGodina = poslovnaGodIf.findOne(source.getPoslovnaGodina());
        if(poslovnaGodina != null){
            narudzbenica.setPoslovnaGodina(poslovnaGodina);
        }

        return narudzbenica;
	}

}
