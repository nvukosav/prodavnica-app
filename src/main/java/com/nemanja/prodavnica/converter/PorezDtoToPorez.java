package com.nemanja.prodavnica.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.PorezDto;
import com.nemanja.prodavnica.model.Porez;

@Component
public class PorezDtoToPorez implements Converter<PorezDto, Porez>{

	@Override
	public Porez convert(PorezDto source) {
		Porez porez = new Porez();
		
		porez.setId(source.getId());
		porez.setNaziv(source.getNazivPoreza());
		
		return porez;
	}

	
}
