package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.PorezDto;
import com.nemanja.prodavnica.model.Porez;

@Component
public class PorezToPorezDto implements Converter<Porez, PorezDto> {

	@Override
	public PorezDto convert(Porez source) {
		PorezDto porezDto = new PorezDto();
		
		porezDto.setId(source.getId());
		porezDto.setNazivPoreza(source.getNaziv());
		
		return porezDto;
	}

    public List<PorezDto> convert(List<Porez> porezList){
        List<PorezDto> PorezDtoList = new ArrayList<>();
        for (Porez p:porezList) {
            PorezDtoList.add(convert(p));
        }
        return PorezDtoList;
    }
}
