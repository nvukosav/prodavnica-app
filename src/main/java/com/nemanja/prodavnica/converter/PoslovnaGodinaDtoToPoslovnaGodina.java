package com.nemanja.prodavnica.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.PoslovnaGodinaDto;
import com.nemanja.prodavnica.model.PoslovnaGodina;

@Component
public class PoslovnaGodinaDtoToPoslovnaGodina implements Converter<PoslovnaGodinaDto, PoslovnaGodina> {

	@Override
	public PoslovnaGodina convert(PoslovnaGodinaDto source) {
		PoslovnaGodina poslovnaGodina = new PoslovnaGodina();
		
		poslovnaGodina.setId(source.getId());
		poslovnaGodina.setGodina(source.getGodina());
		poslovnaGodina.setZakljucena(source.isZakljucena());
		
		return poslovnaGodina;
	}

}
