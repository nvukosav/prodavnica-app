package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.PoslovnaGodinaDto;
import com.nemanja.prodavnica.model.PoslovnaGodina;

@Component
public class PoslovnaGodinatoPoslovnaGodinaDto implements Converter<PoslovnaGodina, PoslovnaGodinaDto>{

	@Override
	public PoslovnaGodinaDto convert(PoslovnaGodina source) {
		return new PoslovnaGodinaDto(source.getId(), source.getGodina(), source.isZakljucena());
		
		
	}

    public List<PoslovnaGodinaDto> convert(List<PoslovnaGodina> poslovneGodine){
        List<PoslovnaGodinaDto> poslovnaGodinaDtoList = new ArrayList<>();
        for (PoslovnaGodina pg:poslovneGodine) {
            poslovnaGodinaDtoList.add(convert(pg));
        }
        return poslovnaGodinaDtoList;
    }
	
}
