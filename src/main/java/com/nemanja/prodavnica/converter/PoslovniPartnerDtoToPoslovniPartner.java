package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.nemanja.prodavnica.dto.PoslovniPartnerDto;
import com.nemanja.prodavnica.model.Mesto;
import com.nemanja.prodavnica.model.PoslovniPartner;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IMestoService;
import com.nemanja.prodavnica.service.interfaces.IPreduzeceService;

@Component
public class PoslovniPartnerDtoToPoslovniPartner implements Converter<PoslovniPartnerDto, PoslovniPartner>{
	
	@Autowired
	IMestoService mestoService;
	
	@Autowired
	IPreduzeceService preduzeceService;
	
	@Override
	public PoslovniPartner convert(PoslovniPartnerDto source) {
		Mesto mesto = mestoService.findOne(source.getMesto());
		Preduzece preduzece = preduzeceService.findOne(source.getPreduzece());
		PoslovniPartner poslovniPartner = new PoslovniPartner();
		poslovniPartner.setNaziv(source.getNazivPartnera());
		poslovniPartner.setAdresa(source.getAdresa());
		poslovniPartner.setVrstaPartnera(source.getVrstaPartnera());
		poslovniPartner.setBrojRacuna(source.getBrojRacuna());
		poslovniPartner.setPib(source.getPib());
		poslovniPartner.setMb(source.getMb());
		poslovniPartner.setId(source.getId());
		if (mesto!=null) {
			poslovniPartner.setMesto(mesto);
		}
		if (preduzece!=null) {
			poslovniPartner.setPreduzece(preduzece);
		}
		return poslovniPartner;
	}
	

}
