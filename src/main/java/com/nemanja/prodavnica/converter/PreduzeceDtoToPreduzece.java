package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.PreduzeceDto;
import com.nemanja.prodavnica.model.Mesto;
import com.nemanja.prodavnica.model.Preduzece;
import com.nemanja.prodavnica.service.interfaces.IMestoService;

@Component
public class PreduzeceDtoToPreduzece implements Converter<PreduzeceDto, Preduzece> {

	@Autowired
	IMestoService mestoService;

	@Override
	public Preduzece convert(PreduzeceDto source) {
		Mesto mesto = mestoService.findOne(source.getMesto());
		Preduzece preduzece = new Preduzece();
		preduzece.setNaziv(source.getNaziv());
		preduzece.setAdresa(source.getAdresaPreduzeca());
		preduzece.setPib(source.getPib());
		preduzece.setMb(source.getMb());
		preduzece.setTel(source.getTelefon());
		preduzece.setEmail(source.getEmail());
		preduzece.setBrojRacuna(source.getBrojRacuna());
		preduzece.setId(source.getId());
		if (mesto!=null) {
			preduzece.setMesto(mesto);
		}
		return preduzece;
	}
	
	
}
