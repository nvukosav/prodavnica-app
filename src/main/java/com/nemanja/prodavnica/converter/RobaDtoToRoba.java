package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.RobaDto;
import com.nemanja.prodavnica.model.Grupa;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.service.interfaces.IGrupaService;

@Component
public class RobaDtoToRoba implements Converter<RobaDto, Roba>{

	@Autowired
	IGrupaService grupaService;
	
	@Override
	public Roba convert(RobaDto source) {
		Grupa grupa = grupaService.findOne(source.getGrupa());
		
		Roba roba = new Roba();
		if (source.getId()!=null) roba.setId(source.getId());
		roba.setNaziv(source.getNazivRobe());
		roba.setJedinicaMere(source.getJedinicaMere());
		if (grupa!=null) {
			roba.setGrupa(grupa);
		}
		return roba;
	}


	
	
}
