package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.RobaDto;
import com.nemanja.prodavnica.model.Roba;

@Component
public class RobaToRobaDto implements Converter<Roba, RobaDto> {

	@Override
	public RobaDto convert(Roba source) {
		return new RobaDto(source.getId(), source.getNaziv(), source.getJedinicaMere(), source.getGrupa().getId());
	}

	public List<RobaDto> convert(List<Roba> robaUsluge){
		List<RobaDto> retVal = new ArrayList<RobaDto>();
		for (Roba roba : robaUsluge) {
			retVal.add(convert(roba));
		}
		return retVal;	
	}
	
}
