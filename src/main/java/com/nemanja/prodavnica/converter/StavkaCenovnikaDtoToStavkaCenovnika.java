package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaCenovnikaDto;
import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.service.interfaces.ICenovnikService;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@Component
public class StavkaCenovnikaDtoToStavkaCenovnika implements Converter<StavkaCenovnikaDto, StavkaCenovnika>{

	@Autowired
	ICenovnikService cenovnikService;
	
	@Autowired
	IRobaService robaService;
	
	@Override
	public StavkaCenovnika convert(StavkaCenovnikaDto source) {
		Cenovnik cenovnik = cenovnikService.findOne(source.getCenovnik());
		Roba roba = robaService.findOne(source.getRoba());
		StavkaCenovnika stavka = new StavkaCenovnika();
		stavka.setCena(source.getCena());
		if (roba!=null) {
			stavka.setRoba(roba);
		}
		if (cenovnik!=null) {
			stavka.setCenovnik(cenovnik);
		}
		return stavka;
	}

	
	
}
