package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaCenovnikaDto;
import com.nemanja.prodavnica.model.StavkaCenovnika;

@Component
public class StavkaCenovnikaToStavkaCenovnikaDto implements Converter<StavkaCenovnika, StavkaCenovnikaDto> {

	@Override
	public StavkaCenovnikaDto convert(StavkaCenovnika source) {
		return new StavkaCenovnikaDto(source.getId(), source.getCena(), source.getCenovnik().getId(), source.getRoba().getId());
	}

	public List<StavkaCenovnikaDto> convert(List<StavkaCenovnika> stavke){
		List<StavkaCenovnikaDto> retVal = new ArrayList<StavkaCenovnikaDto>();
		for (StavkaCenovnika stavka : stavke) {
			retVal.add(convert(stavka));
		}
		return retVal;	
	}
	
	
}
