package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaFaktureDto;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaFakture;
import com.nemanja.prodavnica.service.interfaces.IFakturaService;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@Component
public class StavkaFaktureDtoToStavkaFakture implements Converter<StavkaFaktureDto, StavkaFakture> {

	@Autowired
	private IFakturaService fakturaService;
	
	@Autowired
	private IRobaService robaService;

	@Override
	public StavkaFakture convert(StavkaFaktureDto source) {
		Faktura f = fakturaService.findOne(source.getFaktura());
		Roba r = robaService.findOne(source.getRoba());
		
		StavkaFakture stavka = new StavkaFakture();
		stavka.setKolicina(source.getKolicina());
		stavka.setCena(source.getCena());
		stavka.setRabat(source.getRabat());
		stavka.setPorezOsnovica(source.getOsnovicaZaPorez());
		stavka.setStopaPoreza(source.getStopaPoreza());
		stavka.setIznosPoreza(source.getIznosPorez());
		stavka.setIznos(source.getIznosStavka());
		if (f!=null) {
			stavka.setFaktura(f);
		}
		if (r!=null) {
			stavka.setRoba(r);
		}
		return stavka;
	}

}
