package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaFaktureDto;
import com.nemanja.prodavnica.model.StavkaFakture;

@Component
public class StavkaFaktureToStavkaFaktureDto implements Converter<StavkaFakture, StavkaFaktureDto>{

	@Override
	public StavkaFaktureDto convert(StavkaFakture source) {
		return new StavkaFaktureDto(source.getId(), source.getKolicina(), source.getCena(), source.getRabat(), source.getPorezOsnovica(), source.getStopaPoreza(), source.getIznosPoreza(), source.getIznos(), source.getFaktura().getId(), source.getRoba().getId());
	}

	public List<StavkaFaktureDto> convert(List<StavkaFakture> stavke){
		
		List<StavkaFaktureDto> retVal = new ArrayList<StavkaFaktureDto>();
		
		for (StavkaFakture stavka : stavke) {
			retVal.add(convert(stavka));
		}
		
		return retVal;	
	}
	
	
}
