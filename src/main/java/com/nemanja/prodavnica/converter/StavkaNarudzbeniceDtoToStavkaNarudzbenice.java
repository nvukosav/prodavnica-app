package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaNarudzbeniceDto;
import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaNarudzbenice;
import com.nemanja.prodavnica.service.interfaces.INarudzbenicaService;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@Component
public class StavkaNarudzbeniceDtoToStavkaNarudzbenice implements Converter<StavkaNarudzbeniceDto, StavkaNarudzbenice> {

	@Autowired
	INarudzbenicaService narudzbenicaService;

	@Autowired
	IRobaService robaService;

	@Override
	public StavkaNarudzbenice convert(StavkaNarudzbeniceDto source) {

		Narudzbenica narudzbenica = narudzbenicaService.findOne(source.getNarudzbenica());
		Roba roba = robaService.findOne(source.getRoba());

		StavkaNarudzbenice stavkaNarudzbenice = new StavkaNarudzbenice();
		stavkaNarudzbenice.setOpis(source.getOpisRobe());
		stavkaNarudzbenice.setJedinicaMere(source.getJedinicaMere());
		stavkaNarudzbenice.setKolicina(source.getKolicina());

		if (narudzbenica != null) {
			stavkaNarudzbenice.setNarudzbenica(narudzbenica);
		}

		if (roba != null) {
			stavkaNarudzbenice.setRoba(roba);
		}

		return stavkaNarudzbenice;
	}

}
