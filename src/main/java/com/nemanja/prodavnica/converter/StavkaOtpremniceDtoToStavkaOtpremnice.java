package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaOtpremniceDto;
import com.nemanja.prodavnica.model.Otpremnica;
import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.model.StavkaOtpremnice;
import com.nemanja.prodavnica.service.interfaces.IOtpremnicaService;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@Component
public class StavkaOtpremniceDtoToStavkaOtpremnice implements Converter<StavkaOtpremniceDto, StavkaOtpremnice> {

	@Autowired
	private IOtpremnicaService otpremnicaService;
	
	@Autowired
	IRobaService robaService;
	
	@Override
	public StavkaOtpremnice convert(StavkaOtpremniceDto source) {
        Otpremnica otpremnica = otpremnicaService.findOne(source.getOtpremnica());
        Roba roba = robaService.findOne(source.getRoba());

        StavkaOtpremnice stavkaOtpremnice = new StavkaOtpremnice();
        stavkaOtpremnice.setOpis(roba.getNaziv());
        stavkaOtpremnice.setJedinicaMere(source.getJedinicaMere());
        stavkaOtpremnice.setKolicina(source.getKolicina());
        stavkaOtpremnice.setCena(source.getCena());
        stavkaOtpremnice.setIznos(source.getIznos());
        stavkaOtpremnice.setObrisano(false);

        if (otpremnica != null) {
            stavkaOtpremnice.setOtpremnica(otpremnica);
        }

        if (roba != null) {
            stavkaOtpremnice.setRoba(roba);
        }

        return stavkaOtpremnice;
    }

}
