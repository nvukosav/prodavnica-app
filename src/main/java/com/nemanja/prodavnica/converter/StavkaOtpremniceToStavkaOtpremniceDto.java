package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StavkaOtpremniceDto;
import com.nemanja.prodavnica.model.StavkaOtpremnice;

@Component
public class StavkaOtpremniceToStavkaOtpremniceDto implements Converter<StavkaOtpremnice, StavkaOtpremniceDto>{

	@Override
	public StavkaOtpremniceDto convert(StavkaOtpremnice source) {
		return new StavkaOtpremniceDto(source.getId(), source.getJedinicaMere(), source.getKolicina(),
				source.getCena(), source.getIznos(), source.getOpis(), source.getRoba().getId(), source.getOtpremnica().getId(), source.isObrisano());
	}

    public List<StavkaOtpremniceDto> convert(List<StavkaOtpremnice> stavkaOtpremnice) {

        List<StavkaOtpremniceDto> stavkeOtpremniceDto = new ArrayList<>();

        for (StavkaOtpremnice stavke : stavkaOtpremnice) {
            StavkaOtpremniceDto stavkaOtpremniceDto = convert(stavke);
            stavkeOtpremniceDto.add(stavkaOtpremniceDto);
        }

        return stavkeOtpremniceDto;
    }
	
}
