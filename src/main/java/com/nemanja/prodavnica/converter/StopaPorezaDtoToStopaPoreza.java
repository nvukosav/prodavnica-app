package com.nemanja.prodavnica.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StopaPorezaDto;
import com.nemanja.prodavnica.model.Porez;
import com.nemanja.prodavnica.model.StopaPoreza;
import com.nemanja.prodavnica.service.interfaces.IPorezService;

@Component
public class StopaPorezaDtoToStopaPoreza implements Converter<StopaPorezaDto, StopaPoreza> {

	@Autowired
	IPorezService porezService;
	
	@Override
	public StopaPoreza convert(StopaPorezaDto source) {
		Porez porez = porezService.findOne(source.getPorez());
		StopaPoreza stopa = new StopaPoreza();
		stopa.setProcenat(source.getProcenat());
		stopa.setDatumVazenja(source.getDatumVazenja());
		if (porez!=null) {
			stopa.setPorez(porez);
		}
		return stopa;
	}

}
