package com.nemanja.prodavnica.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.nemanja.prodavnica.dto.StopaPorezaDto;
import com.nemanja.prodavnica.model.StopaPoreza;

@Component
public class StopaPorezaToStopaPorezaDto implements Converter<StopaPoreza, StopaPorezaDto>{

	@Override
	public StopaPorezaDto convert(StopaPoreza source) {
		return new StopaPorezaDto(source.getId(), source.getProcenat(), source.getDatumVazenja(), source.getPorez().getId());
	}

	public List<StopaPorezaDto> convert(List<StopaPoreza> stope){
		
		List<StopaPorezaDto> retVal = new ArrayList<StopaPorezaDto>();
		
		for (StopaPoreza stopa : stope) {
			retVal.add(convert(stopa));
		}
		
		return retVal;	
	}
	
}
