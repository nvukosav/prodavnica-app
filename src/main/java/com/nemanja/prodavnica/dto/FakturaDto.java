package com.nemanja.prodavnica.dto;

import java.util.Date;

public class FakturaDto {

	private long id;
	private long brFakture;
	private Date datumFakture;
	private Date datumValute;
	private double iznosBezRabata;
	private double rabat;
	private double osnovica;
	private double ukupanPorez;
	private double iznosZaPlacanje;
	private boolean placeno;
	private int vrstaFakture;
	private long preduzece;
	private long poslovniPartner;
	private long poslovnaGodina;
	
	public FakturaDto() {
		
	}

	public FakturaDto(long id, long brFakture, Date datumFakture, Date datumValute, double iznosBezRabata, double rabat,
			double osnovica, double ukupanPorez, double iznosZaPlacanje, boolean placeno, int vrstaFakture,
			long preduzece, long poslovniPartner, long poslovnaGodina) {
		super();
		this.id = id;
		this.brFakture = brFakture;
		this.datumFakture = datumFakture;
		this.datumValute = datumValute;
		this.iznosBezRabata = iznosBezRabata;
		this.rabat = rabat;
		this.osnovica = osnovica;
		this.ukupanPorez = ukupanPorez;
		this.iznosZaPlacanje = iznosZaPlacanje;
		this.placeno = placeno;
		this.vrstaFakture = vrstaFakture;
		this.preduzece = preduzece;
		this.poslovniPartner = poslovniPartner;
		this.poslovnaGodina = poslovnaGodina;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBrFakture() {
		return brFakture;
	}

	public void setBrFakture(long brFakture) {
		this.brFakture = brFakture;
	}

	public Date getDatumFakture() {
		return datumFakture;
	}

	public void setDatumFakture(Date datumFakture) {
		this.datumFakture = datumFakture;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public double getIznosBezRabata() {
		return iznosBezRabata;
	}

	public void setIznosBezRabata(double iznosBezRabata) {
		this.iznosBezRabata = iznosBezRabata;
	}

	public double getRabat() {
		return rabat;
	}

	public void setRabat(double rabat) {
		this.rabat = rabat;
	}

	public double getOsnovica() {
		return osnovica;
	}

	public void setOsnovica(double osnovica) {
		this.osnovica = osnovica;
	}

	public double getUkupanPorez() {
		return ukupanPorez;
	}

	public void setUkupanPorez(double ukupanPorez) {
		this.ukupanPorez = ukupanPorez;
	}

	public double getIznosZaPlacanje() {
		return iznosZaPlacanje;
	}

	public void setIznosZaPlacanje(double iznosZaPlacanje) {
		this.iznosZaPlacanje = iznosZaPlacanje;
	}

	public boolean isPlaceno() {
		return placeno;
	}

	public void setPlaceno(boolean placeno) {
		this.placeno = placeno;
	}

	public int getVrstaFakture() {
		return vrstaFakture;
	}

	public void setVrstaFakture(int vrstaFakture) {
		this.vrstaFakture = vrstaFakture;
	}

	public long getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(long preduzece) {
		this.preduzece = preduzece;
	}

	public long getPoslovniPartner() {
		return poslovniPartner;
	}

	public void setPoslovniPartner(long poslovniPartner) {
		this.poslovniPartner = poslovniPartner;
	}

	public long getPoslovnaGodina() {
		return poslovnaGodina;
	}

	public void setPoslovnaGodina(long poslovnaGodina) {
		this.poslovnaGodina = poslovnaGodina;
	}
	
	
	
	
}
