package com.nemanja.prodavnica.dto;

public class GrupaDto {

	private long id;
	private String nazivGrupe;
	private long preduzece;
	private long porez;
	
	public GrupaDto() {}

	public GrupaDto(long id, String nazivGrupe, long preduzece, long porez) {
		super();
		this.id = id;
		this.nazivGrupe = nazivGrupe;
		this.preduzece = preduzece;
		this.porez = porez;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazivGrupe() {
		return nazivGrupe;
	}

	public void setNazivGrupe(String nazivGrupe) {
		this.nazivGrupe = nazivGrupe;
	}

	public long getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(long preduzece) {
		this.preduzece = preduzece;
	}

	public long getPorez() {
		return porez;
	}

	public void setPorez(long porez) {
		this.porez = porez;
	}
	
	
}

