package com.nemanja.prodavnica.dto;

public class PorezDto {

	private long id;
	private String nazivPoreza;
	
	public PorezDto() {}
	
	public PorezDto(int id, String nazivPorez) {
		super();
		this.id = id;
		this.nazivPoreza = nazivPorez;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazivPoreza() {
		return nazivPoreza;
	}

	public void setNazivPoreza(String nazivPoreza) {
		this.nazivPoreza = nazivPoreza;
	}
	
	
}
