package com.nemanja.prodavnica.dto;

public class PoslovnaGodinaDto {

	private long id;
	private int godina;
	private boolean zakljucena;
	
	public PoslovnaGodinaDto() {}

	public PoslovnaGodinaDto(long id, int godina, boolean zakljucena) {
		super();
		this.id = id;
		this.godina = godina;
		this.zakljucena = zakljucena;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getGodina() {
		return godina;
	}

	public void setGodina(int godina) {
		this.godina = godina;
	}

	public boolean isZakljucena() {
		return zakljucena;
	}

	public void setZakljucena(boolean zakljucena) {
		this.zakljucena = zakljucena;
	}
	
}
