package com.nemanja.prodavnica.dto;

public class PoslovniPartnerDto {

	private long id;
	private String nazivPartnera;
	private String adresa;
	private int vrstaPartnera;
	private String brojRacuna;
	private String pib;
	private String mb;
	private long preduzece;
	private long mesto;
	
	public PoslovniPartnerDto() {}

	public PoslovniPartnerDto(long id, String nazivPartnera, String adresa, int vrstaPartnera, String brojRacuna,
			String pib, String mb, long preduzece, long mesto) {
		super();
		this.id = id;
		this.nazivPartnera = nazivPartnera;
		this.adresa = adresa;
		this.vrstaPartnera = vrstaPartnera;
		this.brojRacuna = brojRacuna;
		this.pib = pib;
		this.mb = mb;
		this.preduzece = preduzece;
		this.mesto = mesto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazivPartnera() {
		return nazivPartnera;
	}

	public void setNazivPartnera(String nazivPartnera) {
		this.nazivPartnera = nazivPartnera;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getVrstaPartnera() {
		return vrstaPartnera;
	}

	public void setVrstaPartnera(int vrstaPartnera) {
		this.vrstaPartnera = vrstaPartnera;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public String getPib() {
		return this.pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public long getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(long preduzece) {
		this.preduzece = preduzece;
	}

	public long getMesto() {
		return mesto;
	}

	public void setMesto(long mesto) {
		this.mesto = mesto;
	}
	
	
	
}
