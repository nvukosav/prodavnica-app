package com.nemanja.prodavnica.dto;

public class PreduzeceDto {

	private long id;
	private String naziv;
	private String adresaPreduzeca;
	private String pib;
	private String mb;
	private String telefon;
	private String email;
	private String brojRacuna;
	private long mesto;
	
	public PreduzeceDto() {}

	public PreduzeceDto(long id, String naziv, String adresaPreduzeca, String pib, String mb, String telefon, String email,
			String brojRacuna, long mesto) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.adresaPreduzeca = adresaPreduzeca;
		this.pib = pib;
		this.mb = mb;
		this.telefon = telefon;
		this.email = email;
		this.brojRacuna = brojRacuna;
		this.mesto = mesto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresaPreduzeca() {
		return adresaPreduzeca;
	}

	public void setAdresaPreduzeca(String adresaPreduzeca) {
		this.adresaPreduzeca = adresaPreduzeca;
	}

	public String getPib() {
		return this.pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBrojRacuna() {
		return this.brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public long getMesto() {
		return mesto;
	}

	public void setMesto(long mesto) {
		this.mesto = mesto;
	}
	
	
	
}
