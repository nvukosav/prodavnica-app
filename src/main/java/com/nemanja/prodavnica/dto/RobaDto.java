package com.nemanja.prodavnica.dto;

public class RobaDto {

	private Long id;
	private String nazivRobe;
	private String jedinicaMere;
	private Long grupa;
	
	public RobaDto() {}

	public RobaDto(Long id, String nazivRobe, String jedinicaMere, Long grupa) {
		super();
		this.id = id;
		this.nazivRobe = nazivRobe;
		this.jedinicaMere = jedinicaMere;
		this.grupa = grupa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivRobe() {
		return nazivRobe;
	}

	public void setNazivRobe(String nazivRobe) {
		this.nazivRobe = nazivRobe;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public Long getGrupa() {
		return grupa;
	}

	public void setGrupa(Long grupa) {
		this.grupa = grupa;
	}
	
	
}
