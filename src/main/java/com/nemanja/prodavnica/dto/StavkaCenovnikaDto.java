package com.nemanja.prodavnica.dto;

public class StavkaCenovnikaDto {

	private long id;
	private float cena;
	private long cenovnik;
	private long roba;
	
	public StavkaCenovnikaDto() {}

	public StavkaCenovnikaDto(long id, float cena, long cenovnik, long roba) {
		super();
		this.id = id;
		this.cena = cena;
		this.cenovnik = cenovnik;
		this.roba = roba;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public long getCenovnik() {
		return cenovnik;
	}

	public void setCenovnik(long cenovnik) {
		this.cenovnik = cenovnik;
	}

	public long getRoba() {
		return roba;
	}

	public void setRoba(long roba) {
		this.roba = roba;
	}
	
	
	
}
