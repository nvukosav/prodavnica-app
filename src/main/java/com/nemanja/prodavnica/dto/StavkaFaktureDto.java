package com.nemanja.prodavnica.dto;

public class StavkaFaktureDto {

	private long id;
	private long kolicina;
	private float cena;
	private double rabat;
	private double osnovicaZaPorez;
	private double stopaPoreza;
	private double iznosPorez;
	private double iznosStavka;
	private long faktura;
	private long roba;
	
	public StavkaFaktureDto() {}

	public StavkaFaktureDto(long id, long kolicina, float cena, double rabat, double osnovicaZaPorez,
			double stopaPoreza, double iznosPorez, double inosStavka, long faktura, long roba) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.cena = cena;
		this.rabat = rabat;
		this.osnovicaZaPorez = osnovicaZaPorez;
		this.stopaPoreza = stopaPoreza;
		this.iznosPorez = iznosPorez;
		this.iznosStavka = inosStavka;
		this.faktura = faktura;
		this.roba = roba;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getKolicina() {
		return kolicina;
	}

	public void setKolicina(long kolicina) {
		this.kolicina = kolicina;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public double getRabat() {
		return rabat;
	}

	public void setRabat(double rabat) {
		this.rabat = rabat;
	}

	public double getOsnovicaZaPorez() {
		return osnovicaZaPorez;
	}

	public void setOsnovicaZaPorez(double osnovicaZaPorez) {
		this.osnovicaZaPorez = osnovicaZaPorez;
	}

	public double getStopaPoreza() {
		return stopaPoreza;
	}

	public void setStopaPoreza(double stopaPoreza) {
		this.stopaPoreza = stopaPoreza;
	}

	public double getIznosPorez() {
		return iznosPorez;
	}

	public void setIznosPorez(double iznosPorez) {
		this.iznosPorez = iznosPorez;
	}

	public double getIznosStavka() {
		return iznosStavka;
	}

	public void setIznosStavka(double iznosStavka) {
		this.iznosStavka = iznosStavka;
	}

	public long getFaktura() {
		return faktura;
	}

	public void setFaktura(long faktura) {
		this.faktura = faktura;
	}

	public long getRoba() {
		return roba;
	}

	public void setRoba(long roba) {
		this.roba = roba;
	}
	
	
	
}
