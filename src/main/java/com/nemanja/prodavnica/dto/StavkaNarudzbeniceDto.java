package com.nemanja.prodavnica.dto;

public class StavkaNarudzbeniceDto {

	private long id;
	private String jedinicaMere;
	private int kolicina;
	private String opisRobe;
	private long roba;
	private long narudzbenica;
	private boolean obrisano;
	
	public StavkaNarudzbeniceDto() {}

	public StavkaNarudzbeniceDto(long id, String jedinicaMere, int kolicina, String opisRobe, long roba,
			long narudzbenica, boolean obrisano) {
		super();
		this.id = id;
		this.jedinicaMere = jedinicaMere;
		this.kolicina = kolicina;
		this.opisRobe = opisRobe;
		this.roba = roba;
		this.narudzbenica = narudzbenica;
		this.obrisano = obrisano;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public String getOpisRobe() {
		return opisRobe;
	}

	public void setOpisRobe(String opisRobe) {
		this.opisRobe = opisRobe;
	}

	public long getRoba() {
		return roba;
	}

	public void setRoba(long roba) {
		this.roba = roba;
	}

	public long getNarudzbenica() {
		return narudzbenica;
	}

	public void setNarudzbenica(long narudzbenica) {
		this.narudzbenica = narudzbenica;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
}
