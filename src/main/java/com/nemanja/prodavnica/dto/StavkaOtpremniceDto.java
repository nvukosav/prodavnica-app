package com.nemanja.prodavnica.dto;

public class StavkaOtpremniceDto {

	private long id;
	private String jedinicaMere;
	private int kolicina;
	private float  cena;
	private float iznos;
	private String opisRobe;
	private long roba;
	private long otpremnica;
	private boolean obrisano;
	
	public StavkaOtpremniceDto() {}

	public StavkaOtpremniceDto(long id, String jedinicaMere, int kolicina, float cena, float iznos, String opisRobe,
			long roba, long otpremnica, boolean obrisano) {
		super();
		this.id = id;
		this.jedinicaMere = jedinicaMere;
		this.kolicina = kolicina;
		this.cena = cena;
		this.iznos = iznos;
		this.opisRobe = opisRobe;
		this.roba = roba;
		this.otpremnica = otpremnica;
		this.obrisano = obrisano;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public float getIznos() {
		return iznos;
	}

	public void setIznos(float iznos) {
		this.iznos = iznos;
	}

	public String getOpisRobe() {
		return opisRobe;
	}

	public void setOpisRobe(String opisRobe) {
		this.opisRobe = opisRobe;
	}

	public long getRoba() {
		return roba;
	}

	public void setRoba(long roba) {
		this.roba = roba;
	}

	public long getOtpremnica() {
		return otpremnica;
	}

	public void setOtpremnica(long otpremnica) {
		this.otpremnica = otpremnica;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
}
