package com.nemanja.prodavnica.dto;

import java.util.Date;

public class StopaPorezaDto {

	private long id;
	private float procenat;
	private Date datumVazenja;
	private long porez;
	
	public StopaPorezaDto() {}

	public StopaPorezaDto(long id, float procenat, Date datumVazenja, long porez) {
		super();
		this.id = id;
		this.procenat = procenat;
		this.datumVazenja = datumVazenja;
		this.porez = porez;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getProcenat() {
		return procenat;
	}

	public void setProcenat(float procenat) {
		this.procenat = procenat;
	}

	public Date getDatumVazenja() {
		return datumVazenja;
	}

	public void setDatumVazenja(Date datumVazenja) {
		this.datumVazenja = datumVazenja;
	}

	public long getPorez() {
		return porez;
	}

	public void setPorez(long porez) {
		this.porez = porez;
	}
	
	
}
