package com.nemanja.prodavnica.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Grupa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String naziv;
	
	@OneToMany(mappedBy="grupa", cascade = CascadeType.ALL)
	private Set<Roba> roba = new HashSet<>();
	
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="preduzece_id")
	private Preduzece preduzece;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "porez_id")
	private Porez porez;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "stopaporeza_id")
	private StopaPoreza stopaporeza;
	
	private boolean obrisano;

	public Grupa(String naziv, Set<Roba> roba, Preduzece preduzece, Porez porez) {
		super();
		this.naziv = naziv;
		this.roba = roba;
		this.preduzece = preduzece;
		this.porez = porez;
		this.obrisano = false;
	}

	public Grupa() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Roba> getRoba() {
		return roba;
	}

	public void setRoba(Set<Roba> roba) {
		this.roba = roba;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}

	public Porez getPorez() {
		return porez;
	}

	public void setPorez(Porez porez) {
		this.porez = porez;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}

	public StopaPoreza getStopaporeza() {
		return stopaporeza;
	}

	public void setStopaporeza(StopaPoreza stopaporeza) {
		this.stopaporeza = stopaporeza;
	}
	
	

}
