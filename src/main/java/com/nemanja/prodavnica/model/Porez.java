package com.nemanja.prodavnica.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Porez {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String naziv;
	
	@OneToMany(mappedBy = "porez", cascade = CascadeType.ALL)
	private Set<StopaPoreza> stopePoreza = new HashSet<>();
	
	@OneToMany(mappedBy = "porez", cascade = CascadeType.ALL)
	private Set<Grupa> grupa = new HashSet<>();
	
	private boolean obrisano;
	
	public Porez() {}
	
	

	public Porez(String naziv, Set<StopaPoreza> stopePoreza, Set<Grupa> grupa) {
		super();
		this.naziv = naziv;
		this.stopePoreza = stopePoreza;
		this.grupa = grupa;
		this.obrisano = false;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<StopaPoreza> getStopePoreza() {
		return stopePoreza;
	}

	public void setStopePoreza(Set<StopaPoreza> stopePoreza) {
		this.stopePoreza = stopePoreza;
	}

	public Set<Grupa> getGrupa() {
		return grupa;
	}

	public void setGrupa(Set<Grupa> grupa) {
		this.grupa = grupa;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
}
