package com.nemanja.prodavnica.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class PoslovniPartner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String naziv;
	
	private String adresa;

	private int vrstaPartnera; // 0 = kupac; 1 = prodavac
	
	private String brojRacuna;
	
	private String pib;
	
	private String mb;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name= "preduzece_id")
	private Preduzece preduzece;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="mesto_id")
	public Mesto mesto;
	
	@OneToMany(mappedBy = "poslovniPartner", cascade = CascadeType.ALL)
	private Set<Faktura> fakture = new HashSet<>();
	
    @OneToMany(mappedBy = "poslovniPartner", cascade = CascadeType.ALL)
	private Set<Otpremnica> optremnice = new HashSet<>();
    
    @OneToMany(mappedBy = "poslovniPartner", cascade = CascadeType.ALL)
	private Set<Narudzbenica> narudzbenice = new HashSet<>();
    
	@OneToMany(mappedBy = "poslovniPartner", cascade = CascadeType.ALL)
	private Set<Cenovnik> cenovnici = new HashSet<>();
	
	private boolean obrisano;
	
	public PoslovniPartner() {}

	public PoslovniPartner(String naziv, String adresa, int vrstaPartnera, String brojRacuna, String pib,
			String mb, Preduzece preduzece, Mesto mesto, Set<Faktura> fakture, Set<Otpremnica> optremnice,
			Set<Narudzbenica> narudzbenice, Set<Cenovnik> cenovnici) {
		super();
		this.naziv = naziv;
		this.adresa = adresa;
		this.vrstaPartnera = vrstaPartnera;
		this.brojRacuna = brojRacuna;
		this.pib = pib;
		this.mb = mb;
		this.preduzece = preduzece;
		this.mesto = mesto;
		this.fakture = fakture;
		this.optremnice = optremnice;
		this.narudzbenice = narudzbenice;
		this.cenovnici = cenovnici;
		this.obrisano = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getVrstaPartnera() {
		return vrstaPartnera;
	}

	public void setVrstaPartnera(int vrstaPartnera) {
		this.vrstaPartnera = vrstaPartnera;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public String getPib() {
		return this.pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}

	public Mesto getMesto() {
		return mesto;
	}

	public void setMesto(Mesto mesto) {
		this.mesto = mesto;
	}

	public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}

	public Set<Otpremnica> getOptremnice() {
		return optremnice;
	}

	public void setOptremnice(Set<Otpremnica> optremnice) {
		this.optremnice = optremnice;
	}

	public Set<Narudzbenica> getNarudzbenice() {
		return narudzbenice;
	}

	public void setNarudzbenice(Set<Narudzbenica> narudzbenice) {
		this.narudzbenice = narudzbenice;
	}

	public Set<Cenovnik> getCenovnici() {
		return cenovnici;
	}

	public void setCenovnici(Set<Cenovnik> cenovnici) {
		this.cenovnici = cenovnici;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
	

	
}
