package com.nemanja.prodavnica.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Preduzece {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String naziv;
	
	private String adresa;
	
	private String pib;
	
	private String mb;
	
	private String tel;
	
	private String email;
	
	private String brojRacuna;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="mesto_id")
	private Mesto mesto;
	
	@OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<Faktura> fakture = new HashSet<>();
	
    @OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<Otpremnica> optremnice = new HashSet<>();
    
    @OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<Narudzbenica> narudzbenice = new HashSet<>();
	
    @OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<PoslovniPartner> poslovniPartneri = new HashSet<>();
	
	@OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<Cenovnik> cenovnici = new HashSet<>();
	
    @OneToMany(mappedBy = "preduzece", cascade = CascadeType.ALL)
	private Set<Grupa> grupe = new HashSet<>();
	
	private boolean obrisano;

	public Preduzece(String naziv, String adresa, String pib, String mb, String tel, String email, String brojRacuna,
			Mesto mesto, Set<Faktura> fakture, Set<Otpremnica> optremnice, Set<Narudzbenica> narudzbenice,
			Set<PoslovniPartner> poslovniPartneri, Set<Cenovnik> cenovnici, Set<Grupa> grupe) {
		super();
		this.naziv = naziv;
		this.adresa = adresa;
		this.pib = pib;
		this.mb = mb;
		this.tel = tel;
		this.email = email;
		this.brojRacuna = brojRacuna;
		this.mesto = mesto;
		this.fakture = fakture;
		this.optremnice = optremnice;
		this.narudzbenice = narudzbenice;
		this.poslovniPartneri = poslovniPartneri;
		this.cenovnici = cenovnici;
		this.grupe = grupe;
		this.obrisano = false;
	}

	public Preduzece() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getPib() {
		return this.pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Mesto getMesto() {
		return mesto;
	}

	public void setMesto(Mesto mesto) {
		this.mesto = mesto;
	}

	public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}

	public Set<Otpremnica> getOptremnice() {
		return optremnice;
	}

	public void setOptremnice(Set<Otpremnica> optremnice) {
		this.optremnice = optremnice;
	}

	public Set<Narudzbenica> getNarudzbenice() {
		return narudzbenice;
	}

	public void setNarudzbenice(Set<Narudzbenica> narudzbenice) {
		this.narudzbenice = narudzbenice;
	}

	public Set<PoslovniPartner> getPoslovniPartneri() {
		return poslovniPartneri;
	}

	public void setPoslovniPartneri(Set<PoslovniPartner> poslovniPartneri) {
		this.poslovniPartneri = poslovniPartneri;
	}

	public Set<Cenovnik> getCenovnici() {
		return cenovnici;
	}

	public void setCenovnici(Set<Cenovnik> cenovnici) {
		this.cenovnici = cenovnici;
	}

	public Set<Grupa> getGrupe() {
		return grupe;
	}

	public void setGrupe(Set<Grupa> grupe) {
		this.grupe = grupe;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
	
	
	
}
