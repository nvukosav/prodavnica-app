package com.nemanja.prodavnica.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Roba {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String naziv;
	
	private String jedinicaMere;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="grupa_id")
	private Grupa grupa;
	
	@OneToMany(mappedBy = "roba", cascade= CascadeType.ALL)
	private Set<StavkaFakture> stavkeFakture = new HashSet<>();
	
	@OneToMany(mappedBy = "roba", cascade= CascadeType.ALL)
	private Set<StavkaCenovnika> stavkeCenovnika = new HashSet<>();
	
	
	
	private boolean obrisano;
	
	public Roba() {}

	public Roba(String naziv, String jedinicaMere, Set<StavkaFakture> stavkeFakture,
			Set<StavkaCenovnika> stavkeCenovnika, Grupa grupa) {
		super();
		this.naziv = naziv;
		this.jedinicaMere = jedinicaMere;
		this.stavkeFakture = stavkeFakture;
		this.stavkeCenovnika = stavkeCenovnika;
		this.grupa = grupa;
		this.obrisano = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public Set<StavkaFakture> getStavkeFakture() {
		return stavkeFakture;
	}

	public void setStavkeFakture(Set<StavkaFakture> stavkeFakture) {
		this.stavkeFakture = stavkeFakture;
	}

	public Set<StavkaCenovnika> getStavkeCenovnika() {
		return stavkeCenovnika;
	}

	public void setStavkeCenovnika(Set<StavkaCenovnika> stavkeCenovnika) {
		this.stavkeCenovnika = stavkeCenovnika;
	}

	public Grupa getGrupa() {
		return grupa;
	}

	public void setGrupa(Grupa grupa) {
		this.grupa = grupa;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}
	
	
	
	
}
