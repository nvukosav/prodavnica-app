package com.nemanja.prodavnica.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class StavkaFakture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private long kolicina;
	
	private float cena;
	
	private double rabat;
	
	private double porezOsnovica;
	
	private double stopaPoreza;
	
	private double iznosPoreza;
	
	private double iznos;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "faktura_id")
	private Faktura faktura;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "roba_id")
	private Roba roba;
	
	private boolean obrisano;

	public StavkaFakture() {}

	public StavkaFakture(long kolicina, float cena, double rabat, double porezOsnovica, double stopaPoreza,
			double iznosPoreza, double iznos, Faktura faktura) {
		super();
		this.kolicina = kolicina;
		this.cena = cena;
		this.rabat = rabat;
		this.porezOsnovica = porezOsnovica;
		this.stopaPoreza = stopaPoreza;
		this.iznosPoreza = iznosPoreza;
		this.iznos = iznos;
		this.faktura = faktura;
		this.obrisano = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getKolicina() {
		return kolicina;
	}

	public void setKolicina(long kolicina) {
		this.kolicina = kolicina;
	}

	public float getCena() {
		return cena;
	}

	public void setCena(float cena) {
		this.cena = cena;
	}

	public double getRabat() {
		return rabat;
	}

	public void setRabat(double rabat) {
		this.rabat = rabat;
	}

	public double getPorezOsnovica() {
		return porezOsnovica;
	}

	public void setPorezOsnovica(double porezOsnovica) {
		this.porezOsnovica = porezOsnovica;
	}

	public double getStopaPoreza() {
		return stopaPoreza;
	}

	public void setStopaPoreza(double stopaPoreza) {
		this.stopaPoreza = stopaPoreza;
	}

	public double getIznosPoreza() {
		return iznosPoreza;
	}

	public void setIznosPoreza(double iznosPoreza) {
		this.iznosPoreza = iznosPoreza;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public Faktura getFaktura() {
		return faktura;
	}

	public void setFaktura(Faktura faktura) {
		this.faktura = faktura;
	}

	public Roba getRoba() {
		return roba;
	}

	public void setRoba(Roba roba) {
		this.roba = roba;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}

	
	
}
