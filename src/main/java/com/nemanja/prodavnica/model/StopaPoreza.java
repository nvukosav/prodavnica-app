package com.nemanja.prodavnica.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class StopaPoreza {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	public float procenat;
	
	private Date datumVazenja;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "porez_id")
	private Porez porez;
	
	@OneToMany(mappedBy = "stopaporeza", cascade = CascadeType.ALL)
    private Set<Grupa> grupe = new HashSet<>();
	
	private boolean obrisano;
	
	public StopaPoreza() {
		
	}

	public StopaPoreza(float procenat, Date datumVazenja, Porez porez) {
		super();
		this.procenat = procenat;
		this.datumVazenja = datumVazenja;
		this.porez = porez;
		this.obrisano = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getProcenat() {
		return procenat;
	}

	public void setProcenat(float procenat) {
		this.procenat = procenat;
	}

	public Date getDatumVazenja() {
		return datumVazenja;
	}

	public void setDatumVazenja(Date datumVazenja) {
		this.datumVazenja = datumVazenja;
	}

	public Porez getPorez() {
		return porez;
	}

	public void setPorez(Porez porez) {
		this.porez = porez;
	}

	public boolean isObrisano() {
		return obrisano;
	}

	public void setObrisano(boolean obrisano) {
		this.obrisano = obrisano;
	}

	public Set<Grupa> getGrupe() {
		return grupe;
	}

	public void setGrupe(Set<Grupa> grupe) {
		this.grupe = grupe;
	}
	
	
	
	
	
}
