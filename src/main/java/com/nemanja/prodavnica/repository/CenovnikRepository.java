package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Cenovnik;

@Repository
public interface CenovnikRepository extends JpaRepository<Cenovnik, Long>{

	List<Cenovnik> findAllByObrisano(boolean obrisano);

	Cenovnik findByObrisanoAndId(boolean obrisano, long id);
	
	List<Cenovnik> findByIdNot(long id);
	
	List<Cenovnik> findAllByPreduzece_Id(long id);
	
	List<Cenovnik> findAllByPoslovniPartner_id(long id);
	
}
