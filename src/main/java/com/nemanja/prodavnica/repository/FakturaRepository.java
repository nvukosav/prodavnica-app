package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Faktura;

@Repository
public interface FakturaRepository extends JpaRepository<Faktura, Long> {

	List<Faktura> findAllByVrstaFaktureAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(
			int vrstaFakture,String nazivPartnera);

	List<Faktura>
	findAllByVrstaFaktureAndPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(
			int vrstaFakture, long poslovnaGodina_id, String nazivPartnera);
	

    List<Faktura> findAllByObrisano(boolean obrisano);
    
    Faktura findByObrisanoAndId(boolean obrisano, long id);
	
	List<Faktura> findByObrisanoAndAndVrstaFaktureAndPreduzece_Id(boolean obrisano, boolean vrstaFakture,long id);

	List<Faktura> findByObrisanoAndAndVrstaFaktureAndPreduzece_IdAndPlaceno(
			boolean obrisano, boolean vrstaFakture, long id, boolean placeno);
	
}
