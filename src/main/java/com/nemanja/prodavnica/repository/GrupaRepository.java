package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nemanja.prodavnica.model.Grupa;

public interface GrupaRepository extends JpaRepository<Grupa, Long> {

	Grupa findByObrisanoAndId(boolean obrisano, long id);
	List<Grupa> findAllByPreduzece_idAndObrisano(Long id, boolean obrisano);
	List<Grupa> findAllByObrisanoAndNazivIgnoreCaseContains(boolean obrisano, String naziv);
	
}
