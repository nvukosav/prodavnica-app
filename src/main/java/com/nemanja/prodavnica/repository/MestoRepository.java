package com.nemanja.prodavnica.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.nemanja.prodavnica.model.Mesto;

public interface MestoRepository extends JpaRepository<Mesto, Long> {

	Mesto findByObrisanoAndId(boolean obrisano, long id);
	
	List<Mesto> findAllByNazivIgnoreCaseContainsAndObrisano(String naziv, boolean obrisano);
	
}
