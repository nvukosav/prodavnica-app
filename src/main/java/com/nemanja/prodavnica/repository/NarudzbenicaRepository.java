package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Narudzbenica;

@Repository
public interface NarudzbenicaRepository extends JpaRepository<Narudzbenica, Long>{

    List<Narudzbenica> findAllByPoslovniPartner_NazivIgnoreCaseContains(String nazivPartnera);

    List<Narudzbenica> findAllByPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContains(long poslovnaGodina_id, String nazivPartnera);

    List<Narudzbenica> findAllByObrisano(boolean obrisano);

    Narudzbenica findByObrisanoAndId(boolean obrisano, long id);

    List<Narudzbenica> findByObrisanoAndPreduzece_Id(boolean obrisano, long id);
    
    List<Narudzbenica> findAllByTipNarudzbeniceAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(int tipNar, String nazivPartnera);

    List<Narudzbenica>
	findAllByTipNarudzbeniceAndPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(
			int tipNarudzbenice, long poslovnaGodina_id, String nazivPartnera);
	
}
