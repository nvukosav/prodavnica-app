package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Otpremnica;

@Repository
public interface OtpremnicaRepository extends JpaRepository<Otpremnica, Long> {
	
	
    List<Otpremnica> findAllByPoslovniPartner_NazivIgnoreCaseContains(String nazivPartnera);

    List<Otpremnica> findAllByPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContains(long poslovnaGodina_id, String nazivPartnera);

	List<Otpremnica> findAllByObrisano(boolean obrisano);
	
    Otpremnica findByObrisanoAndId(boolean obrisano, long id);

    List<Otpremnica> findByObrisanoAndPreduzece_Id(boolean obrisano, long id);
}
