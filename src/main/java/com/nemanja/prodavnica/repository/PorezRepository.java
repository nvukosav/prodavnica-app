package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Porez;

@Repository
public interface PorezRepository extends JpaRepository<Porez, Long>{

	Porez findByObrisanoAndId(boolean obrisano, long id);
	
	List<Porez> findAllByObrisano(boolean obrisano);
	
	
}
