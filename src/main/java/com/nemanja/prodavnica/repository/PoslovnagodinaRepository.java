package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.PoslovnaGodina;

@Repository
public interface PoslovnagodinaRepository extends JpaRepository<PoslovnaGodina, Long> {
	
	PoslovnaGodina findByObrisanoAndId(boolean obrisano, long id);
	
	List<PoslovnaGodina> findAllByObrisano(boolean obrisano);

	List<PoslovnaGodina> findByZakljucenaIsFalseAndObrisanoIsFalse();
}
