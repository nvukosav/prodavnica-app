package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nemanja.prodavnica.model.PoslovniPartner;

public interface PoslovniPartnerRepository extends JpaRepository<PoslovniPartner, Long>{
	
    List<PoslovniPartner> findAllByObrisano(boolean obrisano);

    PoslovniPartner findByObrisanoAndId(boolean obrisano, long id);

    List<PoslovniPartner> findByPreduzece_id(Long id);

    List<PoslovniPartner>
    findAllByNazivIgnoreCaseContainsOrAdresaIgnoreCaseContainsOrMesto_NazivIgnoreCaseContainsAndObrisano(
            String nazivPartnera, String adresa, String naziv, boolean obrisano);
	
}
