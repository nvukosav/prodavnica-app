package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.Roba;

@Repository
public interface RobaRepository extends JpaRepository<Roba, Long>{
	
    List<Roba> findAllByObrisanoAndNazivIgnoreCaseContains(boolean obrisano, String nazivRobe);
	List<Roba> findAllByGrupa_idAndObrisanoAndNazivIgnoreCaseContains(Long id,
	boolean obrisano, String nazivRobe);
	Roba findByObrisanoAndId(boolean obrisano, long id);
	
	
}
