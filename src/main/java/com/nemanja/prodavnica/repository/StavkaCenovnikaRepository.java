package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.StavkaCenovnika;

@Repository
public interface StavkaCenovnikaRepository extends JpaRepository<StavkaCenovnika, Long>{

	List<StavkaCenovnika> findAllByObrisano(boolean obrisano);

    StavkaCenovnika findByObrisanoAndId(boolean obrisano, long id);
    List<StavkaCenovnika> findAllByObrisanoAndRoba_Id(boolean obrisano, Long id);

    List<StavkaCenovnika> findAllByObrisanoAndCenovnik_IdAndRoba_NazivIgnoreCaseContains(
            boolean obrisano, long cenovnik_id, String nazivRobe);
    
    List<StavkaCenovnika> findAllByCenovnik_Id(long id);
	
}
