package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.StavkaNarudzbenice;

@Repository
public interface StavkaNarudzbeniceRepository extends JpaRepository<StavkaNarudzbenice, Long>{
	
	  List<StavkaNarudzbenice> findByNarudzbenica_id(Long id);

	  List<StavkaNarudzbenice> findAllByObrisano(boolean obrisano);

	  StavkaNarudzbenice findByObrisanoAndId(boolean obrisano, long id);

}
