package com.nemanja.prodavnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nemanja.prodavnica.model.StopaPoreza;


@Repository
public interface StopaPorezaRepository extends JpaRepository<StopaPoreza, Long>{
	
    List<StopaPoreza> findAllByObrisano(boolean obrisano);

    StopaPoreza findByObrisanoAndId(boolean obrisano, long id);
}
