package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.repository.CenovnikRepository;
import com.nemanja.prodavnica.repository.StavkaCenovnikaRepository;
import com.nemanja.prodavnica.service.interfaces.ICenovnikService;

@Service
public class CenovnikService implements ICenovnikService{
	
	@Autowired
	private CenovnikRepository cenovnikRepo;
	
	@Autowired
	private StavkaCenovnikaRepository stavkaCenovnikaRepo;
	

	@Override
	public List<Cenovnik> findAll() {
		return cenovnikRepo.findAllByObrisano(false);
	}

	@Override
	public List<StavkaCenovnika> findAllByCenovnikId(long id, String nazivRobe) {
		return stavkaCenovnikaRepo.findAllByObrisanoAndCenovnik_IdAndRoba_NazivIgnoreCaseContains(false, id, nazivRobe);
	}

	@Override
	public List<Cenovnik> findAllByNotId(long id) {
		return cenovnikRepo.findByIdNot(id);
	}

	@Override
	public Cenovnik findOne(Long id) {
		return cenovnikRepo.findByObrisanoAndId(false, id);
	}

	@Override
	public Cenovnik save(Cenovnik cenovnik) {
		cenovnikRepo.save(cenovnik);
		return cenovnik;
	}

	@Override
	public Boolean delete(long id) {
		Optional<Cenovnik> result  = cenovnikRepo.findById(id);
		Cenovnik cenovnik =  null;
		if(result.isPresent()) {
			cenovnik = result.get();
		}
		else {
			throw new RuntimeException("Cenovnik nije pronadjen");
		}
		
		cenovnik.setObrisano(true);
		cenovnikRepo.saveAndFlush(cenovnik);

		return true;
	}

	@Override
	public void kopirajCenovnik(Cenovnik target, Cenovnik source) {

		Set<StavkaCenovnika> stavkeCenovnika = source.getStavkeCenovnika();
		
		for(StavkaCenovnika sc : stavkeCenovnika) {
			StavkaCenovnika novaStavka = new StavkaCenovnika();
			novaStavka.setCena(sc.getCena());
			novaStavka.setObrisano(sc.isObrisano());
			novaStavka.setRoba(sc.getRoba());
			novaStavka.setCenovnik(target);
			
			target.getStavkeCenovnika().add(novaStavka);
			stavkaCenovnikaRepo.save(novaStavka);
		}
		
		cenovnikRepo.save(target);
	}

	@Override
	public List<Cenovnik> findAllC() {
		return cenovnikRepo.findAll();
	}

	@Override
	public List<Cenovnik> findAllByPreduzeceId(long id) {
		return cenovnikRepo.findAllByPreduzece_Id(id);
	}

	@Override
	public List<Cenovnik> findAllByPoslParnterId(long id) {
		return cenovnikRepo.findAllByPoslovniPartner_id(id);
	}
	
	

}
