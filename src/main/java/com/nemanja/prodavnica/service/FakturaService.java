package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.StavkaFakture;
import com.nemanja.prodavnica.repository.FakturaRepository;
import com.nemanja.prodavnica.service.interfaces.IFakturaService;

@Service
public class FakturaService implements IFakturaService{
	
	@Autowired
	FakturaRepository fakturaRepo;

	@Override
	public List<Faktura> findAll() {
		return fakturaRepo.findAll();
	}

	@Override
	public Faktura findOne(Long id) {
		Optional<Faktura> result =  fakturaRepo.findById(id);
		if(result.isPresent()) {
			Faktura faktura = result.get();
			return faktura;
		}
		else {
			throw new RuntimeException("Faktura nije pronadjena");
		}
	}

	@Override
	public Faktura save(Faktura faktura) {
		fakturaRepo.save(faktura);
		return faktura;
	}

	@Override
	public Boolean delete(Long id) {
		Optional<Faktura> result = fakturaRepo.findById(id);
		if(result.isPresent()) {
			Faktura faktura = result.get();
			faktura.setObrisano(true);
			fakturaRepo.saveAndFlush(faktura);
			return true;
		}
		else {
			throw new RuntimeException("Faktura nije pronadjena");
		}
		
	}

	@Override
	public void update(Faktura faktura) {
		double osnovica = 0;
		double ukupanPorez = 0;
		double iznosZaPlacanje = 0;
		double iznosBezRabata = 0;
		double  rabat = 0;
		for(StavkaFakture sf : faktura.getStavkeFakture()) {
			rabat += sf.getRabat();
			iznosBezRabata += sf.getKolicina() * sf.getCena();
			osnovica += sf.getPorezOsnovica();
			ukupanPorez += sf.getIznosPoreza();
			iznosZaPlacanje += sf.getIznos();
		}
		
		faktura.setIznosZaPlacanje(iznosZaPlacanje);
		faktura.setOsnovica(osnovica);
		faktura.setUkupanPorez(ukupanPorez);
		faktura.setIznosBezRabata(iznosBezRabata);
		faktura.setRabat(rabat);
		save(faktura);
		
	}

	@Override
	public List<Faktura> findAllByVrstaFaktureAndNazivPartnera(int vrstaFakture, String nazivPartnera) {
		return fakturaRepo.findAllByVrstaFaktureAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(vrstaFakture, nazivPartnera);
	}

	@Override
	public List<Faktura> findAllByVrstaFaktureAndPoslovnaGodinaAndNazivPartnera(
			int vrstaFakture, String nazivPartnera, long poslovnaGodinaId) {
		return fakturaRepo.findAllByVrstaFaktureAndPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(vrstaFakture, 
				poslovnaGodinaId, nazivPartnera);
	}

}
