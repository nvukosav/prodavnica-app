package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Grupa;
import com.nemanja.prodavnica.repository.GrupaRepository;
import com.nemanja.prodavnica.service.interfaces.IGrupaService;

@Service
public class GrupaService implements IGrupaService{
	
	@Autowired
	GrupaRepository grupaRepo;

	@Override
	public List<Grupa> findAll(String naziv) {

		return grupaRepo.findAllByObrisanoAndNazivIgnoreCaseContains(false, naziv);
	}

	@Override
	public Grupa findOne(Long id) {
		Optional<Grupa> result = grupaRepo.findById(id);
		if(result.isPresent()) {
			Grupa gr = result.get();
			return gr;
		}
		else {
			throw new RuntimeException("Grupa sa tim idom ne postoji");
		}
	}

	@Override
	public List<Grupa> findByPreduzece_id(Long id) {
		return grupaRepo.findAllByPreduzece_idAndObrisano(id, false);
	}

	@Override
	public Grupa save(Grupa grupa) {
		grupaRepo.save(grupa);
		return grupa;
	}

	@Override
	public Boolean delete(Long id) {
		Optional<Grupa> result = grupaRepo.findById(id);
		if(result.isPresent()) {
			Grupa gr = result.get();
			gr.setObrisano(true);
			grupaRepo.saveAndFlush(gr);
			return true;
		}
		else {
			throw new RuntimeException("Grupa sa tim idom ne postoji");
		}
	}

	
	
}
