package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Mesto;
import com.nemanja.prodavnica.repository.MestoRepository;
import com.nemanja.prodavnica.service.interfaces.IMestoService;

@Service
public class MestoService implements IMestoService{
	
	@Autowired
	MestoRepository mestoRepo;

	@Override
	public List<Mesto> findAll(String naziv) {
		return mestoRepo.findAllByNazivIgnoreCaseContainsAndObrisano(naziv, false);
	}

	@Override
	public Mesto findOne(Long id) {
		Optional<Mesto> result = mestoRepo.findById(id);
		if(result.isPresent()) {
			Mesto mesto = result.get();
			return mesto;
		}
		else {
			throw new RuntimeException("Nije pronadjeno mesto");
		}
	}

	@Override
	public Mesto save(Mesto mesto) {
		mestoRepo.save(mesto);
		return mesto;
	}

	@Override
	public Boolean delete(Long id) {
		Optional<Mesto> result = mestoRepo.findById(id);
		
		if(result.isPresent()) {
			Mesto mesto = result.get();
			mesto.setObrisano(true);
			mestoRepo.saveAndFlush(mesto);
			return true;
		}
		else {
			throw new RuntimeException("Mesto sa tim IDom ne postoji!");
		}
	}
	
	

	

}
