package com.nemanja.prodavnica.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.model.StavkaFakture;
import com.nemanja.prodavnica.model.StavkaNarudzbenice;
import com.nemanja.prodavnica.repository.NarudzbenicaRepository;
import com.nemanja.prodavnica.service.interfaces.ICenovnikService;
import com.nemanja.prodavnica.service.interfaces.IFakturaService;
import com.nemanja.prodavnica.service.interfaces.INarudzbenicaService;

@Service
public class NarudzbenicaService implements INarudzbenicaService{

	@Autowired
	private IFakturaService fakturaService;
	
	@Autowired
	private NarudzbenicaRepository narudzbenicaRepo;
	
	@Autowired
	private ICenovnikService cenovnikService;


	@Override
	public Narudzbenica findOne(Long id) {
		Optional<Narudzbenica> result = narudzbenicaRepo.findById(id);
		
		if(result.isPresent()) {
			Narudzbenica nar = result.get();
			return nar;
		}
		else {
			return null;
		}
	}

	@Override
	public Narudzbenica save(Narudzbenica narudzbenica) {
		narudzbenicaRepo.save(narudzbenica);
		return narudzbenica;
	}

	@Override
	public List<Narudzbenica> findAllByNazivPartnera(String nazivPartnera) {
		return narudzbenicaRepo.findAllByPoslovniPartner_NazivIgnoreCaseContains(nazivPartnera);
	}

	@Override
	public List<Narudzbenica> findAllByPoslovnaGodinaAndNazivPartnera(String nazivPartnera, long poslovnaGodinaId) {
		return narudzbenicaRepo.findAllByPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContains(poslovnaGodinaId, nazivPartnera);
		}

	@Override
	public void napraviFakturuOdNarudzbenice(Narudzbenica narudzbenica, int poslovnaGodina) {
    	narudzbenica.setObrisano(true);
    	save(narudzbenica);

        Date datumValute = DateUtils.addDays(narudzbenica.getDatumNarudzbenice(), 60);

        Faktura faktura = new Faktura();
        faktura.setBrojFakture(poslovnaGodina + 1);
        faktura.setDatumFakture(new Date());
        faktura.setDatumValute(datumValute);
        faktura.setPlaceno(true);
        if(narudzbenica.getPoslovniPartner().getVrstaPartnera() == 1) {
           faktura.setVrstaFakture(1);
        }
        else {
            faktura.setVrstaFakture(0);
        }
        faktura.setPreduzece(narudzbenica.getPreduzece());
        faktura.setPoslovnaGodina(narudzbenica.getPoslovnaGodina());
        faktura.setPoslovniPartner(narudzbenica.getPoslovniPartner());
        faktura.setNarudzbenica(narudzbenica);
      
        
        Set<StavkaNarudzbenice> stavkeNarudzbenice = narudzbenica.getStavkeNarudzbenice();
        
        
        List<Cenovnik> cenovnici = new ArrayList<Cenovnik>();
        
        System.out.println("Narudzbenica je: " + narudzbenica.getTipNarudzbenice());
        
        if(narudzbenica.getTipNarudzbenice() == 1) {
        	List<Cenovnik >cenovnici2 = cenovnikService.findAllByPoslParnterId(narudzbenica.getPoslovniPartner().getId());
        	
        	for (Cenovnik cenovnik : cenovnici2) {
				cenovnici.add(cenovnik);
			}
        }
        else {
        	List<Cenovnik> cenovnici2 = cenovnikService.findAllByPreduzeceId(narudzbenica.getPreduzece().getId());	
        	for (Cenovnik cenovnik : cenovnici2) {
				cenovnici.add(cenovnik);
			}
        }
        
        List<StavkaCenovnika> stavkeCenovnika = new ArrayList<StavkaCenovnika>();
        
        for (Cenovnik c : cenovnici) {
        	for(StavkaCenovnika s : c.getStavkeCenovnika()) {
        		stavkeCenovnika.add(s);
        	}
		}


        Set<StavkaFakture> nadjeneStavke = new HashSet<>();

        for (StavkaNarudzbenice sn : stavkeNarudzbenice) {
        	        	
        	for(StavkaCenovnika sc : stavkeCenovnika) {
        		
        		if(sc.getRoba().getId() == sn.getRoba().getId()) {

                    StavkaFakture novaStavkaFakture = new StavkaFakture();
                    novaStavkaFakture.setIznosPoreza(sc.getCena() * sn.getKolicina() * (sn.getRoba().getGrupa().getStopaporeza().getProcenat() / 100));
                    novaStavkaFakture.setIznos(sc.getCena() * sn.getKolicina() * (1+(sn.getRoba().getGrupa().getStopaporeza().getProcenat() / 100)));
                    novaStavkaFakture.setCena(sc.getCena());
                    novaStavkaFakture.setKolicina(sn.getKolicina());
                    novaStavkaFakture.setPorezOsnovica(sc.getCena() * sn.getKolicina());
                    novaStavkaFakture.setObrisano(false);
                    novaStavkaFakture.setRabat(0);
                    novaStavkaFakture.setRoba(sn.getRoba());
                    novaStavkaFakture.setStopaPoreza(sn.getRoba().getGrupa().getStopaporeza().getProcenat());
                    novaStavkaFakture.setFaktura(faktura);


                    nadjeneStavke.add(novaStavkaFakture);
        		}
        	}
        }

        faktura.setStavkeFakture(nadjeneStavke);
        fakturaService.update(faktura);
		
	}

	@Override
	public List<Narudzbenica> findAllByTipAndNazivPartnera(int tip, String naziv) {
		return narudzbenicaRepo.findAllByTipNarudzbeniceAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(tip, naziv);

	}

	@Override
	public List<Narudzbenica> findAllByTipAndNazivPartneraAndGodina(int tip, String naziv, int godina) {
		return narudzbenicaRepo.findAllByTipNarudzbeniceAndPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContainsAndObrisanoIsFalse(tip, godina, naziv);
	}


	
}
