package com.nemanja.prodavnica.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.Faktura;
import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.Otpremnica;
import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.model.StavkaFakture;
import com.nemanja.prodavnica.model.StavkaNarudzbenice;
import com.nemanja.prodavnica.model.StavkaOtpremnice;
import com.nemanja.prodavnica.repository.OtpremnicaRepository;
import com.nemanja.prodavnica.service.interfaces.ICenovnikService;
import com.nemanja.prodavnica.service.interfaces.IFakturaService;
import com.nemanja.prodavnica.service.interfaces.IOtpremnicaService;

@Service
public class OtpremnicaService implements IOtpremnicaService {

	@Autowired
	private OtpremnicaRepository otpremnicaRepo;
	
	@Autowired
	private NarudzbenicaService narudzbenicaService;
	
	@Autowired
	private IFakturaService fakturaService;
	
	@Autowired
	private ICenovnikService cenovnikService;
	
	@Override
	public Otpremnica findOne(Long id) {
		Optional<Otpremnica> result = otpremnicaRepo.findById(id);
		if(result.isPresent()) {
			Otpremnica o = result.get();
			return o;
		}
		else {
			throw new RuntimeException("Nije pronadjena otpremnica");
		}
	}

	@Override
	public Otpremnica save(Otpremnica otpremnica) {
		otpremnicaRepo.save(otpremnica);
		return otpremnica;
	}

	@Override
	public void update(Otpremnica otpremnica) {
		double iznosZaPlacanje = 0;
		
		for(StavkaOtpremnice so : otpremnica.getStavkeOtpremnice()) {
			if(!so.isObrisano()) {
				iznosZaPlacanje += so.getIznos();
			}
		}
		otpremnica.setIznosZaPlacanje(iznosZaPlacanje);
		save(otpremnica);
	}

	@Override
	public List<Otpremnica> findAllByNazivPartnera(String nazivPartnera) {
		return otpremnicaRepo.findAllByPoslovniPartner_NazivIgnoreCaseContains(nazivPartnera);
	}

	@Override
	public List<Otpremnica> findAllByPoslovnaGodinaAndNazivPartnera(String nazivPartnera, long poslovnaGodinaId) {
		return otpremnicaRepo.findAllByPoslovnaGodina_IdAndPoslovniPartner_NazivIgnoreCaseContains(poslovnaGodinaId, nazivPartnera);
	}

	@Override
	public void napraviFakturuOdOtpremnice(Otpremnica otpremnica, int poslednjaPoslovnjaGodina) {
	 	otpremnica.setObrisano(true);
    	save(otpremnica);

        Date datumValute = DateUtils.addDays(otpremnica.getDatumOtpremnice(), 60);

        Faktura faktura = new Faktura();
        faktura.setBrojFakture(poslednjaPoslovnjaGodina + 1);
        faktura.setDatumFakture(new Date());
        faktura.setDatumValute(datumValute);
        faktura.setPlaceno(true);
        faktura.setVrstaFakture(1);
        faktura.setPreduzece(otpremnica.getPreduzece());
        faktura.setPoslovnaGodina(otpremnica.getPoslovnaGodina());
        faktura.setPoslovniPartner(otpremnica.getPoslovniPartner());
        faktura.setOtpremnica(otpremnica);
        
        // naci id robe usluga iz narudzbenice
        Set<StavkaOtpremnice> stavkeOtpremnice = otpremnica.getStavkeOtpremnice();
      
        Set<StavkaFakture> nadjeneStavke = new HashSet<>(); 
        
        for(StavkaOtpremnice so : stavkeOtpremnice) {
    			StavkaFakture novaStavkaFakture = new StavkaFakture();
                novaStavkaFakture.setIznosPoreza(so.getIznos()* (so.getRoba().getGrupa().getStopaporeza().getProcenat() / 100));
                novaStavkaFakture.setIznos(so.getIznos() * (1+(so.getRoba().getGrupa().getStopaporeza().getProcenat() / 100)));
                novaStavkaFakture.setCena(so.getCena());
                novaStavkaFakture.setKolicina(so.getKolicina());
                novaStavkaFakture.setPorezOsnovica(so.getIznos());
                novaStavkaFakture.setRabat(0);
                novaStavkaFakture.setObrisano(false);
                novaStavkaFakture.setRoba(so.getRoba());
                novaStavkaFakture.setStopaPoreza(so.getRoba().getGrupa().getStopaporeza().getProcenat());
                novaStavkaFakture.setFaktura(faktura);

                nadjeneStavke.add(novaStavkaFakture);
        }

        faktura.setStavkeFakture(nadjeneStavke);
        fakturaService.update(faktura);
	}

	@Override
	public void napraviOtpremnicuOdNarudzbenice(Narudzbenica narudzbenica) {

        List<Otpremnica> listaOtpremnica = otpremnicaRepo.findAll();
        
        //narudzbenica.setTipNarudzbenice(false);
        narudzbenica.setObrisano(true);
        narudzbenicaService.save(narudzbenica);

        Otpremnica otpremnica = new Otpremnica();
        otpremnica.setBrojOtpremnice(listaOtpremnica.size() + 1);
        otpremnica.setDatumOtpremnice(new Date());
        otpremnica.setPreduzece(narudzbenica.getPreduzece());
        otpremnica.setPoslovnaGodina(narudzbenica.getPoslovnaGodina());
        otpremnica.setPoslovniPartner(narudzbenica.getPoslovniPartner());
        otpremnica.setTipOtpremnice(false);
        otpremnica.setObrisano(false);
        otpremnica.setNarudzbenica(narudzbenica);
   
        
        List<Cenovnik> cenovnik = cenovnikService.findAllByPreduzeceId(1);
        
        Set<StavkaNarudzbenice> stavkeNarudzbenice = narudzbenica.getStavkeNarudzbenice();
        Set<StavkaOtpremnice> stavkeOtpremnice = new HashSet<>();
        List<StavkaCenovnika> stavkeCenovnika = new ArrayList<StavkaCenovnika>();
        
        for (Cenovnik c : cenovnik) {
        	for(StavkaCenovnika s : c.getStavkeCenovnika()) {
        		stavkeCenovnika.add(s);
        	}
		}




        for (StavkaNarudzbenice stavkaNarudzbenice : stavkeNarudzbenice) {
        	
        	
        	for(StavkaCenovnika sc : stavkeCenovnika) {
        		
        		if(sc.getRoba().getId() == stavkaNarudzbenice.getRoba().getId()) {
        	
                    StavkaOtpremnice stavkaOtpremnice = new StavkaOtpremnice();
                    stavkaOtpremnice.setJedinicaMere(stavkaNarudzbenice.getJedinicaMere());
                    stavkaOtpremnice.setKolicina(stavkaNarudzbenice.getKolicina());
                    stavkaOtpremnice.setRoba(stavkaNarudzbenice.getRoba());
                    stavkaOtpremnice.setCena(sc.getCena());
                    stavkaOtpremnice.setIznos(stavkaNarudzbenice.getKolicina() * sc.getCena());
                    stavkaOtpremnice.setOtpremnica(otpremnica);
                    stavkaOtpremnice.setObrisano(false);
                    stavkaOtpremnice.setOpis(stavkaNarudzbenice.getOpis());

                    stavkeOtpremnice.add(stavkaOtpremnice);
        			}
        		}
        }

        otpremnica.setStavkeOtpremnice(stavkeOtpremnice);
        update(otpremnica);
		
	}

	
	
}
