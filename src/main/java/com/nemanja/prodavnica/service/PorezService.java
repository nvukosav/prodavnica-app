package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Porez;
import com.nemanja.prodavnica.repository.PorezRepository;
import com.nemanja.prodavnica.service.interfaces.IPorezService;

@Service
public class PorezService implements IPorezService {
	
	@Autowired
	PorezRepository porezRepo;

	@Override
	public List<Porez> findAll() {
        return porezRepo.findAll();

	}

	@Override
	public Porez findOne(Long id) {
		Optional<Porez> result = porezRepo.findById(id);
		if(result.isPresent()) {
			return result.get();
		}
		else {
			throw new RuntimeException("Porez nije pronadjen");
		}
	}

	@Override
	public Porez save(Porez porez) {
		porezRepo.save(porez);
		return porez;
	}

	@Override
	public Boolean delete(Long id) {
		Optional<Porez> result = porezRepo.findById(id);
		if(result.isPresent()) {
			Porez porez = result.get();
			porez.setObrisano(true);
			porezRepo.saveAndFlush(porez);
			return true;
		}
		else {
			throw new RuntimeException("Porez nije pronadjen");
		}
	}

	
}
