package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.Roba;
import com.nemanja.prodavnica.repository.RobaRepository;
import com.nemanja.prodavnica.service.interfaces.IRobaService;

@Service
public class RobaService implements IRobaService {

	@Autowired
	RobaRepository robaRepo;
	
	@Override
	public List<Roba> findAll(String naziv) {
		return robaRepo.findAllByObrisanoAndNazivIgnoreCaseContains(false, naziv);
	}

	@Override
	public Roba findOne(Long id) {
		Optional<Roba> result = robaRepo.findById(id);
		if(result.isPresent()) {
			return result.get();
		}
		else {
			throw new RuntimeException("Nije Pronadjena RopbaUsluga");
		}
		
	}

	@Override
	public Roba save(Roba roba) {
		robaRepo.save(roba);
		return roba;
	}

	@Override
	public Boolean delete(Long id) {
	
		Optional<Roba> result = robaRepo.findById(id);
		if(result.isPresent()) {
			Roba ru = result.get();
			ru.setObrisano(true);
			robaRepo.saveAndFlush(ru);
			return true;
		}
		else {
			throw new RuntimeException("Nije Pronadjena RopbaUsluga");
		}
	}

	@Override
	public List<Roba> findAllByGrupa_id(Long id, String naziv) {
		return robaRepo.findAllByGrupa_idAndObrisanoAndNazivIgnoreCaseContains(id, false, naziv);
	}

}
