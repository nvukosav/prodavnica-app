package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.StavkaCenovnika;
import com.nemanja.prodavnica.repository.StavkaCenovnikaRepository;
import com.nemanja.prodavnica.service.interfaces.IStavkaCenovnikaService;

@Service
public class StavkaCenovnikaService implements IStavkaCenovnikaService {

	@Autowired
	private StavkaCenovnikaRepository stavkaCRepo;
	
	@Override
	public List<StavkaCenovnika> findAll() {
		return stavkaCRepo.findAll();
	}

	@Override
	public StavkaCenovnika findOne(Long id) {
		Optional<StavkaCenovnika> result = stavkaCRepo.findById(id);
		if(result.isPresent()) {
			return result.get();
		}
		else {
			throw new RuntimeException("Stavka Cenovnika nije pronadjena");
		}
	}

	@Override
	public StavkaCenovnika save(StavkaCenovnika stavkaCenovnika) {
		stavkaCRepo.save(stavkaCenovnika);
		return stavkaCenovnika;
	}

	@Override
	public List<StavkaCenovnika> findAllByRoba_id(Long id) {
		return stavkaCRepo.findAllByObrisanoAndRoba_Id(false, id);
	}

	@Override
	public Boolean delete(Long id) {
		Optional<StavkaCenovnika> result = stavkaCRepo.findById(id);
		if(result.isPresent()) {
			StavkaCenovnika sc = result.get();
			sc.setObrisano(true);
			stavkaCRepo.saveAndFlush(sc);
			return true;
		}
		else {
			throw new RuntimeException("Stavka Cenovnika nije pronadjena");
		}
	}

	@Override
	public List<StavkaCenovnika> findAllByCenovnikId(long id) {
		return stavkaCRepo.findAllByCenovnik_Id(id);
	}

	
	
}
