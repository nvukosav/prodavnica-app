package com.nemanja.prodavnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nemanja.prodavnica.model.StopaPoreza;
import com.nemanja.prodavnica.repository.StopaPorezaRepository;
import com.nemanja.prodavnica.service.interfaces.IStopaPorezaService;

@Service
public class StopaPorezaService implements IStopaPorezaService{

	@Autowired
	StopaPorezaRepository stopaPorezaRepo;
	
	
	@Override
	public List<StopaPoreza> findAll() {
        return stopaPorezaRepo.findAllByObrisano(false);

	}

	@Override
	public StopaPoreza findOne(Long id) {
		Optional<StopaPoreza> result = stopaPorezaRepo.findById(id);
		if(result.isPresent()) {
			return result.get();
		}
		else {
			throw new RuntimeException("StopaPorez nije pronadjena");
		}
	}

	@Override
	public StopaPoreza save(StopaPoreza stopaPoreza) {
		stopaPorezaRepo.save(stopaPoreza);
		return stopaPoreza;
	}

	@Override
	public Boolean delete(Long id) {
		Optional<StopaPoreza> result = stopaPorezaRepo.findById(id);
		if(result.isPresent()) {
			StopaPoreza stopa = result.get();
			stopa.setObrisano(true);
			stopaPorezaRepo.saveAndFlush(stopa);
			return true;
		}
		else {
			throw new RuntimeException("StopaPorez nije pronadjena");
		}
	}

	
	
}
