package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.StavkaCenovnika;


public interface ICenovnikService {

	List<Cenovnik> findAll();
	List<Cenovnik> findAllC();
	List<StavkaCenovnika> findAllByCenovnikId(long id, String nazivRobe);
	List<Cenovnik> findAllByNotId(long id);
	Cenovnik findOne(Long id);
	Cenovnik save(Cenovnik cenovnik);
	Boolean delete(long id);
	
	List<Cenovnik> findAllByPreduzeceId(long id);
	List<Cenovnik> findAllByPoslParnterId(long id);
	
	void kopirajCenovnik(Cenovnik target, Cenovnik source);
	
}
