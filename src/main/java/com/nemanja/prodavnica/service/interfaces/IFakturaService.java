package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Faktura;

public interface IFakturaService {
	
	List<Faktura> findAll();
    Faktura findOne(Long id);
    Faktura save(Faktura faktura);
    Boolean delete(Long id);
    void update(Faktura faktura);
	
    List<Faktura> findAllByVrstaFaktureAndNazivPartnera(int vrstaFakture, String nazivPartnera);
    List<Faktura> findAllByVrstaFaktureAndPoslovnaGodinaAndNazivPartnera(int vrstaFakture, String nazivPartnera, long poslovnaGodinaId);
	

}
