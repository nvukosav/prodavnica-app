package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Grupa;

public interface IGrupaService {
	
	List<Grupa> findAll(String naziv);
	Grupa findOne(Long id);
	List<Grupa> findByPreduzece_id(Long id);
	Grupa save(Grupa grupa);
	Boolean delete(Long id);
}
