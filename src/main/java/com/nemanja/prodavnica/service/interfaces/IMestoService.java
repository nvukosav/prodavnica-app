package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Mesto;

public interface IMestoService {

	List<Mesto> findAll(String naziv);
	Mesto findOne(Long id);
	Mesto save (Mesto mesto);
	Boolean delete(Long id);
}
