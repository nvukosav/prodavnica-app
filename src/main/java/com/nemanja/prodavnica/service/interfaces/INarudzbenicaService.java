package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Narudzbenica;

public interface INarudzbenicaService {
	
	Narudzbenica findOne(Long id);

	Narudzbenica save(Narudzbenica narudzbenica);
	
    List<Narudzbenica> findAllByNazivPartnera(String nazivPartnera);

    List<Narudzbenica> findAllByPoslovnaGodinaAndNazivPartnera(String nazivPartnera, long poslovnaGodinaId);

    void napraviFakturuOdNarudzbenice(Narudzbenica narudzbenica, int poslednjaPoslovnaGodina);
    
    List<Narudzbenica> findAllByTipAndNazivPartnera(int tip, String naziv);
    
    List<Narudzbenica> findAllByTipAndNazivPartneraAndGodina(int tip, String naziv, int godina);


	 
	 
	
}
