package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Narudzbenica;
import com.nemanja.prodavnica.model.Otpremnica;

public interface IOtpremnicaService {
	
    Otpremnica findOne(Long id);

    Otpremnica save(Otpremnica otpremnica);
    
    void update(Otpremnica otpremnica);
    
    List<Otpremnica> findAllByNazivPartnera(String nazivPartnera);

    List<Otpremnica> findAllByPoslovnaGodinaAndNazivPartnera(String nazivPartnera, long poslovnaGodinaId);

    void napraviFakturuOdOtpremnice(Otpremnica otpremnica,int poslednjaPoslovnjaGodina);

    void napraviOtpremnicuOdNarudzbenice(Narudzbenica narudzbenica);
}
