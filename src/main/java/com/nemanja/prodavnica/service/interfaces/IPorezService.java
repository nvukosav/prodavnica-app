package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Porez;

public interface IPorezService {

	List<Porez> findAll();
    Porez findOne(Long id);
    Porez save(Porez porez);
    Boolean delete(Long id);
    
}
