package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.PoslovnaGodina;

public interface IPoslovnaGodinaService {

    List<PoslovnaGodina> findAll();
	PoslovnaGodina findOne(Long id);
    PoslovnaGodina save(PoslovnaGodina poslovnaGodina);
    Boolean delete(Long id);
    
    PoslovnaGodina findPoslovnaGodinaIsNotObrisanoIsNotZakljucena();
	
}
