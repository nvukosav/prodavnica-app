package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Cenovnik;
import com.nemanja.prodavnica.model.PoslovniPartner;

public interface IPoslovniPartnerService {

	List<PoslovniPartner> findAll(String filter);
    List<PoslovniPartner> findAll();
    List<PoslovniPartner> findByPreduzece_id(Long id);
    PoslovniPartner findOne(Long id);
    PoslovniPartner save(PoslovniPartner poslovniPartner);
    Boolean delete(Long id);
    PoslovniPartner findPartner(Cenovnik cenovnik);
	
}
