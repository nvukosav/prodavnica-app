package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.Roba;

public interface IRobaService {

	List<Roba> findAll(String naziv);
    Roba findOne(Long id);
    Roba save(Roba roba);
    Boolean delete(Long id);
    List<Roba> findAllByGrupa_id(Long id, String naziv);
	
}
