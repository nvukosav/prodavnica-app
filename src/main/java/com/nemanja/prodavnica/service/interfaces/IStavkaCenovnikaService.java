package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.StavkaCenovnika;

public interface IStavkaCenovnikaService {

    List<StavkaCenovnika> findAll();
    StavkaCenovnika findOne(Long id);
    StavkaCenovnika save(StavkaCenovnika stavkaCenovnika);
    List<StavkaCenovnika> findAllByRoba_id(Long id);
    Boolean delete(Long id);
	List<StavkaCenovnika> findAllByCenovnikId(long id);
}
