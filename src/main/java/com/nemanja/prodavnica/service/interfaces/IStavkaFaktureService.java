package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.StavkaFakture;

public interface IStavkaFaktureService {

	List<StavkaFakture> findAll();
    List<StavkaFakture> findByFaktura_id(Long id);
    StavkaFakture findOne(Long id);
    StavkaFakture save(StavkaFakture stavkaFakture);
    Boolean delete(Long id);
	
}
