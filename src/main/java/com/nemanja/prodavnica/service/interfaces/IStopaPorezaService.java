package com.nemanja.prodavnica.service.interfaces;

import java.util.List;

import com.nemanja.prodavnica.model.StopaPoreza;

public interface IStopaPorezaService {
	
    List<StopaPoreza> findAll();
    StopaPoreza findOne(Long id);
    StopaPoreza save(StopaPoreza stopaPoreza);
    Boolean delete(Long id);
}
