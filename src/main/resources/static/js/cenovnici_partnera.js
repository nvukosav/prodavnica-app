$(document).ready(function() {
	$("#navigation").load("nav.html");
	$("#preduzece").text("Preduzeće");
	var tabela = $("#tblCenovnici");
	var deleteCenovnik = $("#delete_cenovnik");
	var updateCenovnik = $("#update_cenovnik");
	var copyCenovnik = $("#kopiraj_cenovnik_modal");
	var deleteContent = $("#delete_content");
	var buttons = {
		add: $("#add_cenovnik")
	};

	getPreduzeca();
	popunjavanjeTabele();

	function popunjavanjeTabele() {
		tabela.empty();
		$.ajax({
			url: `api/cenovnik`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {
					var partner;
					$.ajax({
						url: 'api/cenovnik/' + value.id + '/poslovni_partner',
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) {
							partner = data;
							red = $("<tr></tr>");
							red.append("<td class='text-center'>" + partner.nazivPartnera + "</br></td>");
							red.append("<td class='text-center'>" + new Date(value.datumVazenjaOd).toLocaleString() + "</td>");
							red.append("<td class='text-center'>" + new Date(value.datumVazenjaDo).toLocaleString() + "</td>");

							red.append("<td class='text-right'><a href='cenovnik.html?id=" + value.id + "' class='btn btn-outline-primary'>Pregledaj</a></td>");
							red.append("<td>" +
								"<button cenovnik_id='" + value.id + "' class='btn btn-outline-danger delete_cenovnik'>Obriši</button></td>");
							red.append("<td>" +
								"<button cenovnik_kopiraj='" + value.id + "' class='btn btn-outline-primary kopiraj_cenovnik'>Preuzmi stavke</button></td>");
							tabela.append(red);
						}, error: function(error) {
							// TODO
						}
					});

				});
			}
		});
	}

	function getPreduzeca() {
		$.ajax({
			url: 'api/poslovni_partneri/',
			type: 'GET',
			success: function(data) {
				preduzeca = data;
				var select = document.getElementById("selectPreduzece");
				for (var i = 0; i < preduzeca.length; i++) {
					select.options[select.options.length] = new Option(preduzeca[i].nazivPartnera, preduzeca[i].id);
				}
			}
		});
	}

	$("#add_new_cenovnik").on("click", function(event) {
		event.preventDefault();
		updateCenovnik.modal("show");
		buttons.add.show();
	});

	buttons.add.on("click", function(event) {
		var c = {};
		event.preventDefault();
		var preduz = $("#selectPreduzece");
		var datumOd = $("#datumOd");
		var datumDo = $("#datumDo")
		c.datumVazenjaOd = datumOd.val();
		c.datumVazenjaDo = datumDo.val();
		c.poslovniPartnerId = preduz.val();
		c.preduzeceId = null;

		$.ajax({
			url: 'api/cenovnik?preduzece=false&id=' + preduz.val(),
			type: 'POST',
			data: JSON.stringify(c),
			contentType: "application/json"
		}).done(function() {
			popunjavanjeTabele();
			alert("Cenovnik uspešno dodat!");
		});
		updateCenovnik.modal("hide");
		c = null;
	});

	tabela.on("click", "button.delete_cenovnik", function(event) {
		event.preventDefault();
		var cenovnikID = $(this).attr("cenovnik_id");
		deleteContent.text("Obrisati cenovnik?");
		deleteCenovnik.modal("show");
		$("#delete_confirm").on("click", function(event) {
			event.preventDefault();
			$.ajax({
				url: 'api/cenovnik/' + cenovnikID,
				type: 'DELETE',
				contentType: "application/json"
			}).done(function() {
				popunjavanjeTabele();
				alert("Cenovnik je obrisan!");
			});
			deleteCenovnik.modal("hide");
		});
	});

	tabela.on("click", "button.kopiraj_cenovnik", function(event) {
		event.preventDefault();
		var cenovnikCopy = $(this).attr("cenovnik_kopiraj");
		copyCenovnik.modal("show");
		$.ajax({
			// izopšti tekući cenovnik
			url: "api/cenovnik/" + cenovnikCopy + "/bezIzabranog",
			type: 'GET',
			success: function(data) {
				var select = document.getElementById("cenovnikDropdown");
				$("#cenovnikDropdown").empty();
				data.forEach(function(value) {
					$("#cenovnikDropdown").append('<option value="' + value.id + '">' + value.poslovniPartnerId + ": " + new Date(value.datumVazenjaOd).toLocaleString() + " - " + new Date(value.datumVazenjaDo).toLocaleString() + '</option>');
				})
				$("#kopi_cen").on("click", function(event) {
					var ciljaniId = $("#cenovnikDropdown").val();
					$.ajax({
						url: "api/cenovnik/" + cenovnikCopy + "/kopirajIzCenovnika/" + ciljaniId,
						type: 'PUT',
						success: function(data) {
							copyCenovnik.modal("hide");
							alert("Cenovnik uspešno iskopiran!");
						}
					});
				});
			}
		});
	});


});