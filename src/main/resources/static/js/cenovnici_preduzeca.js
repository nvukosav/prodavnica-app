$(document).ready(function() {
	$("#navigation").load("nav.html");


	var preduzece = {};
	var tabelaCenovnika = $("#tblCenovniciPreduzeca");
	var mesto;

	ucitajCenovnike();


	c = {};
	var deleteCenovnik = $("#delete_cenovnik");
	var deleteContent = $("#delete_content");

	var addCenovnik = $("#add_cenovnik");
	buttons = {
		add: $("#buttonAdd_cenovnik"),
		edit: $("#edit_cenovnik"),
	};

	function ucitajPreduzece() {
		$.ajax({
			url: 'api/preduzece/1',
			dataType: "json",
			contentType: "application/json",
			success: function(data) {

				$.ajax({
					url: 'api/mesto/' + data.mesto,
					type: 'GET',
					async: false,
					success: function(data) { mesto = data }
				});

				preduzece = data;


				$("#nazivPreduzeca").text(data.naziv);
				$("#adresaPreduzeca").text(data.adresaPreduzeca + " - " + mesto.naziv);
				$("#telefonPreduzeca").text(data.telefon);
				$("#brojRacunaPreduzeca").text(data.brojRacuna.slice(0, 3) + "-" + data.brojRacuna.slice(3, 15) + "-" + data.brojRacuna.slice(15));
				$("#emailPreduzeca").text(data.email);
				$("#pib").text(data.pib);
				$("#logoPreduzeca").attr("src", data.logo);
			}
		});
	}


	function ucitajCenovnike() {
		tabelaCenovnika.empty();
		$.ajax({
			url: "api/preduzece/1/cenovnici",
			type: 'GET',
			success: function(data) {
				data.forEach(function(value) {
					red = $("<tr></tr>");
					red.append("<td>" + new Date(value.datumVazenjaOd).toLocaleString() + "</td>");
					red.append("<td>" + new Date(value.datumVazenjaDo).toLocaleString() + "</td>");
					red.append("<td class='text-right'><a href='cenovnik.html?id=" + value.id + "' class='btn  btn-outline-primary'>Pregledaj</a></td>");
					red.append("<td style='width: 25px;'><button cenovnik_id='" + value.id + "' class='btn btn-outline-danger delete_cenovnik ' >Obriši</button></td>");
					tabelaCenovnika.append(red);
				})
			}
		})
	}

	function ucitajMesta() {
		$.ajax({
			url: "api/mesto",
			type: 'GET',
			success: function(data) {
				data.forEach(function(value) {
					$("#mestoDropdown").append('<option value="' + value.id + '">' + value.naziv + '</option>');
				})
			}
		})
	}

	$("#updatePreduzece").on("click", function() {
		event.preventDefault();
		$("#naziv").val(preduzece.naziv);
		$("#adresa").val(preduzece.adresaPreduzeca);
		$("#telefon").val(preduzece.telefon);
		$("#email").val(preduzece.email);
		$("#broj_racuna").val(preduzece.brojRacuna);
		$("#pibUpdate").val(preduzece.pib);
		$("#mbUpdate").val(preduzece.mb);
		$("#logo").val(preduzece.logo);
		$("#mestoDropdown").val(preduzece.mesto);
		$("#update_preduzece_modal").modal("show");
	});

	$("#update_confirm").on("click", function(event) {
		event.preventDefault();
		$("#update_preduzece_modal").modal("hide");
		preduzece.naziv = $("#naziv").val();
		preduzece.adresaPreduzeca = $("#adresa").val();
		preduzece.telefon = $("#telefon").val();
		preduzece.email = $("#email").val();
		preduzece.brojRacuna = $("#broj_racuna").val();
		preduzece.pib = $("#pibUpdate").val();
		preduzece.mb = $("#mbUpdate").val();
		preduzece.logo = $("#logo").val();
		preduzece.mesto = $("#mestoDropdown").val();

		$.ajax({
			url: "/api/preduzece/1",
			type: 'PUT',
			data: JSON.stringify(preduzece),
			contentType: "application/json"
		}).done(function() {
			alert("Izmene su uspešno obavljene!");
			ucitajPreduzece();
		});
	});

	$("#add_new_cenovnik").on("click", function(event) {
		event.preventDefault();
		addCenovnik.modal("show");
		buttons.add.show();
		buttons.edit.hide();
	});

	buttons.add.on("click", function(event) {
		c = {};
		event.preventDefault();
		var datumOd = $("#datumOd");
		var datumDo = $("#datumDo")
		c.datumVazenjaOd = datumOd.val();
		c.datumVazenjaDo = datumDo.val();
		c.preduzeceId = 1;
		c.poslovniPartnerId = null;

		console.log(c.datumVazenjaDo);

		if (datumOd.val() > datumDo.val()) {
			addCenovnik.modal("hide");
			alert("Početni datum mora biti pre krajnjeg!");
			return;
		}

		$.ajax({
			url: "api/preduzece/1/cenovnici",
			type: 'GET',
			success: function(data) {
				data.forEach(function(value) {
					if ((datumOd <= value.datumVazenjaOd && value.datumVazenjaOd >= datumDo) || (datumOd <= value.datumVazenjaDo && value.datumVazenjaDo <= datumDo)
						|| (value.datumVazenjaOd < datumOd && datumDo < value.datumVazenjaDo)) {
						alert("test");
					}
				})
			}
		})

		$.ajax({
			url: 'api/cenovnik?preduzece=true&id=1',
			type: 'POST',
			data: JSON.stringify(c),
			contentType: "application/json"
		}).done(function() {
			ucitajCenovnike();
			alert("Cenovnik uspešno dodat!");
		});
		addCenovnik.modal("hide");
		c = {};
	});

	tabelaCenovnika.on("click", "button.delete_cenovnik", function(event) {
		event.preventDefault();
		var cenovnikID = $(this).attr("cenovnik_id");
		deleteContent.text("Da li ste sigurni da želite da obrišete cenovnik koji ima id=" + cenovnikID + " ?");
		deleteCenovnik.modal("show");
		$("#delete_confirm").on("click", function(event) {
			event.preventDefault();
			$.ajax({
				url: 'api/cenovnik/' + cenovnikID,
				type: 'DELETE',
				contentType: "application/json"
			}).done(function() {
				ucitajCenovnike();
				alert("Cenovnik uspešno obrisan!");
			});
			deleteCenovnik.modal("hide");
		});
	});


	tabelaCenovnika.on("click", "button.update_cenovnik", function(event) {
		event.preventDefault();
		var cenovnikID = $(this).attr("cenovnik_id");
		c.id = cenovnikID;
		$.ajax({
			url: 'api/cenovnik/' + cenovnikID,
			type: 'GET',
			success: function(data) {
				addCenovnik.modal("show");
				buttons.edit.show();
				buttons.add.hide();
				var datum = $("#datum");
				var date = new Date(data.datumVazenjaOd);
				var dateStr = date.getFullYear() + "-" + (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1) + "-" + (date.getDate() < 10 ? "0" : "") + date.getDate();
				datum.val(dateStr);
			}
		});
	});

	buttons.edit.on("click", function(event) {
		event.preventDefault();

		var datum = $("#datum");
		c.datumVazenjaOd = datum.val();
		$.ajax({
			url: 'api/cenovnik/' + c.id,
			type: 'PUT',
			data: JSON.stringify(c),
			contentType: "application/json"
		}).done(function() {
			ucitajCenovnike();
			alert("Cenovnik uspešno izmenjen!");
		});
		addCenovnik.modal("hide");
		c = {};
	});

});