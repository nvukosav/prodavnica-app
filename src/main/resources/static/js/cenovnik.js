$(document).ready(function() {
	$("#navigation").load("nav.html");

	var id = getParameters()['id'];
	var tabelaStavki = $("#tabela");
	var deleteStavka = $("#delete_stavka");
	var updateStavka = $("#update_stavke");
	var deleteContent = $("#delete_content");
	var stavkaHtml = {
		roba: $("#add_edit_roba"),
		cena: $("#cena-stavke"),
	};
	var nameSearch = $("#naziv-robe");

	var stavka = {
		id: 0,
		cena: 0,
		cenovnik: id,
		roba: 1
	};

	buttons = {
		addEdit: $("#add_edit_stavka")
	};

	var cenovnikHtml = {
		datumVazenjaOd: $("#datum-vazenja"),
		preduzece: {
			naziv: $("#naziv-preduzeca"),
			adresa: $("#adresa-preduzeca"),
			pib: $("#pib-preduzeca"),
			telefon: $("#telefon-preduzeca"),
			email: $("#email-preduzeca"),
			link: $("#show-preduzece")
		}
	};

	function ucitajStavkeCenovnika() {
		tabelaStavki.empty();
		var naziv = nameSearch.val();
		$.ajax({
			url: `api/cenovnik/${id}/stavke_cenovnika?naziv=${naziv}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {
					red = $("<tr></tr>");
					var nazivRobe;
					$.ajax({
						url: 'api/roba/' + value.roba,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) {
							nazivRobe = data.nazivRobe;
						}
					});
					red.append("<td>" + nazivRobe + "</td>");
					red.append("<td>" + value.cena + " din</td>");
					red.append("<td class='text-center'>" +
						"<button stavka_id='" + value.id + "' class='btn btn-outline-primary update_stavka mr-3 '>Izmeni</button>" +
						"<button stavka_id='" + value.id + "' class='btn btn-outline-danger delete_stavka'>Obriši</button></td>");
					tabelaStavki.append(red);
				});
			}
		});
	}

	function getParameters() {
		var param = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			param.push(hash[0]);
			param[hash[0]] = hash[1];
		}
		return param;
	}

	$("#id").text(id);
	$.get("api/cenovnik/" + id, function(data) {
		var dateOd = new Date(data.datumVazenjaOd);
		var dateDo = new Date(data.datumVazenjaDo);
		cenovnikHtml.datumVazenjaOd.text(dateOd.toLocaleDateString() + " - " + dateDo.toLocaleDateString());
		ucitajStavkeCenovnika();
	});

	$.get("api/roba", function(data) {
		data.forEach(function(value) {
			stavkaHtml.roba.append("<option value='" + value.id + "'>" + value.nazivRobe + "</option>");
		});
	});

	tabelaStavki.on("click", "button.delete_stavka", function(event) {
		event.preventDefault();
		stavka.id = $(this).attr("stavka_id");
		deleteContent.text("Da li ste sigurni da želite da obrišete stavku koja ima id=" + stavka.id + " ?");
		deleteStavka.modal("show");
	});

	tabelaStavki.on("click", "button.delete_stavka", function(event) {
		event.preventDefault();
		stavka.id = $(this).attr("stavka_id");
		deleteContent.text("Da li ste sigurni da želite da obrišete stavku koja ima id=" + stavka.id + " ?");
		deleteStavka.modal("show");
	});

	$("#delete_confirm").on("click", function(event) {
		event.preventDefault();
		$.ajax({
			url: 'api/stavka_cenovnika/' + stavka.id,
			type: 'DELETE',
			contentType: "application/json"
		}).done(function() {
			ucitajStavkeCenovnika();
			alert("Stavka cenovnika uspešno obrisana!");
			stavka.id = 0;
		});
		deleteStavka.modal("hide");
	});

	$("#add_new_stavka").on("click", function(event) {
		event.preventDefault();
		updateStavka.modal("show");
	});

	tabelaStavki.on("click", "button.update_stavka", function(event) {
		event.preventDefault();
		stavka.id = $(this).attr("stavka_id");
		$.ajax({
			url: 'api/stavka_cenovnika/' + stavka.id,
			type: 'GET',
			success: function(data) {
				updateStavka.modal("show");
				stavkaHtml.cena.val(data.cena);
				stavkaHtml.roba.val(data.roba);
			}
		});
	});
	buttons.addEdit.on("click", function(event) {
		event.preventDefault();
		stavka.cena = stavkaHtml.cena.val();
		console.log(stavka.cena);
		stavka.roba = stavkaHtml.roba.val();
		console.log("roba usluga id: " + stavka.roba);
		console.log("cenovnik id: " + stavka.cenovnik);
		var url = "api/stavka_cenovnika";
		var method = "POST";
		if (stavka.id != 0) {
			url += "/" + stavka.id;
			method = "PUT";
		}
		$.ajax({
			url: url,
			type: method,
			data: JSON.stringify(stavka),
			contentType: "application/json"
		}).done(function() {
			ucitajStavkeCenovnika();
			if (stavka.id == 0) {
				alert("Stavka cenovnika dodata!");
			} else {
				alert("Stavka cenovnika izmenjena!");
			}
			stavka.id = 0;
		});
		updateStavka.modal("hide");
	});

});