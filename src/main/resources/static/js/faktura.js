$(document).ready(function(){
    $("#navigation").load("nav.html");
	
    var params = getParameters();
	var jedinicnaCena = 0;
	var porez;
	var vrstaFakture;
	

	
	$.ajax({url: 'api/fakture/'+params["id"],
		dataType: "json",
		contentType: "application/json",
		success: function(data){
            var poslovnaGodina;
			$.ajax({
                url: 'api/poslovne_godine/'+data.poslovnaGodina,
                type: 'GET',
                async: false,
                success: function(data) { poslovnaGodina = data.godina; }
            });
			//$("#addStavka").hide();
			if (!data.placeno) $("#addStavka").show();
			$("#brojRacuna").text(data.brFakture+"/"+poslovnaGodina);
			$("#datumIzdavanja").text(new Date(data.datumFakture).toLocaleString());
			$("#datumValute").text(new Date(data.datumValute).toLocaleString());
			$("#ukupanRabat").text(data.rabat);
			$("#ukupanPorez").text(data.ukupanPorez);
			$("#poreskaOsnovica").text(data.osnovica);
			$("#ukupanIznosFakture").text(Math.round(data.iznosZaPlacanje));
			getGrupa(data.preduzece);
			console.log(data.preduzece);
			getPoslovniPartner(data.poslovniPartner);
			vrstaFakture = data.vrstaFakture;
			console.log("vrsta fakture: " + data.vrstaFakture);
			console.log("vrsta fakture2: " + vrstaFakture);
			if(vrstaFakture === true){
				$("#addStavka").hide();
			}
		}});
	
	$("#addStavka").click(function(e){
		$("#addModal").modal("show");
		$("#confirmSave").on("click",function(e) {
			var kolicina = $("#kolicina").val();
			var rabat = $("#rabat").val();
			var iznosPorez = ((jedinicnaCena*kolicina)-rabat)*porez/100;
			var iznosBezPorez = ((jedinicnaCena*kolicina)-rabat);
			var ukupanIznos = ((jedinicnaCena*kolicina)-rabat)*(1+(porez/100));
			var roba = $("#roba").val();
			if (!roba || !kolicina || kolicina<=0 || rabat>(jedinicnaCena*kolicina))
				return;
			$.ajax({url: 'api/stavkafakture',
				dataType: 'json',
				contentType:'application/json',
				type:'POST',
				data: '{"kolicina":'+kolicina+',"cena":'+jedinicnaCena+',"iznosPorez":'+iznosPorez+',"rabat":'+rabat+',"osnovicaZaPorez":'+iznosBezPorez+',"procenatPorez":'+porez+',"iznosStavka":'+ukupanIznos+',"faktura":'+params["id"]+',"roba":'+roba+'}',
				complete: function(data){
					window.location.reload();
		                }
				});
		    	$("#addModal").modal("hide");
		   	});
	})
	
	function racunajCenu(){
		var kolicina = $("#kolicina").val();
		var rabat = $("#rabat").val();
		$("#iznosPorez").text(((jedinicnaCena*kolicina)-rabat)*porez/100);
		$("#iznosBezPorez").text((jedinicnaCena*kolicina)-rabat);
		$("#ukupanIznos").text(Math.round(((jedinicnaCena*kolicina)-rabat)*(1+(porez/100))));
	};
	
	$("#kolicina, #rabat").on("input",function(e){
		e.preventDefault();
		racunajCenu();
	})
	
	$("#roba").change(function(e){
	e.preventDefault();
	var id = $("#roba").val();
	$.ajax({url: 'api/roba/'+id+'/cena',
		dataType: "json",
		contentType: "application/json",
		async: false,
		success: function(data){
			console.log(data);
			jedinicnaCena = data.cena;
			$("#jedinicnaCena").text(data.cena);
			getPorez();
			racunajCenu();
		}});
	})
	
	function getPorez(){
		var id = $("#roba").val();
		$.ajax({url: 'api/roba/'+id+'/porez',
			dataType: "json",
			contentType: "application/json",
			async: false,
			success: function(data){
				$.ajax({url: 'api/porez/'+data.id+'/stopa',
					dataType: "json",
					contentType: "application/json",
					async: false,
					success: function(data){
						porez = data.procenat;
					}});
			}});
	};
	
	$.ajax({url: 'api/fakture/'+params["id"]+'/stavke',
		dataType: "json",
		contentType: "application/json",
		success: function(data){
			console.log(data);
			addStavke(data);
		}});
	
	
	function addStavke(data){
		for (var i = 0; i < data.length; i++){
			var roba;
			var grupa;
			$.ajax({url: 'api/roba/'+data[i].roba,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					roba=data;
				}});
			$.ajax({url: 'api/grupa/'+roba.grupa,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					grupa=data.nazivGrupe;
				}});
			$("#stavkeTable tbody").append("<tr><td>"+(i+1)+"</td><td>"+roba.nazivRobe+"</td><td>"+grupa+"</td><td>"+
					roba.jedinicaMere+"</td><td>"+data[i].kolicina+"</td><td>"+data[i].cena+"</td><td>"+
					parseFloat((data[i].rabat*100)/(data[i].cena*data[i].kolicina)).toFixed(2)+"</td><td>"+data[i].osnovicaZaPorez+"</td><td>"+data[i].procenatPorez+"</td><td>"+
					data[i].iznosPorez+"</td><td>"+Math.round(data[i].iznosStavka)+"</td></tr>")
		};
	}
	
	function getPoslovniPartner(id){
		$.ajax({url: 'api/poslovni_partneri/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				$("#nazivPP").text(data.nazivPartnera);
				var racun = data.brojRacuna.slice(0, 3) + "-" + data.brojRacuna.slice(3, 15) + "-" + data.brojRacuna.slice(15);
				$("#brojRacunaPP").text(racun);
				$("#adresaIMestoPP").text(data.adresa);
				getMesto(data.mesto);
			}});
	}
	
	function getGrupa(id){		
		$("#roba").empty();
		$.ajax({url: 'api/fakture/'+params["id"]+'/robaCenovnika',
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#roba").append("<option value='"+data[i].id+"'>"+data[i].nazivRobe+"</option>");
				}
				$("#roba").val(0);
			}});
	}
	
	function getMesto(id){
		$.ajax({url: 'api/mesto/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				$("#adresaIMestoPP").text($("#adresaIMestoPP").text() + ", " + data.postanskiBroj + " " + data.naziv+", "+data.drzava);
			}});
	}
	
	function getParameters(){
	    var param = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        param.push(hash[0]);
	        param[hash[0]] = hash[1];
	    }
	    return param;
	}
	
});