$(document).ready(function() {
	$("#navigation").load("nav.html");

	var newFakturaHtml;
	var tabelaFaktura = $("#tabelaFaktura");
	var tip_fakture;
	var nameSearch = $('#naziv-poslovnog-partnera');
	var izvestaji = $("#izvestaj_preduzece");
	var buttons = {
		confirm: $("#confirm-izvestaj"),
	};

	ucitajPoslovneGodine();
	ucitajFakture();


	function ucitajFakture() {
		var tip = $("#tip-fakture input:checked").val();
		if (tip == 1) {
			$("#poslovni-partner").text("Naziv i mesto dobavljača");
		} else {
			$("#poslovni-partner").text("Naziv i mesto kupca");
		}
		tabelaFaktura.empty();
		var tip_fakture = tip == 1 ? "ulazne" : "izlazne";
		var godina = $("#poslovneGodineDropdown").val();
		var naziv = nameSearch.val();
		$.ajax({
			url: `api/fakture/${tip_fakture}?godina=${godina}&naziv=${naziv}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {
					var poslovnaGodina;
					var poslovniPartner;
					var mesto;
					$.ajax({
						url: 'api/poslovne_godine/' + value.poslovnaGodina,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovnaGodina = data.godina; }
					});
					$.ajax({
						url: 'api/poslovni_partneri/' + value.poslovniPartner,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovniPartner = data; }
					});
					$.ajax({
						url: 'api/mesto/' + poslovniPartner.mesto,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { mesto = data; }
					});
					red = $("<tr></tr>");
					red.append("<td>" + value.brFakture + "/" + poslovnaGodina + "</td>");
					red.append("<td>" + new Date(value.datumFakture).toLocaleString() + "</td>");
					red.append("<td>" + new Date(value.datumValute).toLocaleString() + "</td>");
					red.append("<td>" + poslovniPartner.nazivPartnera + "</br>" + mesto.naziv + "</td>");
					red.append("<td>" + poslovniPartner.adresa + "</td>");
					red.append("<td>" + value.iznosZaPlacanje + "</td>");

					red.append("<td><a href='faktura.html?id=" + value.id + "' class='btn btn-outline-primary'>Pregledaj</a></td>");

					red.append("<td><a href='api/fakture/" + value.id + "/report' class='btn btn-outline-light'>PDF</a></td>");
					tabelaFaktura.append(red);
				});
			}
		});
	}

	function ucitajPoslovneGodine() {
		$.ajax({
			url: "api/poslovne_godine",
			type: 'get',
			success: function(data) {
				var select = document.getElementById("godinaDropdown");
				data.forEach(function(value) {
					$("#poslovneGodineDropdown").append('<option value="' + value.id + '">' + value.godina + '</option>');
					select.options[select.options.length] = new Option(value.godina, value.godina);
				})
			}
		})
	}

	$("#get_izvestaj").on("click", function(event) {
		event.preventDefault();
		izvestaji.modal("show");
		buttons.confirm.on("click", function(event) {
			event.preventDefault();
			var godina = $("#godinaDropdown").val();
			var vrsta = $("#vrstaDropdown").val();

			$.get(`api/preduzece/1/reports/${vrsta}?godina=${godina}`, function() {
				$("#modal-pdf").attr('src', `api/preduzece/1/reports/${vrsta}?godina=${godina}`);
				izvestaji.modal("hide");
				$('#open_pdf').modal('show');
			})
				.fail(function() {
					izvestaji.modal("hide");
					alert("Izveštaj nije moguće generisati! Probajte sa promenom kriterijuma.");
				});
		});
	});

	$("#poslovneGodineDropdown").on('change', function(e) {
		ucitajFakture();
	});

	$("#tip-fakture input").on("change", function() {
		ucitajFakture();
	});

	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		ucitajFakture();
	});

});