$(document).ready(function() {
	$("#navigation").load("nav.html");

	$("#preduzece").text("Preduzeće");
	var tabela = $("#tblGrupeRobe");
	var deleteGrupa = $("#delete_grupa");
	var updateGrupa = $("#update_grupa");
	var deleteContent = $("#delete_content");
	var nameSearch = $("#naziv-grupe");
	buttons = {
		add: $("#add_grupa"),
		edit: $("#edit_grupa")
	};

	popunjavanjeTabele();

	function popunjavanjeTabele() {
		tabela.empty();
		var naziv = nameSearch.val();
		$.ajax({
			url: `api/grupa?naziv=${naziv}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				getPreduzeca();
				getPorez();
				data.forEach(function(value) {
					var preduzece;
					var porez;

					$.ajax({
						url: 'api/preduzece/' + value.preduzece,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) {
							preduzece = data;
						}
					});
					$.ajax({
						url: 'api/porez/' + value.porez,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) {
							porez = data;
						}
					});

					red = $("<tr></tr>");
					red.append("<td>" + value.nazivGrupe + "</br></td>");
					red.append("<td>" + porez.nazivPoreza + "</br></td>");
					red.append("<td><a href='roba.html?grupa=" + value.id + "' class='btn btn-outline-primary'>Pregledaj</a></td>");
					red.append("<td><button grupa_id='" + value.id + "' class='btn btn-outline-warning update_grupa'>Izmeni</button> </td>");
					red.append("<td> <button grupa_id='" + value.id + "' class='btn btn-outline-danger delete_grupa'>Obriši</button></td>")
					tabela.append(red);
				});
			}
		});
	}

	function getPreduzeca() {
		$.ajax({
			url: 'api/preduzece/',
			type: 'GET',
			success: function(data) {
				$("#selectPreduzece").empty();
				preduzeca = data;
				var select = document.getElementById("selectPreduzece");
				for (var i = 0; i < preduzeca.length; i++) {
					select.options[select.options.length] = new Option(preduzeca[i].naziv, preduzeca[i].id);
				}
			}
		});
	}

	function getPorez() {
		$.ajax({
			url: 'api/porez/',
			type: 'GET',
			success: function(data) {
				$("#selectPorez").empty();
				var porez = data;
				var select = document.getElementById("selectPorez");
				for (var i = 0; i < porez.length; i++) {
					select.options[select.options.length] = new Option(porez[i].nazivPoreza, porez[i].id);
				}
			}
		});
	}

	$("#add_new_grupa").on("click", function(event) {
		event.preventDefault();
		updateGrupa.modal("show");
		buttons.add.show();
		buttons.edit.hide();
		buttons.add.on("click", function(event) {
			event.preventDefault();
			var grupa = {};
			var preduz = $("#selectPreduzece");
			var naziv = $("#naziv");
			var porez = $("#selectPorez");
			grupa.nazivGrupe = naziv.val();
			grupa.preduzece = preduz.val();
			grupa.porez = porez.val();

			$.ajax({
				url: 'api/grupa',
				type: 'POST',
				data: JSON.stringify(grupa),
				contentType: "application/json"
			}).done(function() {
				popunjavanjeTabele();
				alert("Grupa dodata!");
			});
			updateGrupa.modal("hide");
		});
	});

	tabela.on("click", "button.update_grupa", function(event) {
		event.preventDefault();
		var grupaId = $(this).attr("grupa_id");
		$.ajax({
			url: 'api/grupa/' + grupaId,
			type: 'GET',
			success: function(data) {
				updateGrupa.modal("show");
				buttons.edit.show();
				buttons.add.hide();

				var preduz = $("#selectPreduzece");
				var naziv = $("#naziv");
				var porez = $("#selectPorez");
				naziv.val(data.nazivGrupe);
				preduz.val(data.preduzece);
				porez.val(data.porez);
				buttons.edit.on("click", function(event) {
					var grupa = {};
					event.preventDefault();
					grupa.nazivGrupe = naziv.val();
					grupa.preduzece = preduz.val();
					grupa.porez = porez.val();
					grupa.id = grupaId;
					$.ajax({
						url: 'api/grupa/' + grupaId,
						type: 'PUT',
						data: JSON.stringify(grupa),
						contentType: "application/json"
					}).done(function() {
						$("#tblGrupeRobe").empty();
						popunjavanjeTabele();
						alert("Grupa uspešno izmenjena!");
					});
					updateGrupa.modal("hide");
					grupa = null;
				});
			}
		});
	});

	tabela.on("click", "button.delete_grupa", function(event) {
		console.log("delete clicked");
		event.preventDefault();
		var grupaId = $(this).attr("grupa_id");
		deleteContent.text("Da li ste sigurni da želite da obrišete grupu koje ima id=" + grupaId + " ?");
		deleteGrupa.modal("show");
		$("#delete_confirm").on("click", function(event) {
			event.preventDefault();
			$.ajax({
				url: 'api/grupa/' + grupaId,
				type: 'DELETE',
				contentType: "application/json"
			}).done(function() {
				popunjavanjeTabele();
				alert("Grupa je obrisana!");
			});
			deleteGrupa.modal("hide");
		});
	});


	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		popunjavanjeTabele();
	});

});