$(document).ready(function() {
	$("#navigation").load("nav.html");
	
	var tabela = $("#tblMesta");
	var deleteMesto = $("#delete_mesto");
	var updateMesto = $("#update_mesto");
	var deleteContent = $("#delete_content");
	var deleteMesto = $("#delete_mesto");
	$("#postanskiBroj").keyup(proveraPB);
	var buttons = {
		add: $("#add_mesto"),
		edit: $("#edit_mesto")
	};
	var nameSearch = $("#naziv-filter");
	popunjavanjeTabele();

	var mestoHtml = {
		naziv: $("#nazivMesta"),
		pb: $("#postanskiBroj"),
		drzava: $("#drzava")

	};

	function popunjavanjeTabele() {
		tabela.empty();
		var nazivFilter = nameSearch.val();
		$.ajax({
			url: `api/mesto?naziv=${nazivFilter}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {
					red = $("<tr></tr>");
					red.append("<td>" + value.naziv + "</br></td>");
					red.append("<td>" + value.postanskiBroj + "</br></td>");
					red.append("<td>" + value.drzava + "</br></td>");

					red.append("<td class='text-right'>" +
						"<button mesto_id='" + value.id + "' class='btn btn-outline-warning update_mesto'>Izmeni</button>");
					red.append("<td class='text-left'>" +
						"<button mesto_id='" + value.id + "' class='btn btn-outline-danger delete_mesto'>Obriši</button>");
					tabela.append(red);
				});
			}
		});
	}

	function proveraPB() {
		var pb = $("#postanskiBroj").val();

		if (parseInt(pb) > 10000) {
			document.getElementById("add_mesto").disabled = false;
		} else {
			document.getElementById("add_mesto").disabled = true;

		}
		if (parseInt(pb) > 10000) {
			document.getElementById("edit_mesto").disabled = false;
		} else {
			document.getElementById("edit_mesto").disabled = true;
		}
	}

	$("#add_new_mesto").on("click", function(event) {
		event.preventDefault();
		updateMesto.modal("show");
		buttons.add.show();
		buttons.edit.hide();
		buttons.add.on("click", function(event) {
			var mesto = {};
			event.preventDefault();
			mesto.naziv = mestoHtml.naziv.val();
			mesto.postanskiBroj = mestoHtml.pb.val();
			mesto.drzava = mestoHtml.drzava.val();
			$.ajax({
				url: 'api/mesto',
				type: 'POST',
				data: JSON.stringify(mesto),
				contentType: "application/json"
			}).done(function() {
				tabela.empty();
				popunjavanjeTabele();
				alert("Mesto je dodato!");
			});
			updateMesto.modal("hide");
		});
	});

	tabela.on("click", "button.update_mesto", function(event) {
		event.preventDefault();
		var mestoID = $(this).attr("mesto_id");
		$.ajax({
			url: 'api/mesto/' + mestoID,
			type: 'GET',
			success: function(data) {
				updateMesto.modal("show");
				buttons.edit.show();
				buttons.add.hide();
				mestoHtml.naziv.val(data.naziv);
				mestoHtml.pb.val(data.postanskiBroj);
				mestoHtml.drzava.val(data.drzava);
				buttons.edit.on("click", function(event) {
					var mesto = {};
					event.preventDefault();
					mesto.naziv = mestoHtml.naziv.val();
					mesto.postanskiBroj = mestoHtml.pb.val();
					mesto.drzava = mestoHtml.drzava.val();
					mesto.id = mestoID;
					$.ajax({
						url: 'api/mesto/' + mestoID,
						type: 'PUT',
						data: JSON.stringify(mesto),
						contentType: "application/json"
					}).done(function() {
						popunjavanjeTabele();
						alert("Mesto je izmenjeno!");
					});
					updateMesto.modal("hide");
					mesto = null;
				});
			}
		});
	});

	tabela.on("click", "button.delete_mesto", function(event) {
		console.log("kliknuto delte dugme");
		event.preventDefault();
		var mestoID = $(this).attr("mesto_id");
		console.log("posle mestoid");
		deleteContent.text("Da li ste sigurni da želite da obrišete mesto koje ima id=" + mestoID + " ?");
		console.log("posle deleteContent");
		deleteMesto.modal("show");
		console.log("posle pokazivanja modal-a");
		$("#delete_confirm").on("click", function(event) {
			event.preventDefault();
			$.ajax({
				url: 'api/mesto/' + mestoID,
				type: 'DELETE',
				contentType: "application/json"
			}).done(function() {
				popunjavanjeTabele();
				alert("Mesto je obrisano!");
			});
			deleteMesto.modal("hide");
		});
	});

	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		popunjavanjeTabele();
	});
});