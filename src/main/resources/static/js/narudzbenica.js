$(document).ready(function(){
	
    $("#navigation").load("nav.html");
    
	var params = getParameters();
	var obrisano=false;
	var stavka;
	var deleteContent = $("#delete_content");
	var deleteStavka = $("#delete_stavka");
    
	function addStavke(data){
		for (var i = 0; i < data.length; i++){
		if (!data[i].obrisano) {
					var roba;
        			var grupa;
        			$.ajax({url: 'api/roba/'+data[i].roba,
        				dataType: "json",
        				contentType: "application/json",
        				async: false,
        				success: function(data){
        					roba=data;
        				}});
        			$.ajax({url: 'api/grupa/'+roba.grupa,
        				dataType: "json",
        				contentType: "application/json",
        				async: false,
        				success: function(data){
        					grupa=data.nazivGrupe;
						}});
					if (!obrisano) {
        				$("#stavkeTable tbody").append("<tr><td>"+(i+1)+"</td><td>"+roba.nazivRobe+"</td><td>"+grupa+"</td><td>"+
        					roba.jedinicaMere+"</td><td>"+data[i].kolicina+"</td><td> <button stavka_id='" + data[i].id + "' class='btn btn-outline-danger delete_stavka'>Obriši</button></td></tr>")

        			}else{
						$("#stavkeTable tbody").append("<tr><td>"+(i+1)+"</td><td>"+roba.nazivRobe+"</td><td>"+grupa+"</td><td>"+
        					roba.jedinicaMere+"</td><td>"+data[i].kolicina+"</td></tr>")
					}
		    }

		};
	}

		
	function getPoslovniPartner(id){
		$.ajax({url: 'api/poslovni_partneri/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				$("#nazivPP").text(data.nazivPartnera);
				$("#brojRacunaPP").text(data.brojRacuna);
				$("#adresaIMestoPP").text(data.adresa);
				getMesto(data.mesto);
			}});
	}
	
	function getGrupa(id){		
		$("#roba").empty();
		$.ajax({url: 'api/narudzbenice/'+params["id"]+'/robaCenovnika',
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#roba").append("<option value='"+data[i].id+"'>"+data[i].nazivRobe+"</option>");
				}
				$("#roba").val(0);
			}});
	}
	
	function getMesto(id){
		$.ajax({url: 'api/mesto/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				$("#adresaIMestoPP").text($("#adresaIMestoPP").text() + ", " + data.postanskiBroj + " " + data.naziv+", "+data.drzava);
			}});
	}
	
	function getParameters(){
	    var param = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        param.push(hash[0]);
	        param[hash[0]] = hash[1];
	    }
	    return param;
	}
	
	$.ajax({url: 'api/narudzbenice/' +params["id"],
		dataType: "json",
		contentType: "application/json",
		success: function(data){
            var poslovnaGodina;
			$.ajax({
                url: 'api/poslovne_godine/'+data.poslovnaGodina,
                type: 'GET',
                async: false,
                success: function(data) { 
					poslovnaGodina = data.godina; 
				}
			});
			if(data.obrisano==true || data.tipNarudzbenice==0){
				console.log("tip narudzbenice: " + data.tipNarudzbenice )
				document.getElementById('addStavkaNarudzbenica').style.visibility='hidden';
			}
            obrisano = data.obrisano
			$("#addStavka").show();
			$("#brojRacuna").text(data.brojNarudzbenice+"/"+poslovnaGodina);
			$("#datumIzdavanja").text(new Date(data.datumNarudzbenice).toLocaleString());
			
			getGrupa(data.preduzece);
			getPoslovniPartner(data.poslovniPartner);
		}});
	
	$.ajax({url: 'api/narudzbenice/'+params["id"]+'/stavkeNarudzbenice',
		dataType: "json",
		contentType: "application/json",
		success: function(data){
			console.log("narudzbenice/id/stavke" + data);
			addStavke(data);
			
		}
		
	});
	
	$("#addStavkaNarudzbenica").click(function(e){
		$("#addModal").modal("show");
		$("#confirmSave").on("click",function(e) {
			var kolicina = $("#kolicina").val();
			var roba = $("#roba").val();
			var robaNaziv;
			var jedinicaM;
			if (!roba || !kolicina || kolicina<=0 )
				return;
			$.ajax({url: 'api/roba/'+roba,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					robaNaziv=data.nazivRobe;
					jedinicaM=data.jedinicaMere;
			}});

			var stavkeNarudzbenice = {
				jedinicaMere: jedinicaM,
				kolicina: kolicina,
				opisRobe: robaNaziv,
				roba:roba,
				narudzbenica: params["id"]
			}
			$.ajax({url: 'api/stavka_narudzbenice',
				dataType: 'json',
				contentType:'application/json',
				type:'POST',
				data: JSON.stringify(stavkeNarudzbenice),
				complete: function(data){
					window.location.reload();
		                }
				});
		    	$("#addModal").modal("hide");
		   	});
	})
	
	$("#stavkeTable").on("click","button.delete_stavka", function (event) {
		event.preventDefault();
           stavka = $(this).attr("stavka_id");
		   deleteContent.text("Da li ste sigurni da želite da obrišete stavku?");
		   deleteStavka.modal("show");
		   console.log("stavka id: " + stavka);
		$("#delete_confirm").on("click",function (event) {
		event.preventDefault();
    	    $.ajax({url: 'api/stavka_narudzbenice/'+stavka,
        	    type: 'DELETE',
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					window.location.reload();
				}
			});
		});

    });
	
});