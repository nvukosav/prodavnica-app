$(document).ready(function() {
	$("#navigation").load("nav.html");

	var newNarudzbenicaHtml;
	var tabelaNarudzbenica = $("#tabelaNarudzbenica");
	var nameSearch = $('#naziv-poslovnog-partnera');

	$("#add_new_narudzbenica").load("dialog/add_narudzbenica.html", function() {
		newNarudzbenicaHtml = {
			poslovniPartner: $("#narudzbenica-poslovni-partner"),
			add: $("#add-narudzbenica")
		};

		ucitajNarudzbenice();
		ucitajPoslovneGodine();
		ucitajPartnere();

		function ucitajPartnere() {
			$.get("api/poslovni_partneri", function(data) {
				newNarudzbenicaHtml.poslovniPartner.empty();
				data.forEach(function(value) {
					if (value.vrstaPartnera  ===  1) {
						newNarudzbenicaHtml.poslovniPartner.append('<option value="' +  value.id + '">' + value.nazivPartnera + '</option>');
					}
				});
			});
		}


		function ucitajPoslovneGodine() {
			$.ajax({
				url: "api/poslovne_godine",
				type: 'get',
				success: function(data) {
					data.forEach(function(value) {
						$("#poslovneGodineDropdown").append('<option value="' + value.id + '">' + value.godina + '</option>');
					})
				}
			})
		}

		newNarudzbenicaHtml.add.on("click", function(event) {
			event.preventDefault();
			var novaNarudzbeinca = {
				preduzece: 1,
				poslovniPartner: newNarudzbenicaHtml.poslovniPartner.val()
			};
			$.ajax({
				url: 'api/narudzbenice',
				type: 'POST',
				data: JSON.stringify(novaNarudzbeinca),
				contentType: "application/json",
				success: function(data) {
					location.replace("narudzbenica.html?id=" + data.id);
				},
				error: function(xhr, ajaxOptions, thrownError) {
					console.log(novaNarudzbeinca.poslovniPartner);
					console.log(novaNarudzbeinca.poslovnaGodina);
				}
			});
		});
	});

	$("#poslovneGodineDropdown").on('change', function(e) {
		ucitajNarudzbenice();
	});

	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		ucitajNarudzbenice();
	});

	tabelaNarudzbenica.on("click", "button.napravi_otpremnicu", function(event) {
		event.preventDefault();
		var narudzbenica = $(this).attr("narudzbenica_otpremnica_id");
		$.ajax({
			url: 'api/narudzbenice/' + narudzbenica + '/napraviOtpremnicu',
			type: 'POST',
			contentType: "application/json",
			success: function() {
				alert("Generisanje otpremnice je uspešno!");
				ucitajNarudzbenice();
			}
		});
	});

	tabelaNarudzbenica.on("click", "button.napravi_fakturu", function(event) {
		event.preventDefault();
		var narudzbenica = $(this).attr("narudzbenica_id");
		$.ajax({
			url: 'api/narudzbenice/' + narudzbenica + '/napraviFakturu',
			type: 'POST',
			contentType: "application/json",
			success: function() {
				alert("Generisanje fakture je uspešno!");
				ucitajNarudzbenice()
			}
		});
	});

	$("#tip-nar input").on("change", function() {
		ucitajNarudzbenice();
	});

	function ucitajNarudzbenice() {
		tabelaNarudzbenica.empty();
		var tip = $("#tip-nar input:checked").val();
		console.log(tip);
		var godina = $("#poslovneGodineDropdown").val();
		var naziv = nameSearch.val();

		var tip_nar = tip == 1 ? "ulazne" : "izlazne";

		$.ajax({
			url: `api/narudzbenice/${tip_nar}?godina=${godina}&naziv=${naziv}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {

					var poslovnaGodina;
					var poslovniPartner;
					var mesto;
					console.log(value.poslovnaGodina);
					$.ajax({
						url: 'api/poslovne_godine/' + value.poslovnaGodina,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovnaGodina = data.godina; }
					});
					$.ajax({
						url: 'api/poslovni_partneri/' + value.poslovniPartner,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovniPartner = data; }
					});
					$.ajax({
						url: 'api/mesto/' + poslovniPartner.mesto,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { mesto = data; }
					});
					red = $("<tr></tr>");
					red.append("<td>" + value.brojNarudzbenice + "/" + poslovnaGodina + "</td>");

					red.append("<td>" + poslovniPartner.nazivPartnera + "</br>" + mesto.naziv + "</td>");

					red.append("<td><a href='narudzbenica.html?id=" + value.id + "' class='btn btn-outline-primary'>Pregledaj</a></td>");

					if (poslovniPartner.vrstaPartnera == 0 && value.tipNarudzbenice == true && value.obrisano != true) {
						red.append("<td><button narudzbenica_otpremnica_id='" + value.id + "' class='btn btn-outline-light napravi_otpremnicu'>Napravi otpremnicu</a></td> ");
						red.append("<td><button narudzbenica_id='" + value.id + "' class='btn btn-outline-light napravi_fakturu'>Napravi fakturu</a></td>");
					} else if (value.obrisano != true) {
						red.append("<td class='center-left'><button narudzbenica_id='" + value.id + "' class='btn btn-outline-light napravi_fakturu'>Napravi fakturu</a></td>");

					}


					tabelaNarudzbenica.append(red);
				});
			}
		});
	}


});