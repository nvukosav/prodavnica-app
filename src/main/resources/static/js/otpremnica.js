$(document).ready(function(){
    $("#navigation").load("nav.html");

    var params = getParameters();
	var jedinicnaCena = 0;
	var obrisano=false;
	var stavka;
	
	var deleteContent = $("#delete_content");
	var deleteStavka = $("#delete_stavka");

	
	function getGrupa(id){		
		$("#roba").empty();
		$.ajax({url: 'api/otpremnice/'+params["id"]+'/robaCenovnika',
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#roba").append("<option value='"+data[i].id+"'>"+data[i].nazivRobe+"</option>");
				}
				$("#roba").val(0);
			}});
	}
	
	function getPoslovniPartner(id){
		$.ajax({url: 'api/poslovni_partneri/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				$("#nazivPP").text(data.nazivPartnera);
				var racun = data.brojRacuna.slice(0, 3) + "-" + data.brojRacuna.slice(3, 15) + "-" + data.brojRacuna.slice(15);
				$("#brojRacunaPP").text(racun);
				$("#adresaIMestoPP").text(data.adresa);
				getMesto(data.mesto);
			}});
	}
	
	function getParameters(){
	    var param = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        param.push(hash[0]);
	        param[hash[0]] = hash[1];
	    }
	    return param;
	}
	
	function getMesto(id){
		$.ajax({url: 'api/mesto/'+id,
			dataType: "json",
			contentType: "application/json",
			success: function(data){
				console.log(data);
				$("#adresaIMestoPP").text($("#adresaIMestoPP").text() + ", " + data.postanskiBroj + " " + data.naziv+", "+data.drzava);
			}});
	}
	
	function addStavke(data){
		for (var i = 0; i < data.length; i++){
		if (!data[i].obrisano) {
			var roba;
			var grupa;
			$.ajax({url: 'api/roba/'+data[i].roba,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					roba=data;
				}});
			$.ajax({url: 'api/grupa/'+roba.grupa,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					grupa=data.nazivGrupe;
				}});
			if (!obrisano) {
				$("#stavkeTable tbody").append("<tr><td>"+(i+1)+"</td><td>"+roba.nazivRobe+"</td><td>"+grupa+"</td><td>"+
					roba.jedinicaMere+"</td><td>"+data[i].kolicina+"</td><td>"+data[i].cena+"</td><td>"+data[i].kolicina*data[i].cena+"</td><td> <button stavka_id='" + (i+1) + "' class='btn btn-outline-danger delete_stavka'>Obriši</button></td></tr>")

			}else{
                $("#stavkeTable tbody").append("<tr><td>"+(i+1)+"</td><td>"+roba.nazivRobe+"</td><td>"+grupa+"</td><td>"+
					roba.jedinicaMere+"</td><td>"+data[i].kolicina+"</td><td>"+data[i].cena+"</td><td>"+data[i].kolicina*data[i].cena+"</td></tr>")
				}
			};
		}
	}	
	
	function racunajCenu(){
		var kolicina = $("#kolicina").val();
		console.log(kolicina);
		$("#ukupanIznos").text(jedinicnaCena*kolicina);
	};
	
	$("#stavkeTable").on("click","button.delete_stavka", function (event) {
		event.preventDefault();
        stavka = $(this).attr("stavka_id");
        deleteContent.text("Da li ste sigurni da želite da obrišete stavku "+stavka+" ?");
		deleteStavka.modal("show");
		$("#delete_confirm").on("click",function (event) {
			event.preventDefault();
            $.ajax({url: 'api/stavkaOtpremnice/'+stavka,
                type: 'DELETE',
        		dataType: "json",
        		contentType: "application/json",
        		async: false,
        		success: function(data){
        			 window.location.reload();
        			 $("#ukupanIznosOtpremnice").text("0");
				}
			});
		});
	});
		
	
	$.ajax({url: 'api/otpremnice/' +params["id"],
		dataType: "json",
		contentType: "application/json",
		success: function(data){
            var poslovnaGodina;
			$.ajax({
                url: 'api/poslovne_godine/'+data.poslovnaGodina,
                type: 'GET',
                async: false,
                success: function(data) { poslovnaGodina = data.godina; }
			});

			if(data.obrisano==true){
				document.getElementById('addStavkaOtpremnice').style.visibility='hidden';
			}

            obrisano = data.obrisano
			$("#addStavka").show();
			$("#brojRacuna").text(data.brojOtpremnice+"/"+poslovnaGodina);
			$("#datumIzdavanja").text(new Date(data.datumOtpremnice).toLocaleString());
			$("#ukupanIznosOtpremnice").text(data.iznosZaUplatu);

			getGrupa(data.preduzece);
			getPoslovniPartner(data.poslovniPartner);
		}});
	
	$.ajax({url: 'api/otpremnice/'+params["id"]+'/stavkeOtpremnice',
		dataType: "json",
		contentType: "application/json",
		success: function(data){
			console.log(data);
			addStavke(data);
		}});
	
	$("#addStavkaOtpremnice").click(function(e){
		$("#addModal").modal("show");
		$("#confirmSave").on("click",function(e) {
			var kolicina = $("#kolicina").val();
            var roba = $("#roba").val();
			var robaNaziv;
			var jedinicaM;
			if (!roba || !kolicina || kolicina<=0 )
				return;
			$.ajax({url: 'api/roba/'+roba,
				dataType: "json",
				contentType: "application/json",
				async: false,
				success: function(data){
					robaNaziv=data.nazivRobe;
                    jedinicaM=data.jedinicaMere;
                    
            }});
	           var stavkeOtpremnice = {
	   				jedinicaMere: jedinicaM,
	   				kolicina: kolicina,
	   				opisRobe: robaNaziv,
	   				roba:roba,
	                   otpremnica: params["id"],
	                   iznos:jedinicnaCena*kolicina,
	                   cena: jedinicnaCena
	               }

	   			$.ajax({url: 'api/stavkaOtpremnice',
	   				dataType: 'json',
	   				contentType:'application/json',
	   				type:'POST',
	   				data: JSON.stringify(stavkeOtpremnice),
	   				complete: function(data){
	   					window.location.reload();
	   		                }
	   				});
	   		    	$("#addModal").modal("hide");
	   		   	});
	   	});
	   	
	$("#roba").change(function(e){
		e.preventDefault();
		var id = $("#roba").val();
		$.ajax({url: 'api/roba/'+id+'/cena',
			dataType: "json",
			contentType: "application/json",
			async: false,
			success: function(data){
				console.log(data);
				jedinicnaCena = data.cena;
				$("#jedinicnaCena").text(data.cena);
				// getPorez();
				 racunajCenu();
			}});
	});
	   $("#kolicina").on("input",function(e){
		e.preventDefault();
		racunajCenu();
	});
	
});