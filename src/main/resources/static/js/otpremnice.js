$(document).ready(function() {
	$("#navigation").load("nav.html");

	var newOtpremnicaHtml;
	var tabelaOtpremnica = $("#tabelaOtpremnica");
	var nameSearch = $('#naziv-poslovnog-partnera');
	ucitajPoslovneGodine();
	ucitajOtpremnice();

	function ucitajOtpremnice() {


		tabelaOtpremnica.empty();

		var godina = $("#poslovneGodineDropdown").val();
		var naziv = nameSearch.val();
		$.ajax({
			url: `api/otpremnice/?godina=${godina}&naziv=${naziv}`,
			type: 'GET',
			contentType: "application/json",
			success: function(data, textStatus, request) {
				data.forEach(function(value) {
					var poslovnaGodina;
					var poslovniPartner;
					var mesto;
					$.ajax({
						url: 'api/poslovne_godine/' + value.poslovnaGodina,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovnaGodina = data.godina; }
					});
					$.ajax({
						url: 'api/poslovni_partneri/' + value.poslovniPartner,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { poslovniPartner = data; }
					});
					$.ajax({
						url: 'api/mesto/' + poslovniPartner.mesto,
						type: 'GET',
						async: false, // blokira zatvaranje prozora
						success: function(data) { mesto = data; }
					});
					red = $("<tr></tr>");
					red.append("<td>" + value.brojOtpremnice + "/" + poslovnaGodina + "</td>");

					red.append("<td>" + poslovniPartner.nazivPartnera + "</br>" + mesto.naziv + "</td>");
					red.append("<td>" + poslovniPartner.adresa + "</td>");

					red.append("<td><a href='otpremnica.html?id=" + value.id + "' class='btn btn-outline-primary'>Pregledaj</a></td>");

					if (value.obrisano != true) {
						red.append("<td><button otpremnica_id=' " + value.id + "' class='btn btn-outline-light napravi_fakturu'>Napravi fakturu</a></td>");
					}

					red.append("<td><button otpremnica_id='" + value.id + "' class='btn btn-outline-light pdf_izvestaj'>PDF</a></td>");

					tabelaOtpremnica.append(red);
				});
			}
		});
	}

	function ucitajPoslovneGodine() {
		$.ajax({
			url: "api/poslovne_godine",
			type: 'get',
			success: function(data) {
				data.forEach(function(value) {
					$("#poslovneGodineDropdown").append('<option value="' + value.id + '">' + value.godina + '</option>');
				})
			}
		})
	}

	function ucitajPartnere() {
		$.get("api/poslovni_partneri", function(data) {
			newOtpremnicaHtml.poslovniPartner.empty();
			var optKupci = $('<optgroup label="Kupci"></optgroup>');

			data.forEach(function(value) {
				if (value.vrstaPartnera === 0) {
					optKupci.append("<option value='" + value.id + "'>" + value.nazivPartnera + "</option>");
				}
			});
			newOtpremnicaHtml.poslovniPartner.append(optKupci);

		});

	}


	$("#add_new_otpremnica").load("dialog/add_otpremnica.html", function() {
		newOtpremnicaHtml = {
			poslovniPartner: $("#otpremnica-poslovni-partner"),
			add: $("#add-otpremnica")
		};
		ucitajPartnere();
		newOtpremnicaHtml.add.on("click", function(event) {
			event.preventDefault();
			var novaOtpremnica = {
				preduzece: 1,
				poslovniPartner: newOtpremnicaHtml.poslovniPartner.val()
			};
			$.ajax({
				url: 'api/otpremnice',
				type: 'POST',
				data: JSON.stringify(novaOtpremnica),
				contentType: "application/json",
				success: function(data) {
					location.replace("otpremnica.html?id=" + data.id);
				}
			});
		});
	});

	tabelaOtpremnica.on("click", "button.napravi_fakturu", function(event) {
		event.preventDefault();
		var otpremnica = $(this).attr("otpremnica_id");
		$.ajax({
			url: 'api/otpremnice/' + otpremnica + '/napraviFakturu',
			type: 'POST',
			contentType: "application/json",
			success: function() {
				alert("Generisanje fakture je uspešno!");
				ucitajOtpremnice();
			}
		});
	});

	tabelaOtpremnica.on("click", "button.pdf_izvestaj", function(event) {
		event.preventDefault();
		var otpremnicaId = $(this).attr("otpremnica_id");
		$.get(`api/otpremnice/${otpremnicaId}/report`, function() {
			$("#modal-pdf").attr('src', `api/otpremnice/${otpremnicaId}/report`);
			$('#open_pdf').modal('show');
		})
			.fail(function() {
				alert("Izveštaj ne postoji ili ga nije moguće generisati!");
			});
	});


	$("#poslovneGodineDropdown").on('change', function(e) {
		ucitajOtpremnice();
	});

	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		ucitajOtpremnice();
	});
});