$(document).ready(function() {
    $("#navigation").load("nav.html");
    
    var tabelaPartnera = $("#tabelaPartnera");
    var deleteContent = $('#delete_content');
    var deletePartner = $('#delete_partner');
    var mestoDropdown = $("#mestoDropdown");
    var updatePartnerModal = $("#update_partner");
    var poslovniPartner = {};
    var partnerId;
    var searchFilter = $("#filter");
    
    ucitajPartnere();
    ucitajMesta();
    
    function ucitajPartnere(){
        tabelaPartnera.empty();
        var tip = $("#tip-partnera input:checked").val();
        var filter = searchFilter.val();
        $.ajax({
            url: `api/poslovni_partneri?filter=${filter}&tip=${tip}`,
            type: 'GET',
            contentType:"application/json",
            success: function(data, textStatus, request) {
                data.forEach(function (value) {
                    var mesto;
                    $.ajax({
                        url: 'api/mesto/'+value.mesto,
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            mesto = data

                        }
                    });
                    red = $("<tr></tr>");
                    red.append("<td>"+value.nazivPartnera + "</td>");
                    red.append("<td>"+value.adresa +" - " + mesto.naziv + "</td>");
                    red.append("<td>"+value.brojRacuna + "</td>");
                    red.append("<td>"+value.pib + "</td>");
                    red.append("<td>"+value.mb + "</td>");
                    red.append("<td><button partner_id='"+value.id+"' class='btn btn-outline-primary update_partner'>Izmeni</button></td>");
                    red.append("<td><button partner_id='"+value.id+"' class='btn btn-outline-danger delete_partner'>Obriši</button></td>");
                    tabelaPartnera.append(red);
                });
            }
        })
    }
    
    function ucitajMesta(){
        $.ajax({
            url:"api/mesto",
            type: 'GET',
            success: function(data){
                data.forEach(function(value){
                    mestoDropdown.append('<option value="' + value.id + '">' + value.naziv + '</option>');
                    $("#a_mestoDropdown").append('<option value="' + value.id + '">' + value.naziv + '</option>');
                })
            }
        })
    }
    
    function clearAddModal(){
        $("#a_naziv").val('');
        $("#a_adresa").val('');
        $("#a_partnerTip").val('');
        $("#a_brojRacuna").val('');
        $("#a_pib").val('');
        $("#a_mb").val('');
        $("#a_mestoDropdown").val('');
    }
    
    $("#addPartnerConfirm").on("click", function(event){
        event.preventDefault();
        $("#add_partner").modal("hide");
        var newPartner = {};
        newPartner.nazivPartnera = $("#a_naziv").val();
        newPartner.adresa = $("#a_adresa").val();
        newPartner.vrstaPartnera = $("#a_partnerTip").val();
        newPartner.brojRacuna = $("#a_broj_racuna").val();
        newPartner.preduzece = 1;
        newPartner.pib = $("#a_pib").val();
        newPartner.mb = $("#a_mb").val();
        newPartner.mesto = $("#a_mestoDropdown").val();
//        clearAddModal();

console.log("partner", newPartner);


        $.ajax({
            url: "api/poslovni_partneri",
            type: 'POST',
            data: JSON.stringify(newPartner),
            contentType: "application/json"
        }).done(function(){
        	tabelaPartnera.empty();
			alert("Partner je dodat!");
            ucitajPartnere();

        })
    });
    
    tabelaPartnera.on("click", "button.update_partner", function(event){
        event.preventDefault();
        partnerId = $(this).attr("partner_id");
        $.ajax({
            url:'api/poslovni_partneri/' + partnerId,
            type: 'GET',
            success: function(data){
                poslovniPartner.id = data.id;
                poslovniPartner.nazivPartnera = data.nazivPartnera;
                poslovniPartner.adresa = data.adresa;
                poslovniPartner.vrstaPartnera = data.vrstaPartnera;
                poslovniPartner.brojRacuna = data.brojRacuna;
                poslovniPartner.preduzece = data.preduzece;
                poslovniPartner.mesto = data.mesto;
                poslovniPartner.pib = data.pib;
                $("#naziv").val(data.nazivPartnera);
                $("#adresa").val(data.adresa);
                $("#broj_racuna").val(data.brojRacuna);
                $("#mestoDropdown").val(data.mesto);
                $("#pib").val(data.pib);
                $("#mb").val(data.mb);
                updatePartnerModal.modal("show");
            }
        })
    });
    
    $("#update_confirm").on("click", function(event){
        event.preventDefault();
        updatePartnerModal.modal("hide");
        poslovniPartner.nazivPartnera = $("#naziv").val();
        poslovniPartner.adresa = $("#adresa").val();
        poslovniPartner.brojRacuna = $("#broj_racuna").val();
        poslovniPartner.pib = $("#pib").val();
        poslovniPartner.mb = $("#mb").val();
        poslovniPartner.mesto = $("#mestoDropdown").val();

        $.ajax({
            url: "api/poslovni_partneri/"+ partnerId,
            type: 'PUT',
            data: JSON.stringify(poslovniPartner),
            contentType:"application/json"
        }).done(function(){
			alert("Poslovni partner je ažuriran!");
            ucitajPartnere();
        });
    });
    
    tabelaPartnera.on("click", "button.delete_partner", function(event){
        event.preventDefault();
        partnerId = $(this).attr("partner_id");
        deleteContent.text("Da li ste sigurni da želite da obrišete partnera sa id-em "+partnerId+" ?");
        deletePartner.modal("show");

    });
    
    $("#delete_confirm").on("click", function(event){
        event.preventDefault();
        $.ajax({
            url: 'api/poslovni_partneri/'+partnerId,
            type: 'DELETE',
            contentType: "application/json"
        }).done(function(){
            deletePartner.modal("hide");
            var tip = $("#tip-partnera input:checked").val();
            searchFilter.val('');
            ucitajPartnere(tip);
        })
    });
    
    $("#tip-partnera input").on("change", function(){
        ucitajPartnere();
    });
    
    $("#addPartnerIzlaz").on("click", function(){
        clearAddModal();
    });
    
    searchFilter.on('keyup', function (event) {
        event.preventDefault();
        ucitajPartnere();
    });
});