$(document).ready(function() {

	$("#navigation").load("nav.html");

	var preduzece = {};

	ucitajPreduzece();

	function ucitajPreduzece() {
		$.ajax({
			url: 'api/preduzece/1',
			dataType: "json",
			contentType: "application/json",
			success: function(data) {

				$.ajax({
					url: 'api/mesto/' + data.mesto,
					type: 'GET',
					async: false,
					success: function(data) {
						mesto = data
					}
				});

				preduzece = data;

				$("#naziv").text(data.naziv);
				$("#adresa").text(data.adresaPreduzeca + ", " + mesto.naziv);
				$("#telefon").text(data.telefon);
				$("#racun").text(data.brojRacuna);
				$("#email").text(data.email);
				$("#pib").text(data.pib);
				$("#mb").text(data.mb);
			}
		});
	}
});