$(document).ready(function() {
	$("#navigation").load("nav.html");

	var grpId = getGrpId();
	var grupaFilter = $("#grupa-robe-filter");
	var nameSearch = $("#naziv-robe");
	var robaTable = $("#robaTable tbody");

	var params = getParameters();
	var grupaParam = "";
	if (params["grupa"] != null) {
		grupaFilter.hide();
		grupaParam = params["grupa"];
	}

	getGrupa();
	getRoba();

	function getRoba() {
		robaTable.empty();
		var naziv = nameSearch.val();
		var grupa = grupaParam !== "" ? grupaParam : grupaFilter.val();
		$.ajax({
			url: `api/roba?naziv=${naziv}&grupa=${grupa}`,
			type: 'GET',
			dataType: "json",
			contentType: "application/json",
			success: function(data, textStatus, request) {
				for (i = 0; i < data.length; i++)
					addRoba(data[i]);
			}
		});
	}

	function addRoba(data) {
		var grupa;
		$.ajax({
			url: 'api/grupa/' + data.grupa,
			type: 'GET',
			async: false,
			success: function(data) {
				grupa = data.nazivGrupe;
			}
		});
		$("#robaTable tbody").append("<tr><td>" + data.nazivRobe + "</td><td>" + data.jedinicaMere + "</td><td>" +
			grupa + "</td><td class='text-right'><button value='" + data.id + "' class='btn btn-outline-primary edit'>Izmeni</button></td><td>" +
			"<button value='" + data.id + "' class='btn btn-outline-danger delete'>Obriši</button></td></tr>");
	}

	function getGrupa() {
		$.ajax({
			url: 'api/grupa',
			dataType: "json",
			contentType: "application/json",
			success: function(data) {
				for (i = 0; i < data.length; i++) {
					$("#grupa").append("<option value='" + data[i].id + "'>" + data[i].nazivGrupe + "</option>");
					grupaFilter.append("<option value='" + data[i].id + "'>" + data[i].nazivGrupe + "</option>");
				}
				if (params["grupa"] == null)
					$("#grupa").selectedIndex = 0;
				else $("#grupa").val(params["grupa"]);
			}
		});
	}
	function getParameters() {
		var param = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			param.push(hash[0]);
			param[hash[0]] = hash[1];
		}
		return param;
	}

	function getGrpId() {
		var url = window.location.href;
		var id = url.substring(url.lastIndexOf('=') + 1)
		var sliced = id.slice(2);
		return id;
	}

	$("#addRoba").click(function(e) {
		$("#title").text("Dodavanje robe");
		$("#naziv").val("");
		$("#jedinicaMere").val("");
		$("#addEditModal").modal("show");
		$("#confirmSave").on("click", function(e) {
			e.preventDefault();
			var naziv = $("#naziv").val();
			var jedinicaMere = $("#jedinicaMere").val();
			var grupa = $("#grupa").val();
			$.ajax({
				url: 'api/roba',
				type: 'POST',
				data: '{"nazivRobe":"' + naziv + '", "jedinicaMere":"' + jedinicaMere + '", "grupa":"' + grupa + '"}',
				contentType: "application/json",
				complete: function(data) {
					window.location.reload();
				}
			});
			$("#addEditModal").modal("hide");
		});
	});

	$(document).on("click", ".edit", function(e) {
		var id = $(this).val();
		console.log(id);
		$("#title").text("Izmjena robe");
		$.ajax({
			url: 'api/roba/' + id,
			type: 'GET',
			dataType: "json",
			contentType: "application/json",
			success: function(data) {
				$("#naziv").val(data.nazivRobe);
				$("#jedinicaMere").val(data.jedinicaMere);
				$("#grupa").val(data.grupa);
			}
		});
		$("#addEditModal").modal("show");
		$("#confirmSave").on("click", function(e) {
			e.preventDefault();
			var naziv = $("#naziv").val();
			var jedinicaMere = $("#jedinicaMere").val();
			var grupa = $("#grupa").val();
			$.ajax({
				url: 'api/roba/' + id,
				type: 'PUT',
				data: '{"id":' + id + ',"nazivRobe":"' + naziv + '", "jedinicaMere":"' + jedinicaMere + '", "grupa":' + grupa + '}',
				contentType: "application/json",
				complete: function(data) {
					window.location.reload();
				}
			});
			$("#addEditModal").modal("hide");
		});
	});

	$(document).on("click", ".delete", function(e) {
		e.preventDefault();
		var id = $(this).val();
		$("#deleteModal").modal("show");
		$("#confirm").on("click", function(e) {
			e.preventDefault();
			$.ajax({
				url: 'api/roba/' + id,
				type: 'DELETE',
				contentType: "application/json",
				complete: function(data) {
					window.location.reload();
				}
			});
			$("#deleteModal").modal("hide");
		});
	});


	nameSearch.on('keyup', function(event) {
		event.preventDefault();
		getRoba();
	});
	
	grupaFilter.on('change', function(event) {
		event.preventDefault();
		getRoba();
	});
});