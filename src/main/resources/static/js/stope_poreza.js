$(document).ready(function() {
	$("#navigation").load("nav.html");

	var tabela = $("#tabela");
	var deleteContent = $("#delete_content");
	var deleteStopa = $("#delete_stopa");
	var updateStopa = $("#update_stopa");
	var buttons = {
		addEdit: $("#add_edit_stopa")
	};
	var stopa = {
		id: 0,
		procenat: 0,
		datumVazenja: new Date(),
		porez: 1
	};
	var stopaHtml = {
		procenat: $("#procenat"),
		datumVazenja: $("#datum-vazenja"),
		porez: $("#add_edit_porez")
	};

	ucitajStope();
	appendPorez();

	function ucitajStope() {
		tabela.empty();
		$.get("api/stopa_poreza", function(data) {
			data.forEach(function(value) {
				red = $("<tr></tr>");
				red.append("<td>" + value.procenat + "%" + "</td>");
				red.append("<td>" + new Date(value.datumVazenja).toLocaleString() + "</td>");
				$.ajax({
					url: "api/porez/" + value.porez,
					dataType: "json",
					contentType: "application/json",
					async: false,
					success: function(data) {
						red.append("<td>" + data.nazivPoreza + "</td>");
					}
				});
				red.append("<td class='text-right'>" +
					"<button stopa_id='" + value.id + "' class='btn btn-outline-primary update_stopa mr-2'>Izmeni</button>" +
					"<button stopa_id='" + value.id + "' class='btn btn-outline-danger delete_stopa'>Obriši</button></td>");
				tabela.append(red);
			});
		});
	}

	function appendPorez() {
		$.get("api/porez", function(data) {
			data.forEach(function(value) {
				stopaHtml.porez.append("<option value='" + value.id + "'>" + value.nazivPoreza + "</option>");
			});
		});
	}

	$("#add_new_stopa").on("click", function(event) {
		event.preventDefault();
		updateStopa.modal("show");
		stopa.id = 0;
	});

	buttons.addEdit.on("click", function(event) {
		event.preventDefault();
		stopa.procenat = stopaHtml.procenat.val();
		stopa.datumVazenja = stopaHtml.datumVazenja.val();
		stopa.porez = stopaHtml.porez.val();
		var url = "api/stopa_poreza";
		var method = "POST";
		if (stopa.id != 0) {
			url += "/" + stopa.id;
			method = "PUT";
		}
		$.ajax({
			url: url,
			type: method,
			data: JSON.stringify(stopa),
			contentType: "application/json"
		}).done(function() {
			ucitajStope();
			if (stopa.id == 0) {
				alert("Stopa poreza je dodata!");
			} else {
				alert("Stopa poreza je izmenjena!");
			}
			stopa.id = 0;
		});
		updateStopa.modal("hide");
	});

	tabela.on("click", "button.update_stopa", function(event) {
		event.preventDefault();
		stopa.id = $(this).attr("stopa_id");
		$.ajax({
			url: 'api/stopa_poreza/' + stopa.id,
			type: 'GET',
			success: function(data) {
				updateStopa.modal("show");
				stopaHtml.procenat.val(data.procenat);
				var date = new Date(data.datumVazenja);
				var dateStr = date.getFullYear() + "-" + (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1) + "-" + (date.getDate() < 10 ? "0" : "") + date.getDate();
				stopaHtml.datumVazenja.val(dateStr);
				stopaHtml.porez.val(data.porez);
			}
		});
	});

	tabela.on("click", "button.delete_stopa", function(event) {
		event.preventDefault();
		stopa.id = $(this).attr("stopa_id");
		deleteContent.text("Da li ste sigurni da želite da obrišete stopu koja ima id =" + stopa.id + " ?");
		deleteStopa.modal("show");
	});

	$("#delete_confirm").on("click", function(event) {
		event.preventDefault();
		$.ajax({
			url: 'api/stopa_poreza/' + stopa.id,
			type: 'DELETE',
			contentType: "application/json"
		}).done(function() {
			ucitajStope();
			alert("Stopa poreza je obrisana!");
			stopa.id = 0;
		});
		deleteStopa.modal("hide");
	});

});